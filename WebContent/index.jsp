<!DOCTYPE html>
<html class="full-height" lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>City Performance Monitor</title>
    <meta name="description" content="Material design landing page template built by TemplateFlip.com"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" >
    <link type="text/css" rel="stylesheet" href="css/mdb.min.css" >
    <link type="text/css" rel="stylesheet" href="css/main.css">
  </head>
  <body id="top">
    <header class="myHeader">
      <!-- Navbar-->
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar" id="navbar">
        <div class="container navCont"><a class="navbar-brand" href="index.jsp"><img class="img-logo" src="img/logo_alpha_270x.png" alt="unalab logo"></a>
        <span class="navSpan">City Performance Monitor </span>
        <span class="text_center d-none">
				 	<span style="text-align: center" id="testo"></span>
				 </span>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
          <div class="collapse navbar-collapse" id="navbarContent">
    </div>
        </div>
      </nav>
      <!-- Intro Section-->
      <section class="view hm-gradient" id="intro">
        <div class="full-bg-img d-flex align-items-center">
          <div class="container">
            <div class="row no-gutters">
              <div class="col-md-10 col-lg-6 text-center text-md-left margins">
                <div class="white-text">
                  <div class="wow fadeInLeft" data-wow-delay="0.3s">
                    <h1 class="h1-responsive font-bold mt-sm-5 blackText">City Perfomance Monitor <i class="fa fa-tachometer" aria-hidden="true"></i></h1>
                    <div class="h6 blackText">
                     The City Performance Monitor is the performance analytics and monitoring tool used by the <a href="https://unalab.eu/home" target="_blank">UNaLab project </a>cities.
                     It increases stakeholder and citizen awareness of urban conditions through an easy to understand representation of the effectivness of the NBS implemented in the city using social, environmental and economic performnce indicators. You can find more information about NBS from  <a href="https://ec.europa.eu/research/environment/index.cfm?pg=nbs" target="_blank">here</a>
                     </div>
                  </div><br>
                  <div class="wow fadeInLeft" data-wow-delay="0.3s">
                  <a class="btn btn-outline-white blackText" href="#cities">Select your city</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </header>
    <div id="content">


<section class="py-5" id="projects">
  <div class="container">
    <div class="wow fadeIn">

    </div>
    <div class="row wow fadeInLeft" data-wow-delay=".3s"> 
<!--       <div class="col-lg-6 col-xl-5 pr-lg-5 pb-5"><img class="img-fluid rounded z-depth-2" src="img/Tampere.jpg" alt="project image"/></div> -->
     <div class="col-lg-6 col-xl-7 pl-lg-5 pb-4">

      </div>
    </div>
    
    <div class="row pt-5 wow fadeInRight" data-wow-delay=".3s">
      <div class="col-lg-6 col-xl-7 mb-3">
        <div class="row mb-3">
          <div class="col-1 mr-1"><i class="fa fa-bar-chart fa-2x"></i></div>
          <div class="col-10">
            <h3 class="font-bold">What are Key Performance Indicators?</h3>
            <h6 class="grey-text">
             Key Performance Indicators (KPIs) are measureable variables that are used to demonstrate NBS effectiveness. Each KPI targets a specific  identified challenge.
UNaLab project classifies and summarises recommended KPIs in a handbook. The handbook provides also a comprehensive descriptions, measurement/monitoring techniques and general guidance for KPI selection.
You can find more information about UNaLab KPIs at <a href="https://unalab.eu/documents/d31-nbs-performance-and-impact-monitoring-report" target="_blank">here.</a></h6>
          </div>
        </div>

      </div>
       <div class="col-lg-6 col-xl-5 mb-3"><img class="img-fluid rounded z-depth-2" src="img/Eindhoven.jpg" alt="project image"/></div> 
</div>
    

      </div>
    </div>
   
 
</section>

<section class="text-center py-5 grey lighten-4" id="about">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3">How it works</h2>
<!--       <p class="px-5 mb-5 pb-3 lead blue-grey-text"> -->
<!--         Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna -->
<!--         aliqua. Ut enim ad minim veniam. -->
<!--       </p> -->
    </div>
    <div class="row">
      <div class="col-md-4 mb-r wow fadeInUp" data-wow-delay=".3s"><img src="img/1.svg" class="fa fa-cubes  fa-5x mySize"></img>
        <h3 class="h4 mt-3">Select your city</h3>
        <p class="mt-3 blue-grey-text">
          Select the city to monitor.
        </p>
      </div>
      <div class="col-md-4 mb-r wow fadeInUp" data-wow-delay=".5s"><img style="width:29%" src="img/3.png" class="fa fa-cubes fa-5x mySize"></img>
      
        <h3 class="h4 mt-3">Citizen view</h3>
        <p class="mt-3 blue-grey-text">
          Provides the list of aggregated and simplified list of performance indicators. Click on the indicator to see the trend.
        </p>
      </div>
      <div class="col-md-4 mb-r wow fadeInUp" data-wow-delay=".4s"><img src="img/2.png" class="fa fa-cubes  fa-5x mySize"></img>
        <h3 class="h4 mt-3">City Dashboard</h3>
        <p class="mt-3 blue-grey-text">
          As the city showcase, maps and charts facilitates the understanding of how much the implemented NBSs are improving the urban areas.
        </p>
      </div>
    </div>
  </div>
</section>
<section class="text-center py-5 myGreen text-white" id="cities">
<form action>
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3">Select your city</h2>
      <p class="px-5 mb-5 pb-3 lead">
        Select the city to monitor.
      </p>
    </div>
    <div class="card-deck">

  <!-- Card -->
   <a href="/cpm_v2/GetCommonKPIs?city=3&lang=en" class="card mb-4 customHeight wow fadeInUp">
<!--   <div class="card mb-4 customHeight wow fadeInUp"> -->
 

    <!--Card image-->
    
    <div class="view overlay">
    
      <img class="card-img-top" src="img/Eindhoven.jpg" alt="Card image cap">
     
        <div class="mask rgba-white-slight"></div>
      
      
    </div>

    <!--Card content-->
    <div class="card-body">
  <input type="hidden" class="cityRadio" id="defaultGroupExample1" value="eindhoven" name="city" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
  <label for="defaultGroupExample1" style="color:black">Eindhoven</label>
	</div>
	 
<!-- 	</div> -->
	</a>
  
  
  
 
  
  <!-- Card -->

  <!-- Card -->
  <a href="/cpm_v2/GetCommonKPIs?city=2&lang=en" class="card mb-4 customHeight wow fadeInUp">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="img/Tampere.jpg" alt="Card image cap">
       
        <div class="mask rgba-white-slight"></div>
      
    </div>

    <!--Card content-->
    <div class="card-body">
  <input type="hidden" class="cityRadio" id="defaultGroupExample2" value="tampere" name="city">
  <label for="defaultGroupExample2" style="color:black">Tampere</label>
	</div>

  
  </a>
  <!-- Card -->

  <!-- Card -->
  <a href="/cpm_v2/GetCommonKPIs?city=1&lang=en" class="card mb-4 customHeight  wow fadeInUp">

    <!--Card image-->
    <div class="view overlay">
      <img class="card-img-top" src="img/Genova.jpg" alt="Card image cap">
     
        <div class="mask rgba-white-slight"></div>
      
    </div>

    <!--Card content-->
    <div class="card-body">
  <input type="hidden" class="cityRadio" id="defaultGroupExample3" value="genova" name="city">
  <label for="defaultGroupExample3" style="color:black">Genova</label>
	</div>

  
  <!-- Card -->

</a>

    
  </div>
  
  			<input name="scale" type="hidden" value="local">
			<input name="year" type="hidden" value="2030">
			<input name="indicator" type="hidden" value="aq_NO2">
			<input name="impact" type="hidden" value="absolute">
			<input name="scenario" type="hidden" value="1to5">
  
  
 
  
  
  </form>
</section>

 
    <footer class="page-footer indigo darken-2 center-on-small-only pt-0 mt-0">

      <div class="footer-copyright">
        <div class="container-fluid">
         <span class="footer-section section-center white-text" style="float: left;">
				<span>� 2019 &nbsp;</span>
				<a href="https://unalab.eu/home" target="_blank">UNaLAB</a>
				<span>&nbsp; All rights reserved.</span>
			</span>
       
       		<span class="footer-section section-right white-text" style="float: right;">
				<span>Powered by &nbsp;</span> 
				<a href="https://digitalenabler.eng.it/en/">
					<img class="footer-logo" src="img/digital-enabler-white_symbol.png"/>
					<span>Digital<br>Enabler</span>
				</a>
			</span>
        </div>
      </div>
    </footer>
    <script type="text/javascript" src="js/bootstrap/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/popper.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/mdb.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/main.js"></script>
    <script>new WOW().init();</script>
  </body>
</html>