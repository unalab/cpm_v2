$(document).ready(function(){
	 $('.datepicker').datepicker();
});

$('.datepicker').change(function(){
	current.filterFrom = $('#filterFrom').val();
	current.filterTo = $('#filterTo').val();
	generateData(current.idKpi,current.city,current.type);
	$("#chart1").show();
});

function isInDateRange(date) {
	var isInRange = false;
	var parsedDate = Date.parse(date);
	var parsedFrom = Date.parse(current.filterFrom);
	var parsedTo = Date.parse(current.filterTo);
	if(!isNaN(parsedFrom) && !isNaN(parsedTo)) {
		if(parsedDate >= parsedFrom && parsedDate <= parsedTo) {
			isInRange = true;
			return isInRange;
		}
	} else if(!isNaN(parsedFrom) && isNaN(parsedTo)) {
		if(parsedDate >= parsedFrom) {
			isInRange = true;
			return isInRange;
		}
	} else if(isNaN(parsedFrom) && !isNaN(parsedTo)) {
		if(parsedDate <= parsedTo) {
			isInRange = true;
			return isInRange;
		}
	} else if(isNaN(parsedFrom) && isNaN(parsedTo)) {
		return true;
	}
	return isInRange;
}	

function calculateVariance(data) {
	var values = [];
	for (var i = 0; i < data.length; i++) {
		var val = data[i];
		values.push(val);
	}
	var total = values.length;
	var sum = values.reduce(function(a,b){
		return a + b;
	});
	var mean = sum/total;
	var varianceValues = [];
	for (var j = 0; j < total; j++) {
		var val = values[j] - mean;
		varianceValues.push(val*val);
	}
	var sumVariance = varianceValues.reduce(function(a,b){
		return a + b;
	});
	var variance = sumVariance/total;
	console.log("VARIANCE:" + variance);
	$('#varianceValue').text(variance.toFixed(2));
}

function calculateMin(data) {
	var values = [];
	for (var i = 0; i < data.length; i++) {
		var val = data[i];
		values.push(val);
	}
	var min = Math.min(...values);
	$('#minValue').text(min.toFixed(2));
}

function calculateMax(data) {
	var values = [];
	for (var i = 0; i < data.length; i++) {
		var val = data[i];
		values.push(val);
	}
	var max = Math.max(...values);
	$('#maxValue').text(max.toFixed(2));
}

function calculateAvg(data) {
	var values = [];
	for (var i = 0; i < data.length; i++) {
		var val = data[i];
		values.push(val);
	}
	var total = values.length;
	var sum = values.reduce(function(a,b){
		return a + b;
	});
	var mean = sum/total;
	$('#avgValue').text(mean.toFixed(2));
}