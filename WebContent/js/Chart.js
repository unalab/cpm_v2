//
//function renderChart(indicatorValue, datasetName){
//    
//
//	var TUTORIAL_SAVVY ={
//
//			loadStudentData : function(){
//				var input = new Object();
//
//				input['indicator'] = indicatorValue;
//				input['param'] = datasetName;
//
//				console.log("Chart name: " + input['indicator']);
//				var formattedstudentListArray =[];
//
//				$.ajax({
//
//					async: false,
//
//					url: "ChartDataServlet",
//
//					dataType:"json",
//					data: input,
//
//					success: function(chartData) {
//
//						console.log(chartData);
//
//						$.each(chartData,function(index,elem){
//							//console.log(Object.values(elem));
//							formattedstudentListArray.push(Object.values(elem));
//						});
//					}
//				});
//
//				return formattedstudentListArray;
//			},
//			
//			 loadDistrictData: function(){
//				 var input = new Object();
//				 input['param'] = datasetName;
//				 var formattedDistrictData = [];
//				 $.ajax({
//
//						async: false,
//
//						url: "DistrictDataServlet",
//
//						dataType:"json",
//						data: input,
//
//						success: function(districtData) {
//
//							console.log(districtData);
//
//							$.each(districtData,function(index,elem){
//								//console.log(Object.values(elem));
//								formattedDistrictData.push(Object.values(elem));
//							});
//						}
//					});
//
//					return formattedDistrictData;
//
//			 },
//			createChartData : function(formattedstudentListArray,formattedDistrictData){
//
//				//console.log(formattedstudentListArray);
//				console.log(formattedDistrictData);
//
//				return {
//
//					labels: formattedDistrictData,
//					datasets : [
//						{    	    
//							label: indicatorValue,
//							fill: true,
//							borderColor: "#82933b",
//							backgroundColor: "#82933b",
//							data : formattedstudentListArray
//
//						}
//						]
//				};
//			},
//			/*Renders the Chart on a canvas and returns the reference to chart*/
//			renderStudenrRadarChart:function(radarChartData){
//
//				$("canvas#canvas").remove();
//				$("div.chartContainer").append(' <canvas id="canvas"></canvas>');
//				var context2D = document.getElementById("canvas"),
//
//				myRadar = new Chart(context2D, {
//
//					type: "horizontalBar",
//					data: radarChartData,
//					options: {
//						responsive: true,
//						maintainAspectRatio: false,
//						aspectRatio: 2,
//					},
//					scaleShowLabels : false,
//					pointLabelFontSize : 0
//				});
//
//				console.log(myRadar);    
//				return myRadar;
//
//			},
//
//			/*Initalization Student render chart*/
//			initRadarChart : function(){
//
//				var studentData = TUTORIAL_SAVVY.loadStudentData();
//				var districtData = TUTORIAL_SAVVY.loadDistrictData();
//
//				chartData = TUTORIAL_SAVVY.createChartData(studentData,districtData);
//
//				radarChartObj = TUTORIAL_SAVVY.renderStudenrRadarChart(chartData);
//
//			}
//	};
//
//
//	$(document).ready(function(){
//
//		TUTORIAL_SAVVY.initRadarChart();
//	})
//
//}
//
//
//
//function GetCountryList(){
//    var data2 = "";
//    $.ajax({
//        type: "POST",
//        url: "qs",
//        data: "{\"type\":" + "\"country\""+      
//       "}",
//        contentType: "application/x-www-form-urlencoded",
//        dataType: "json",
//        success: function(response) {
//
//            var resultsArray =  (typeof response) == 'string' ? eval('(' + response + ')')    : response;  
//
//
//
//            var data2 = new Array();
//            for(var i=0; i<resultsArray.length;i++){
//                data2[i] =  resultsArray[i].workgroup;
//
//
//                var barChartData = {
//                        labels : data2,
//                        datasets : [
//                                {
//                                        fillColor : "rgba(220,220,220,0.5)",
//                                        strokeColor : "rgba(220,220,220,1)",
//                                        data : [65,59,90,81,56,55,40,80]
//                                }
//                        ]
//
//                };
//           }
//
//
//    var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData);
//
//        }
//
//
//    });                 
//
//}
//
//


var labels = [];
var baseValues = [];
var absValues = [];
var indName = '';

function retrieveSideChartData(year, indicator, indType, scenario){
	labels = [];
	baseValues = [];
	absValues = [];
	var input = new Object();
	var url = new URL(window.location.href);
	var url_params = new URLSearchParams(url.search);
	var string;
	city = url_params.get('city');
	input['city'] = city;
	input['scale'] = "neighbourhood";
	input['type'] = indType;
	input['prevision'] = prevision;
	input['prevision2'] = prevision2;
	
	input['year'] = year;
	//input['impact'] = impact;
	input['scenario'] = scenario;
	let indicatorType= type+"_";
	//var test = $('.menu-title.indicator').text();
	
	//if(test!=="-"){
	//	indicator = test;
	//}
	
	//indicator = indicator.replace("-","");
	//indicator = indicator.replace("--","");
	//console.log("INDICATOR: " + indicator);
	
	input['indicator'] = indicator;
	indName = indicator;
	
	$.ajax({
        
      async: false,
      type: "GET",
      url: "ChartServlet",
     
      data: input,
      success: function(chartData) {
    	  	
    	  	var json = JSON.parse(chartData);
    	  	
    	  	for (var i in json){
    	  	
    	  		labels.push(json[i].Neighbourhood);
    	  		baseValues.push(json[i].base);
    	  		absValues.push(json[i].absolute);
    	  		
    	  		
    	  		
    	  	}

	
       },
	
       error: function(xhr, status, error) {
    	   var err = eval("(" + xhr.responseText + ")");
    	   alert(err.Message);
    	 }
      });


}

function retrieveChartData(){
	
	
	labels = [];
	baseValues = [];
	absValues = [];
	var input = new Object();
	
	var url = new URL(window.location.href);
	var url_params = new URLSearchParams(url.search);
	var string;
	city = url_params.get('city');
	scale = url_params.get('scale');
	string = url_params.get('indicator');
	type= string.substring(0,2);
	indicator = string.substring(3);
	year = url_params.get('year');
	impact = url_params.get('impact');
	scenario = url_params.get('scenario');
	input['city'] = city;
	input['scale'] = scale;
	input['type'] = type;
	
	input['year'] = year;
	input['impact'] = impact;
	input['scenario'] = scenario;
	input['prevision'] = prevision;
	input['prevision2'] = prevision2;
	let indicatorType= type+"_";
	var test = $('.menu-title.indicator').text();
	
//	if(test!=="-"){
//		indicator = test;
//	}
//	
//	indicator = indicator.replace("-","");
//	indicator = indicator.replace("--","");
	console.log("INDICATOR: " + indicator);
	
	input['indicator'] = indicator;
	indName = indicator;
	
	
	$.ajax({
        
      async: false,
      type: "GET",
      url: "ChartServlet",
     
      data: input,
      success: function(chartData) {
    	  	
    	  	var json = JSON.parse(chartData);
    	  	
    	  	for (var i in json){
    	  	
    	  		labels.push(json[i].Neighbourhood);
    	  		baseValues.push(json[i].base);
    	  		absValues.push(json[i].absolute);
    	  		
    	  		
    	  		
    	  	}

	
       },
	
       error: function(xhr, status, error) {
    	   var err = eval("(" + xhr.responseText + ")");
    	   alert(err.Message);
    	 }
      });

	
}

function renderChart(chartId){

   var CHART_BUILD = { 
		   
	loadData : function(){
    console.log("LABELS " + labels);
    console.log("ABS " + absValues);
    console.log("BASE " + baseValues);
    var data = {
    		  labels: labels,
    		  datasets: [{
    		    label: "Base Values",
    		    backgroundColor: "#90ab5a",
    		    data: baseValues
    		  }, 
    		  {
    		    label: "Absolute Values",
    		    backgroundColor: "77943e",
    		    data: absValues
    		  }]
    		};
    var option = {
    		responsive: true,
    		  legend: {
    		    position: "top"
    		  },
    		  title: {
    		    display: true,
    		    text: indName + " chart"
    		  },
    		  scales: {
    		    yAxes: [{
    		      ticks: {
    		        beginAtZero: true
    		      }
    		    }]
    		  }
    	};
    var ctx = document.getElementById(chartId),
    
	  myRadar = new Chart(ctx, {
		    type: "horizontalBar",
		    data: data,
		    options: option
		  });
    console.log(myRadar);
    return myRadar;
},

initRadarChart : function(){

	var myChart = CHART_BUILD.loadData();
}
};
   
   $(document).ready(function(){
	   
	   CHART_BUILD.initRadarChart();
	  })

}
