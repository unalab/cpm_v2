var idKnowage;
var jsonCalculated;
var value;
var value2;
var idKnowage2;
var city;
var cityString = "";
var lang="en";
var json = "";
var auto;
var t;
var indicatorName;
var kpiName;
var timeArray = [];
var dataArray = [];
var current;
var globalChart;

var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

$( window ).on( "load", function(){
    if(!isChrome){
    		$('.text_center').removeClass("hide");
   		if($("testo")){
   			
   			$('#testo').html("Please, use Google Chrome to have best performances");
   			
   		}
    		
    }
});


function createFromKPIEngine(){
	
	var input = new  Object();
	input['id'] = $('#id').val();
	input['name'] = $('#name').val();
	input['desc'] = $('#description').val();
	input['unit'] = $('#unit').val();
	input['nbs'] = $('#nbs').val();
	input['kpiName'] = "-";
	input['level'] = $('#level').val();
	input['cat2'] = $('#cat2').val();
	input['city'] = cityString;
	
	
	$.ajax({
		async: false,
        type: "POST",
        data: input,
        url: "AddKPIfromEngine",
        success: function(data){ 
        	console.log(data);
        	window.location.href = "/cpm_v2/overview.jsp?city=" + city + "&idCPM="+data;
        }
	});
}

function getKPIs(){
	
	
	var input = new Object();
	input['city'] = cityString;
	$.ajax({
		async: false,
        type: "GET",
        data: input,
        url: "getCpmKPIs",
        
        success: function(data){ 
        	console.log(data);
        	json = JSON.parse(data);
        	console.log(json);
        	$('#auto').empty();
        	$('<option value="-"> - Select an option - </option>').appendTo('#auto');
        	console.log(json.length);
        	for (i=0; i<json.length; i++){
        		console.log(json[i]);
        		
        		var option = $('<option></option>').appendTo('#auto');
        		option.attr("value", json[i].name);
        		option.text(json[i].name);
        		
        	}
        	
        	
        }
        });
}

function dashboardParams(idKpi, city, type){
//	var id = idKpi;
//	console.log(id);
//	console.log("CITY FROM MANUAL " + city );
//	var c = city;
	current = {};
	current = {idKpi:idKpi,city:city,type:type};
	generateData(idKpi,city,type);
//	var dashboard = prefix+manualkpidashName+suffix+param1+id+param2+c;
//	console.log(dashboard);
//	$("#cockpit").attr("src",dashboard);
//	console.log($("#cockpit").attr("src",dashboard));
	if($("#icon_"+idKpi).text()==="insert_chart"){
		$(".eyes").text("insert_chart");
		$("#icon_"+idKpi).text("visibility_off");
		$("#chart1").show();
		$("#table1").show()
		$("#chart1").removeClass("hide");
		$("#table1").removeClass("hide");
		$("#dashboardContainer").removeClass("hide");		
		$("#pag").hide();
	}
	else{
		$("#icon_"+idKpi).text("insert_chart");
		$("#chart1").hide();
		$("#table1").hide();
		$("#dashboardContainer").addClass("hide");
		$("#pag").show();
	}
	
}



function getCityName(id_city){
	
	var input = new Object();
	input['id'] = id_city;
	$.ajax({
		async: false,
        type: "GET",
        url: "GetCity",
        data: input,
        success: function(name){
        	cityString = name;
        	console.log(cityString);
        }
	});
}

function dashboardParams2(idKpi, city, unitB){
	var id = idKpi;
	var c = cityString;
	
	var unit = unitB;
	console.log("UNIT BEFORE: " + unit);
	if (unit === '%'){
		unit = 'percent';
	}
	
	console.log("UNIT AFTER: " + unit);
	console.log("CITY FROM: " + cityString);
	console.log("ID KPI: " +id);
	
	
	
	console.log("CITY ID: "+ cityString);
	var input = new Object();
	input['idkpi'] = id;
	input['city'] = cityString;
	
	$.ajax({
		async: false,
        type: "POST",
        url: "GetIdKnowage",
        data: input,
        success: function(id){ 
        	    console.log("ID ID ID " + id);
        		var idKnowage = id;
        		console.log("idKnowage:" + idKnowage);
        		
        		console.log("CITY " + c);
        		$.ajax({
    				async: false,
    				type: "POST",
    				url: "GetIdCityKnowage",
    				data: input,
    				
    				success: function(city_category){
    					var cityKnowage =  city_category;
    					console.log("city knowage: " + cityKnowage);
    					getCityName(city);
    					console.log("CITTA TROVATA: " + cityString);
    					var dashboard = prefix+computedName+suffix+param1+idKnowage+param2+cityKnowage+param3+cityString+param4+unit;
    					console.log("DASHBOARD: " + dashboard);
    					
    					$("#cockpit").attr("src",dashboard);
    					
    					if($("#icon_"+idKpi).text()==="insert_chart"){
    						
    						$(".eyes").text("insert_chart");
    						$("#icon_"+idKpi).text("visibility_off");
							$("#chart1").show();
							$("#chart1").removeClass("hide");
    						$("#pag").hide();
    					}
    					else{
    						$("#icon_"+idKpi).text("insert_chart");
    						$("#chart1").hide();
    						$("#pag").show();
    					}
    					}
    				});

        }
        
	});

	
	
}

function isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}



function getEngineKPIs(){
	var input = new Object();
	input['city'] = cityString;
	$.ajax({
		async: false,
        type: "GET",
        data: input,
        url: "getCpmKPIs",
        
        success: function(data){ 
        	console.log(data);
        	json = JSON.parse(data);
        	console.log(json);
        	$('#auto2').empty();
        	console.log("INDICATOR NAME: " +indicatorName);
        	var first = $('<option></option>').appendTo('#auto2');
        	first.attr("value",indicatorName);
        	first.text(indicatorName);
        	console.log(json.length);
        	for (i=0; i<json.length; i++){
        		console.log(json[i]);
        		
        		var option = $('<option></option>').appendTo('#auto2');
        		option.attr("value", json[i].name);
        		option.text(json[i].name);
        		
        	}
        }
        });
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:application/json;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}




	function generateJson(city){
	var input = new Object();
	input['city'] = city;
	
	
	$.ajax({
	  async: false,
	  data: input,
      type: "GET",
      url: "download",

      success: function(jsonServer){  
    	json = jsonServer;
    	console.log(json);
	  }
   	
	});

   var filename = "KPIs.json";
    
    download(filename, json);
}




function myParam (kpi,level,category,city,title,tableId){
	
	var kpiId = kpi;
	var l= level;
	console.log("LEVEL: " + level);
	var category2 = category;
	var ct = city;
	console.log(category2);
	var title = title;
	var id = tableId;
	
	var text4;
	var text5;
	if(lang==="en"){
		text4 = insert.en;
		text5 = wit.en;
	}
	else if (lang==="it"){
		text4 = insert.it;
		text5 = wit.it;
	}
	else if (lang==="fi"){
		text4 = insert.fi;
		text5 = wit.fi;
	}
	else if (lang==="du"){
		text4 = insert.du;
		text5 = wit.du;
	}
	$(".title").text(text4 + title + " "+text5+ " id " + id);
	var oModal = $('#modal2');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(category2);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('input[name="title"]').val(title);
	oModal.find('input[name="tableId"]').val(id);
    oModal.modal();
}
		
function myCityParam (kpi,level,category,city,title,tableId){

	var kpiId = kpi;
	var l= level;
	var category2 = category;
	var ct = city;
	console.log(category2);
	var title = title;
	var id = tableId;
	var text4;
	var text5;
	if(lang==="en"){
		text4 = insert.en;
		text5 = wit.en;
	}
	else if (lang==="it"){
		text4 = insert.it;
		text5 = wit.it;
	}
	else if (lang==="fi"){
		text4 = insert.fi;
		text5 = wit.fi;
	}
	else if (lang==="du"){
		text4 = insert.du;
		text5 = wit.du;
	}
	$(".title").text(text4 + title + " "+text5+ " id " + id);
	var oModal = $('#modalcity');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(category2);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('input[name="title"]').val(title);
	oModal.find('input[name="tableId"]').val(id);
    oModal.modal();
}


$(document).ready(function(){
	
	$('.dropdown-trigger').dropdown();
	$('.dropdown-button').dropdown();	
	
	
//	$('#myTable2').tablesorter();
	var url = new URL(window.location.href);
	console.log("URL: " + url);

	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	lang = url_params.get('lang');
	$('.tooltipped').tooltip();
	console.log("URL CITY: " +city);
	getCityName(city);
	console.log("CITY STRING " + cityString);
	
	$("#titleCity").text("City Perfomance Monitor - "+ cityString);

$('#myTable').pageMe({
    pagerSelector:'#myPager',
    activeColor: 'unalabGreen',
    showPrevNext:true,
    hidePageNumbers:false,
    perPage:10
  });
$('.tooltipped').tooltip();
$('.modal').modal();

$('#computedForm').hide();
$('#manualComputedSwitch').change(function(){
	if($(this).is(":checked")){
		$('#manualForm').hide();
		$('#computedForm').show();
	}
	else if($(this).not(":checked")){
		$('#computedForm').hide();
		$('#manualForm').show();	
	}
});


if ($("#one").val() == "task"){
	$("#two").parent().show();
	$("#third").parent().hide();
}
else if ($("#one").val() == "city" || $("#ƒ").val() == "project") {
	$("#two").parent().hide();
	$("#third").parent().show();
}

getKPIs();
if($("#indType").val()=="citymanual" || $("#indType").val()=="manual"){
	$("#autoSelect").addClass("hide");
	$("#createAuto").addClass("hide");
}
else{
	$("#autoSelect").removeClass("hide");
	$("#createAuto").removeClass("hide");
}




$("#indType").change(function(){
	t= $(this).val();
	if($(this).val() == "computed"){
		$("#autoSelect").removeClass("hide");
		$("#createAuto").removeClass("hide");
		auto = "-";
		$("#submit").attr("disabled", true);
		
	}
	else{
		$("#autoSelect").addClass("hide");
		$("#createAuto").addClass("hide");
	}
});

$("#one").change(function() {
	
	if($(this).val() == "task") {
		
		$("#two").parent().show();
		$("#third").parent().hide();
		
	}
	else if($(this).val() == "city" || $(this).val() == "project") {
		
		$("#two").parent().hide();
		$("#third").parent().show();
		
	}
	
});

if ($("#level").val() == "task"){
	$("#cat1").parent().show();
	$("#cat2").parent().hide();
}
else if ($("#level").val() == "city" || $("#level").val() == "project") {
	$("#cat1").parent().hide();
	$("#cat2").parent().show();
}



if ($("#computedLevel").val() == "task"){
	$("#computedCat1").parent().show();
	$("#computedCat2").parent().hide();
}
else if ($("#computedLevel").val() == "city" || $("#computedLevel").val() == "project") {
	$("#computedCat1").parent().hide();
	$("#computedCat2").parent().show();
}


$("#computedLevel").change(function() {
	
	if($(this).val() == "task") {
		console.log("ciao");
		$("#computedCat1").parent().show();
		$("#computedCat2").parent().hide();
		
	}
	else if($(this).val() == "city" || $(this).val() == "project") {
		
		$("#computedCat1").parent().hide();
		$("#computedCat2").parent().show();
		
	}
	
	
	
});


var url = new URL(window.location.href);
console.log("URL: " + url);

var url_params = new URLSearchParams(url.search);
city = url_params.get('city');
lang = url_params.get('lang');
if(lang===null){
	lang="en";
}
console.log("CITY: "+ city);
getCityName(city);
console.log("CITY STRING: " + cityString);
var dashboardname;
if(cityString.toLowerCase()==="genova")
{
	dashboardname = "GenovaCPMDashboard";
}
else{
	dashboardname = cityString.toLowerCase()+"CPM3";
}

console.log(dashboardname);
var dashboardURL = prefix+dashboardname+suffixnoparameters;
console.log(dashboardURL);
$('#cityDashboard').attr('src', dashboardURL);


$('#checkCityComputed').change(function(){
	
	if($(this).is(":checked")){
		$("#autoSelect2").removeClass("hide");
		getKPIs();
		$('select').formSelect();
		
		
		
		
	}
	else if($(this).not(":checked")){
		$("#autoSelect2").addClass("hide");
	}
	
	
	
	
})


$("#compKPI2 li a").click(function(el){
	
	var $this = $(this);
	
	if($this.find("span").length > 0){
		$val = $this.find("span").text();
	}else{
		$val = $this.text();
	}
	console.log("SELECTED: "+ $val);
	
	$('#indicatorValue2').text($val);
	
});


$("#search").on("keyup", function() {
var value = $(this).val();
if(value!=""){
	console.log("page hide")
	$('#pag').hide();
}
else{
	console.log("page show")
	$('#pag #myPager').empty();
	$('#pag').show();
}
console.log("VALUE: " + value);
var set =  $(".tableContent table tr");
var length = set.length;

set.each(function(index){
    if (index != 0 && index < length) {

    	$row = $(this);
		
        var id = $row.find("td").text();
       
        if (id.toLowerCase().indexOf(value.toLowerCase()) == -1) {
            $row.hide();
        } 
        else if (value!=""){
        		$row.show();
        }
    } else if (value=="") {
          $('#myTable').pageMe({
  		    pagerSelector:'#myPager',
  		    activeColor: 'unalabGreen',
  		    showPrevNext:true,
  		    hidePageNumbers:false,
  		    perPage: 10
  		  });
    }
    
});
});

$('#nameEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#nameError').removeClass("hide");
		$("#submitEdit").attr("disabled", true);
		
	}
	else{if($("#descEdit").val()=="" || $("#unitEdit").val()=="" || $("#nbsEdit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submitEdit").attr("disabled", true);
		$('#nameError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submitEdit").attr("disabled", false);
		$('#nameError').addClass("hide");
	}
	}	
});

$('#descEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#descError').removeClass("hide");
		$("#submitEdit").attr("disabled", true);
		
	}
	else{
		if($("#nameEdit").val()=="" || $("#unitEdit").val()=="" || $("#nbsEdit").val()==""){
			input.removeClass("invalid").addClass("valid");
			$("#submitEdit").attr("disabled", true);
			$('#descError').addClass("hide");
		}
		else {
			input.removeClass("invalid").addClass("valid");
			$("#submitEdit").attr("disabled", false);
			$('#descError').addClass("hide");
		}
	}	
});

$('#unitEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#unitError').removeClass("hide");
		$("#submitEdit").attr("disabled", true);
		
	}
	else{if($("#descEdit").val()=="" || $("#nameEdit").val()=="" || $("#nbsEdit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submitEdit").attr("disabled", true);
		$('#unitError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submitEdit").attr("disabled", false);
		$('#unitError').addClass("hide");
	}
	}	
});



$('#nbsEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#nbsError').removeClass("hide");
		$("#submitEdit").attr("disabled", true);
		
	}
	else{if($("#descEdit").val()=="" || $("#unitEdit").val()=="" || $("#nameEdit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submitEdit").attr("disabled", true);
		$('#nbsError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submitEdit").attr("disabled", false);
		$('#nbsError').addClass("hide");
	}
	}	
});

$('#nameAdminEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#nameError').removeClass("hide");
		$("#submitAdminEdit").attr("disabled", true);
		
	}
	else{if($("#descAdminEdit").val()=="" || $("#unitAdminEdit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submitAdminEdit").attr("disabled", true);
		$('#nameError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submitAdminEdit").attr("disabled", false);
		$('#nameError').addClass("hide");
	}
	}	
});

$('#descAdminEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#descError').removeClass("hide");
		$("#submitAdminEdit").attr("disabled", true);
		
	}
	else{
		if($("#nameAdminEdit").val()=="" || $("#unitAdminEdit").val()==""){
			input.removeClass("invalid").addClass("valid");
			$("#submitAdminEdit").attr("disabled", true);
			$('#descError').addClass("hide");
		}
		else {
			input.removeClass("invalid").addClass("valid");
			$("#submitAdminEdit").attr("disabled", false);
			$('#descError').addClass("hide");
		}
	}	
});

$('#unitAdminEdit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#unitError').removeClass("hide");
		$("#submitAdminEdit").attr("disabled", true);
		
	}
	else{if($("#descAdminEdit").val()=="" || $("#nameAdminEdit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submitAdminEdit").attr("disabled", true);
		$('#unitError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submitAdminEdit").attr("disabled", false);
		$('#unitError').addClass("hide");
	}
	}	
});



$('#id').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#idError').removeClass("hide");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		
	}
	else{if($("#description").val()=="" || $("#name").val()=="" || $("#nbs").val()=="" || $("#unit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		$('#idError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", false);
		$("#submit2").attr("disabled", false);
		$('#idError').addClass("hide");
	}
	}	
});


$('#name').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#newNameError').removeClass("hide");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		
	}
	else{if($("#description").val()=="" || $("#id").val()=="" || $("#nbs").val()=="" || $("#unit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		$('#newNameError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", false);
		$("#submit2").attr("disabled", false);
		$('#newNameError').addClass("hide");
	}
	}	
});

$('#description').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#newDescError').removeClass("hide");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		
	}
	else{if($("#name").val()=="" || $("#id").val()=="" || $("#nbs").val()=="" || $("#unit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		$('#newDescError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", false);
		$("#submit2").attr("disabled", false);
		$('#newDescError').addClass("hide");
	}
	}	
});

$('#unit').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#newUnitError').removeClass("hide");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		
	}
	else{if($("#description").val()=="" || $("#id").val()=="" || $("#nbs").val()=="" || $("#name").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		$('#newUnitError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", false);
		$("#submit2").attr("disabled", false);
		$('#newUnitError').addClass("hide");
	}
	}	
});

$('#nbs').keyup(function(event) {
	var input=$(this);
	var message=$(this).val();
	console.log(message);
	if(message==""){input.removeClass("valid").addClass("invalid");
		$('#newNbsError').removeClass("hide");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		
	}
	else{if($("#description").val()=="" || $("#id").val()=="" || $("#name").val()=="" || $("#unit").val()==""){
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", true);
		$("#submit2").attr("disabled", true);
		$('#newNbsError').addClass("hide");
	}
	else {
		input.removeClass("invalid").addClass("valid");
		$("#submit").attr("disabled", false);
		$("#submit2").attr("disabled", false);
		$('#newNbsError').addClass("hide");
	}
	}	
});

//$('#submit').attr('disabled', true);
$('input[type="text"], input[type="number"], textarea').on('keyup',function() {
    var id = $("#id").val();
    var name = $("#name").val();
    var desc = $("#description").val();
    var unit = $("#unit").val();
    var nbsValue = $("#nbs").val();
   
    console.log("T: " + t);
    t=$("#indType").val();
    if(t=='manual' || t=='citymanual'){
    	if(id != '' && name != '' && desc != '' && unit != '' && nbsValue != '') {
    		$('#submit').attr('disabled', false);
    	}
    	else {
    		$('#submit').attr('disabled', true);
    	}
    }
    
    else{
    	

    	if(id != '' && name != '' && desc != '' && unit != '' && nbsValue != '') {
    		auto = $("#auto").val();
        	$("#auto").change(function() {
        		auto= $(this).val();
        	});
        	console.log("AUTO: " + auto);
    		if(auto=='-'){
    			$('#submit').attr('disabled', true);
    			$('#submit2').attr('disabled', false);
    		}
    		else{
    			$('#submit').attr('disabled', false);
    			$('#submit2').attr('disabled', true)
    		}   
    	} else{
    	
    		$('#submit').attr('disabled', true);
    		$('#submit2').attr('disabled', true);
        
    	}
    }
});

$("#auto").change(function() {
	auto= $(this).val();
	var id = $("#id").val();
    var name = $("#name").val();
    var desc = $("#description").val();
    var unit = $("#unit").val();
    var nbsValue = $("#nbs").val();
    t = "computed";
    if(id != '' && name != '' && desc != '' && unit != '' && nbsValue != '' && auto == "-"){
    	
		$('#submit').attr('disabled', true);
		$('#submit2').attr('disabled', false);
	}
	else if(id != '' && name != '' && desc != '' && unit != '' && nbsValue != '' && auto != "-"){
		$('#submit').attr('disabled', false);
		$('#submit2').attr('disabled', true);
	}
	else{
		$('#submit').attr('disabled', true);
		$('#submit2').attr('disabled', true);
	}
});

$('input[type="text"], input[type="number"], textarea').on('keyup',function() {
    var id = $("#idComputed").val();
    var name = $("#nameComputed").val();
    var desc = $("#descriptionComputed").val();
    var unit = $("#unitComputed").val();
    var ind = $('#indicatorValue').val();
    
    if(id != '' && name != '' && desc != '' && unit != '' &&  ind!= 'none') {
        $('#submitComputed').attr('disabled', false);
    } else {
        $('#submitComputed').attr('disabled', true);
    }
});



//$('#submitEdit').attr('disabled', true);
//$('#submitAdminEdit').attr('disabled', true);
//$('input[type="text"], textarea').on('keyup',function() {
//	var nameAdmin = $("#nameAdminEdit").val();
//	var descAdmin = $("#descAdminEdit").val();
//	var unitAdmin = $("#unitAdminEdit").val();
//	if(nameAdmin != '' && descAdmin != '' && unitAdmin != '') {
//		$('#submitAdminEdit').attr('disabled', false);
//	} else {
//		$('#submitAdminEdit').attr('disabled', true);
//	}
//});

$('#checkCityComputed').change(function(){ 
	if($(this).is(":checked")){
		console.log("CECCATO");
		getEngineKPIs();
		$('select').formSelect();
		//$('#submitEdit').attr('disabled', true);
		var ind = $('#auto2').val();
		 var name = $("#nameEdit").val();
		 var desc = $("#descEdit").val();
		 var unit = $("#unitEdit").val();
//		if(name != '' && desc != '' && unit != '' &&  ind!= 'none') {
//			$('#submitEdit').attr('disabled', false);
//		} else {
//			$('#submitEdit').attr('disabled', true);
//	
//		}
	}
//	else if ($(this).not(":checked")){
//		console.log(" NON CECCATO");
//		var name = $("#nameEdit").val();
//		 var desc = $("#descEdit").val();
//		 var unit = $("#unitEdit").val();
//		if(name != '' && desc != '' && unit != '') {
//			$('#submitEdit').attr('disabled', false);
//		} else {
//			$('#submitEdit').attr('disabled', true);
//		}
//	}
});

if($('#autoSelect2').hasClass(':not(.hide)')){
	$('input[type="text"], input[type="number"], textarea').on('keyup',function() {
    
    var name = $("#nameEdit").val();
    var desc = $("#descEdit").val();
    var unit = $("#unitEdit").val();
    
    	
    		var ind = $('#auto2').val();
    		if(name != '' && desc != '' && unit != '' &&  ind!= 'none') {
    	        $('#submitEdit').attr('disabled', false);
    	    } else {
    	        $('#submitEdit').attr('disabled', true);
    	    }
    		
 });
	
	
	
}
    
   else if ($('#shown').hasClass('hide')){
    		$('input[type="text"], textarea').on('keyup',function() {
    	    
    	    var name = $("#nameEdit").val();
    	    var desc = $("#descEdit").val();
    	    var unit = $("#unitEdit").val();
    		
//    		if(name != '' && desc != '' && unit != '') {
//	        $('#submitEdit').attr('disabled', false);
//	    } else {
//	        $('#submitEdit').attr('disabled', true);
//	    }
    		
    		
    		
    });
    		
    		
    		
    }
    

    



});


function myParamCityInfo (kpi,level,category,city2,name,tableId,desc,unit,isManual,nbs){
	

	console.log("CITY: "+ city);
	var kpiId = kpi;
	var l= level;
	var c = category;
	var ct = city;
	console.log(c);
	var name = name;
	var id = tableId;
	var description= desc;
	var unitM = unit;
	var manual="citymanual";
	console.log("MANUAL " + manual);
	if(manual === "citymanual"){
		console.log("kpi manuale");
		$('#checkCityComputed')[0].checked = false;
		$('#autoSelect2').addClass("hide");
		
	}
	else {
		console.log("kpi automatico");
		$('#checkCityComputed')[0].checked= true;
		$('#autoSelect2').removeClass("hide");
		console.log("KPI ID : "+ kpiId);
		//getValueEngine(kpiId);
		getEngineKPIs();
	}
	var text1;
	var text2;
	if(lang==="en"){
		text1= edit.en;
		text2=wit.en;
	}
	else if (lang==="it"){
		text1= edit.it;
		text2=wit.it;
	}
	else if (lang==="fi"){
		text1 = edit.fi;
		text2=wit.fi;
	}
	else if (lang==="du"){
		text1= edit.du;
		text2=wit.du;
	}
	
	$(".title").text(text1 + name + " " + text2+ "id " + id);
	var oModal = $('#modal3');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(c);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('textarea[name="name"]').val(name);
	oModal.find('input[name="tableId"]').val(id);
	oModal.find('textarea[name="desc"]').val(description);
	oModal.find('input[name="unit"]').val(unitM);
	oModal.find('input[name="nbs"]').val(nbs);
	oModal.find('input[name="kpiName"]').val($("#auto2").val());
	
	
	oModal.find('input[name="kpiName"]').val(kpiName);
	//oModal.find('label').class('active');
	oModal.modal();
	//M.updateTextFields();
	
}




function getValueEngine(id){

	var input = new Object();
	input['id'] = id;
	$.ajax({
		async: false,
        type: "GET",
        url: "GetValueEngine",
        data: input,
        
        success: function(indicator){ 
            console.log("INDICATOR: " +indicator);
        	indicatorName = indicator;
	    }
	});
}
function getKPI(level, category, city, lang){
	console.log("CHIAMA FUNZIONE");
	var input = new Object();

if (level === "common"){
	level = "task";
}

console.log("CITY: " + city+ " LEVEL: " + level + " CATEGORY: "+ category);

    input['lang'] = lang;
	input['city'] = city;
	input['lev'] = level;
	input['category']= category;
	$('#myTable').empty();
	$.ajax({
		async: false,
        type: "POST",
        url: "GetGuestKPIList",
        data: input,
        success: function(json){
					data = JSON.parse(json);
					for (var i = 0; i< data.length; i++){
						console.log("WHATIF " + data[i].kpiLevel);
						if(data[i].kpiLevel=='common' || data[i].kpiLevel=='task'){
							$('#nbsTitle').addClass("hide");
						}
						else {
							$('#nbsTitle').removeClass("hide");
						}
						
						var row= $('<tr></tr>').appendTo('#myTable');
						row.addClass(data[i].icon+"Class");
						
						var id = $('<td  class="unalabTd"></td>').appendTo(row);
						id.text(data[i].tableId);

					

						var columnName = $('<td class="unalabTd"></td>').appendTo(row);
						
						var ahref2 = $('<a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip=""></a>').appendTo(columnName);
						ahref2.attr("data-tooltip", data[i].description);
						ahref2.tooltip();
						var infoIcon = $('<i class="tiny material-icons"></i>').appendTo(ahref2);
						infoIcon.text("info");
						var p = $('<p style="display:inline-block; margin-block-start: 0; margin-block-end: 0;"></p>').appendTo(columnName);
						p.text(data[i].name + " ");
						if(data[i].kpiLevel=='common' || data[i].kpiLevel=='task'){
							$('#nbsTitle').addClass("hide");
						}
						else {
							$('#nbsTitle').removeClass("hide");
							var nbs=$('<td class="unalabTd"></td>').appendTo(row);
							nbs.text(data[i].nbs);
						}
						

						var category=$('<td class="unalabTd"></td>').appendTo(row);
						var ahref = $('<a style="padding:0 4px !important" href=# class="btn-flat"></a>').appendTo(category);
						var catIcon = $('<i class="tiny material-icons"></i>').appendTo(ahref);
						catIcon.text(data[i].icon);
						
						var valueC =  $('<td class="unalabTd"></td>').appendTo(row);
						
						
						var tool = $('<a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip=""> </a>').appendTo(valueC);
						if(data[i].isManual === 'automatic'){
							tool.attr("data-tooltip", "value computed by data management tool");
							
						}
						else{
							tool.attr("data-tooltip", data[i].motivation);
							tool.tooltip();
						}
						
						var i2 = $('<i class="tiny material-icons"></i>').appendTo(tool);
						i2.text("info");

						var p = $('<p style="display:inline-block; margin-block-start: 0; margin-block-end: 0;"></p>').appendTo(valueC);
						p.text(data[i].value);
						
						

						var unit = $('<td class="unalabTd"></td>').appendTo(row);
						unit.text(data[i].unit);

						var date= $('<td class="unalabTd"></td>').appendTo(row);
						date.text(data[i].date);
						if(data[i].isManual === 'manual'|| data[i].isManual === 'citymanual'){
							var action1= $('<td class="unalabTd"></td>').appendTo(row);
							var a1 = $('<a style="padding:0 !important" onclick="" href="" class="tooltipped btn-flat view" data-position="top" data-tooltip="">').appendTo(action1);
							a1.attr("onclick", "dashboardParams('"+data[i].idKPI+"','"+data[i].idCity+"','"+data[i].isManual+"')");
							a1.attr("href", "#chart1");
							a1.attr("data-tooltip", "View indicator");
							a1.tooltip();
							var i3 = $('<i class="material-icons eyes" id="">').appendTo(a1);
							i3.attr("id", "icon_"+data[i].idKPI);
							i3.text("insert_chart");
						}
						else {
							var action1= $('<td class="unalabTd"></td>').appendTo(row);
							var a1 = $('<a style="padding:0 !important" onclick="" href="" class="tooltipped btn-flat view" data-position="top" data-tooltip="">').appendTo(action1);
							a1.attr("onclick", "dashboardParams('"+data[i].idKPI+"','"+data[i].idCity+"','"+data[i].isManual+"')");
							a1.attr("href", "#chart1");
							a1.attr("data-tooltip", "View indicator");
							a1.tooltip();
							var i3 = $('<i class="material-icons eyes" id="">').appendTo(a1);
							i3.attr("id", "icon_"+data[i].idKPI);
							i3.text("insert_chart");
						}
						
						
					}

					 $("#myTable").paginathing({
              
              			perPage: 10
            
					});


        }
	});
}


function getManagerKPI(level, category, city, lang){
	console.log("CHIAMA FUNZIONE");
	var input = new Object();

if (level === "common"){
	level = "task";
}

console.log("CITY: " + city+ " LEVEL: " + level + " CATEGORY: "+ category);

    input['lang'] = lang;
	input['city'] = city;
	input['level'] = level;
	input['category']= category;
	
	$('#myTable').empty();
	$.ajax({
		async: false,
        type: "POST",
        url: "GetKPIList",
        data: input,
        success: function(json){
					data = JSON.parse(json);
					console.log("ECCO I DATI: " + json);
					for (var i = 0; i< data.length; i++){
						
						var row= $('<tr></tr>').appendTo('#myTable');
						row.addClass(data[i].icon+"Class");
						
						var id = $('<td  class="unalabTd"></td>').appendTo(row);
						id.text(data[i].tableId);

						var columnName = $('<td class="unalabTd"></td>').appendTo(row);
						
						var ahref2 = $('<a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip=""></a>').appendTo(columnName);
						ahref2.attr("data-tooltip", data[i].description);
						ahref2.tooltip();
						var infoIcon = $('<i class="tiny material-icons"></i>').appendTo(ahref2);
						infoIcon.text("info");
						var p = $('<p style="display:inline-block; margin-block-start: 0; margin-block-end: 0;"></p>').appendTo(columnName);
						p.text(data[i].name + " (entityID="+data[i].entity+")");
						if(data[i].kpiLevel=='common' || data[i].kpiLevel=='task'){
							$('#nbsTitle').addClass("hide");
						}
						else{
							$('#nbsTitle').removeClass("hide");
							var nbs=$('<td class="unalabTd"></td>').appendTo(row);
							nbs.text(data[i].nbs);
						}
						

						var category=$('<td class="unalabTd"></td>').appendTo(row);
						var ahref = $('<a style="padding:0 4px !important" href=# class="btn-flat"></a>').appendTo(category);
						var catIcon = $('<i class="tiny material-icons"></i>').appendTo(ahref);
						catIcon.text(data[i].icon);


						var valueC =  $('<td class="unalabTd"></td>').appendTo(row);
						
						
						var tool = $('<a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip=""> </a>').appendTo(valueC);
						
						if(data[i].isManual === 'automatic'){
							tool.attr("data-tooltip", "value computed by data management tool");
						}
						else{
							tool.attr("data-tooltip", data[i].motivation);
						}
						tool.tooltip();
						var i2 = $('<i class="tiny material-icons"></i>').appendTo(tool);
						
						i2.text("info");

						var p = $('<p style="display:inline-block; margin-block-start: 0; margin-block-end: 0;"></p>').appendTo(valueC);
						p.text(data[i].value);
						
						
						
						
						

						var unit = $('<td class="unalabTd"></td>').appendTo(row);
						unit.text(data[i].unit);

						var date= $('<td class="unalabTd"></td>').appendTo(row);
						date.text(data[i].date);
						if(data[i].isManual === 'manual'){
							var action1= $('<td class="unalabTd"></td>').appendTo(row);
							var a1 = $('<a style="padding:0 !important" onclick="" href="" class="tooltipped btn-flat view" data-position="top" data-tooltip="">').appendTo(action1);
							a1.attr("onclick", "dashboardParams('"+data[i].idKPI+"','"+data[i].idCity+"','"+data[i].isManual+"')");
							a1.attr("href", "#chart1");
							a1.attr("data-tooltip", "View indicator");
							a1.tooltip();
							var i3 = $('<i class="material-icons eyes" id="">').appendTo(a1);
							i3.attr("id", "icon_"+data[i].idKPI);
							i3.text("insert_chart");
							var a2 = $('<a style="padding:0 !important" href="" id="btnMio" onclick="" class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="Set a new value"></a>').appendTo(action1);
							a2.tooltip();
							a2.attr("href", "#modal2");
							
							a2.attr("onclick", "myParam('"+data[i].idKPI+"','"+data[i].kpiLevel+"','"+data[i].kpiCategory+"','"+data[i].idCity+"','"+data[i].name+"','"+data[i].tableId+"')");
							var i4 = $('<i class="material-icons"></i>').appendTo(a2);
							
							i4.text("add_circle_outline");
						}
						else if (data[i].isManual === 'citymanual'){
							var action1= $('<td class="unalabTd"></td>').appendTo(row);
							var a1 = $('<a style="padding:0 !important" onclick="" href="" class="tooltipped btn-flat view" data-position="top" data-tooltip="">').appendTo(action1);
							a1.attr("onclick", "dashboardParams('"+data[i].idKPI+"','"+data[i].idCity+"', '"+data[i].isManual+"')");
							a1.attr("href", "#chart1");
							a1.attr("data-tooltip", "View indicator");
							a1.tooltip();
							var i3 = $('<i class="material-icons eyes" id="">').appendTo(a1);
							i3.attr("id", "icon_"+data[i].idKPI);
							i3.text("insert_chart");
							var a2 = $('<a style="padding:0 !important" href="" id="btnMio" onclick="" class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="Set a new value"></a>').appendTo(action1);
							a2.tooltip();
							a2.attr("href", "#modal2");
							a2.attr("onclick", "myParam('"+data[i].idKPI+"','"+data[i].kpiLevel+"','"+data[i].kpiCategory+"','"+data[i].idCity+"','"+data[i].name+"','"+data[i].tableId+"')");
							var i4 = $('<i class="material-icons">add_circle_outline</i>').appendTo(a2);
							var a3= $('<a style="padding:0 !important" href="" id="btnMio" onclick="" class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="Edit indicator"></a>').appendTo(action1);
							a3.tooltip();
							a3.attr("href", "#modal3");
							a3.attr("onclick", "myParamCityInfo('"+data[i].idKPI+"','"+data[i].kpiLevel+"','"+data[i].kpiCategory+"','"+data[i].idCity+"','"+data[i].name+"','"+data[i].tableId+"','"+data[i].description+"','"+data[i].unit+"','"+data[i].isManual+"', '"+data[i].nbs+"')");
							var i5=$('<i class="material-icons">edit</i>').appendTo(a3);	
						}
						else {
							var action1= $('<td class="unalabTd"></td>').appendTo(row);
							var a1 = $('<a style="padding:0 !important" onclick="" href="" class="tooltipped btn-flat view" data-position="top" data-tooltip="">').appendTo(action1);
							a1.attr("onclick", "dashboardParams('"+data[i].idKPI+"','"+data[i].idCity+"', '"+data[i].isManual+"')");
							a1.attr("href", "#chart1");
							a1.attr("data-tooltip", "View indicator");
							a1.tooltip();
							var i3 = $('<i class="material-icons eyes" id="">').appendTo(a1);
							i3.attr("id", "icon_"+data[i].idKPI);
							i3.text("insert_chart");
							var a2= $('<a style="padding:0 !important" href="" id="btnMio2" onclick="" class="tooltipped modal-trigger btn-flat" data-position="top" class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="Edit indicator"></a>').appendTo(action1);
							a2.tooltip();
							a2.attr("href", "#modal3");
							a2.attr("onclick", "myParamCityCalculatedInfo('"+data[i].idKPI+"','"+data[i].kpiLevel+"','"+data[i].idKnowage+"','"+data[i].kpiCategory+"','"+data[i].idCity+"','"+data[i].name+"','"+data[i].tableId+"','"+data[i].description+"','"+data[i].unit+"','"+data[i].isManual+"','"+data[i].nbs+"')")
							var i4 =$('<i class="material-icons">edit</i>').appendTo(a2);
						
						}
						
						
					}

					 $("#myTable").paginathing({
              
              			perPage: 10
            
					});
        }
	});
}

function myParamCityCalculatedInfo (kpi,level,idKnowage,category,city2,name,tableId,desc,unit,isManual,nbs){
	
	console.log("CITY: "+ city);
	console.log("KPI CALCOLATO NUMERO: " + kpi);
	var kpiId = kpi;
	var l= level;
	var c = category;
	var ct = city;
	console.log(c);
	var name = name;
	var id = tableId;
	var description= desc;
	var unitM = unit;
	var manual="automatic";
	console.log("MANUAL " + manual);
	if(manual === "citymanual"){
		console.log("kpi manuale");
		$('#checkCityComputed')[0].checked = false;
		$('#autoSelect2').addClass("hide");
		getEngineKPIs();
	}
	else {
		console.log("kpi automatico");
		$('#checkCityComputed')[0].checked= true;
		$('#autoSelect2').removeClass("hide");
		console.log("ID KPI CALCOLATO: " + kpiId);
		getValueEngine(kpiId);
		getEngineKPIs();

		
	}
	
	var text1;
	var text2;
	
	if(lang==="en"){
		text1= edit.en;
		text2=wit.en;
	}
	else if (lang==="it"){
		text1= edit.it;
		text2=wit.it;
	}
	else if (lang==="fi"){
		text1 = edit.fi;
		text2=wit.fi;
	}
	else if (lang==="du"){
		text1= edit.du;
		text2=wit.du;
	}
	
		$('select').formSelect();
	$(".title").text(text1 + name + " " + text2+ "id " + id);
	var oModal = $('#modal3');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(c);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('textarea[name="name"]').val(name);
	oModal.find('input[name="tableId"]').val(id);
	oModal.find('textarea[name="desc"]').val(description);
	oModal.find('input[name="unit"]').val(unitM);
	oModal.find('input[name="id_knowage"]').val(idKnowage);
	oModal.find('input[name="nbs"]').val(nbs);
	oModal.find('input[name="kpiName"]').val($('#auto2').val());
	//oModal.find('label').class('active');
	oModal.modal();
	//M.updateTextFields();
	
}

function myParamAdminInfo(kpi,level,category,city2,name,tableId,desc,unit,isManual,nbs){
	

	console.log("CITY: "+ 3);
	var kpiId = kpi;
	var l= level;
	var c = category;
	var ct = 3;
	console.log(c);
	var name = name;
	var id = tableId;
	var description= desc;
	var unitM = unit;
	var manual=isManual;
	console.log("MANUAL " + manual);
	var text1;
	var text2;
	if(lang==="en"){
		text1= edit.en;
		text2=wit.en;
	}
	else if (lang==="it"){
		text1= edit.it;
		text2=wit.it;
	}
	else if (lang==="fi"){
		text1 = edit.fi;
		text2=wit.fi;
	}
	else if (lang==="du"){
		text1= edit.du;
		text2=wit.du;
	}

	
	$(".title").text(text1 + name + " " + text2+ "id " + id);
	var oModal = $('#modal3');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(c);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('textarea[name="name"]').val(name);
	oModal.find('input[name="tableId"]').val(id);
	oModal.find('textarea[name="desc"]').val(description);
	oModal.find('input[name="unit"]').val(unitM);
	oModal.find('input[name="nbs"]').val(nbs);
	//oModal.find('label').class('active');
	oModal.modal();
	//M.updateTextFields();
	
}



//sort Table

function sortTable(n) {
	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	  table = document.getElementById("myTable2");
	  switching = true;
	  // Set the sorting direction to ascending:
	  dir = "asc"; 
	  /* Make a loop that will continue until
	  no switching has been done: */
	  while (switching) {
	    // Start by saying: no switching is done:
	    switching = false;
	    rows = table.rows;
	    /* Loop through all table rows (except the
	    first, which contains table headers): */
	    for (i = 1; i < (rows.length - 1); i++) {
	      // Start by saying there should be no switching:
	      shouldSwitch = false;
	      /* Get the two elements you want to compare,
	      one from current row and one from the next: */
	      
	    	  x = rows[i].getElementsByTagName("TD")[n];
	      
	    	  y = rows[i + 1].getElementsByTagName("TD")[n];
	      
	      
	      
	      
	      
	      if (n==0){
	    	  if (dir == "asc") {
	  	    	
		    		if (Number(x.innerHTML) > Number(y.innerHTML)) {
		        	
		    			// If so, mark as a switch and break the loop:
		    			shouldSwitch = true;
		    			break;
		    		}
		    		} else if (dir == "desc") {
		    			if (Number(x.innerHTML) < Number(y.innerHTML)) {
		    				// If so, mark as a switch and break the loop:
		    				shouldSwitch = true;
		    				break;
		    			}
		    		}
	      }
	      else{
	    	  if(n==6){
	    		  if($('#nbsTitle').hasClass("hide")){
	    			  n=5;
	    		  }
	    	  }
	    	  
	    	  else if(n==5){
	    		  if($('#nbsTitle').hasClass("hide")){
	    			  n=4;
	    		  }
	    	  }
	    	  
	    	  
	    	  if (dir == "asc") {
	  	    	
	    		  
	    		     
		    		if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
		        	
		    			// If so, mark as a switch and break the loop:
		    			shouldSwitch = true;
		    			break;
		    		}
		    		} else if (dir == "desc") {
		    			if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
		    				// If so, mark as a switch and break the loop:
		    				shouldSwitch = true;
		    				break;
		    			}
		    		}
	      }
	    }
	    if (shouldSwitch) {
	      /* If a switch has been marked, make the switch
	      and mark that a switch has been done: */
	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	      switching = true;
	      // Each time a switch is done, increase this count by 1:
	      switchcount ++; 
	    } else {
	      /* If no switching has been done AND the direction is "asc",
	      set the direction to "desc" and run the while loop again. */
	      if (switchcount == 0 && dir == "asc") {
	        dir = "desc";
	        switching = true;
	      }
	    }
	  }

}


$(document).ready(function() {

	$("#myTable").paginathing({
              
              perPage: 10
            
          });
	
	
	
	var url = new URL(window.location.href);
	console.log("URL: " + url);

	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	
	console.log("URL CITY: " +city);
	switch(city){
	case "1" :
		cityString = "Genova";
		break;
	case "2":
		cityString ="Tampere";
		break;
	case "3":
		cityString ="Eindhoven";
		break;
	case "Eindhoven":
		cityString ="Eindhoven";
		break;
	case "Genova":
		cityString ="Genova";
		break;
	case "Tempere":
		cityString ="Eindhoven";
		break;
	}
	console.log("CITY STRING " + cityString);
	
	$('#measureButton').attr("href","/cpm_v2/measure.jsp?city="+cityString);
	$('#formulaButton').attr("href","/cpm_v2/formula.jsp?city="+cityString);
	$('#scheduleButton').attr("href","/cpm_v2/schedule.jsp?city="+cityString);

$('input[type=radio][name=radio1').on("change", function () {
	
	var valore = $('input[name=radio1]:checked').val();
	var cat="";
	var url = new URL(window.location.href);
	console.log("URL: " + url);
	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	lang = url_params.get('lang');
	
	
	$("#chart1").addClass("hide");
	$('.eyes').text("insert_chart");
	console.log("VALORE " + valore);
if (valore === "common"){
	$("#collapsibleOne").show();
	$("#collapsibleTwo").hide();
	
}
else if (valore === "city" || valore === "project") {
	$("#collapsibleTwo").show();
	
	$("#collapsibleOne").hide();
}

  
  $('#pag').empty();

  
  	getKPI(valore,"all", city, lang);
  
	//getKPI(valore,"all");
	


});


$('input[type=radio][name=radio2').on("change", function () {
	console.log("sono qui dentro");
	//$(this).closest("form").submit();
	
	var valore = $('input[name=radio2]:checked').val();
	var cat="";
	var url = new URL(window.location.href);
	console.log("URL: " + url);
	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	lang = url_params.get('lang');
	
	
	$("#chart1").addClass("hide");
	$('.eyes').text("insert_chart");
	console.log("VALORE " + valore);
if (valore === "common"){
	$("#collapsibleOne").show();
	$("#collapsibleTwo").hide();
	
}
else if (valore === "city" || valore === "project") {
	$("#collapsibleTwo").show();
	
	$("#collapsibleOne").hide();
}

  
  $('#pag').empty();

  
  
 
	getManagerKPI(valore,"all", city, lang);
  
	//getKPI(valore,"all");
	


});


$('.event-type-filter-one').on("change",function(){
	  console.log("sono qui");
	  $("#myTable").paginathing({
          perPage: 99999999999
      });
   	  $('.pagination').hide();
	  
		var id= $(this).prop('id');
		
	  console.log(id+" "+$(this).prop("checked"));
	  
	  
		if($(this).prop("checked")){
			
			var cardCol = $("#transitCard ."+id+"Class").detach();
			console.log("CARDONE: "+cardCol);
			cardCol.prependTo("#myTable");
			
			console.log("CARD COL "+cardCol);
			
			
		}  
		else{
			console.log("else")

			var cardCol = $("."+id+"Class").detach();
			cardCol.appendTo("#transitCard");
			
			
		}
		

		$("#myTable").paginathing({
            perPage: 10
        });
		
	});


	$('.event-type-filter-two').on("change",function(){
	  console.log("sono qui");
	  $("#myTable").paginathing({
          perPage: 99999999999
      });
   	  $('.pagination').hide();
	  
		var id= $(this).prop('id');
		
	  console.log(id+" "+$(this).prop("checked"));
	  
	  
		if($(this).prop("checked")){
			
			var cardCol = $("#transitCard ."+id+"Class").detach();
			
			cardCol.prependTo("#myTable");
			
			console.log("CARD COL "+cardCol);
			
			
		}  
		else{
			console.log("else")

			var cardCol = $("."+id+"Class").detach();
			cardCol.appendTo("#transitCard");
			
			
		}
		

		$("#myTable").paginathing({
            perPage: 10
        });
		
	});
	
	
	
	
	
	$("#submitEdit").click(function(event){
		
		var error_free=true;
		
			var element=$("#nameEdit");
			console.log("ELEMENT: " + element);
			var valid=element.hasClass("valid");
			var error_element=$("span", element.parent());
			if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
			else{error_element.removeClass("error_show").addClass("error");}
		
		if (!error_free){
			event.preventDefault(); 
			console.log("SONO NELL IF");
		}
		else{
			alert('No errors: Form will be submitted');
			console.log("SONO NELL ELSE");
		}
	});

});

var data;
var labelname;

function getKPIName(idKpi){
	var input = new Object();
	input['idKpi']= idKpi;
	$.ajax({
		async:false,
		type: "GET",
		 url: "GetKPIName",
	        data: input,
	        success: function(kpiName){
	        	labelname=kpiName;
	        	console.log(labelname);
	        }
			
	});
	
	$.ajax({
		async:false,
		type: "GET",
		 url: "GetKPIUnit",
	        data: input,
	        success: function(kpiUnit){
	        	labelname=labelname+" ("+kpiUnit+")";
	        	console.log(labelname);
	        }
			
	});
	
	
	return labelname;
}



function generateData(idKpi,city,type) {
    
	getKPIName(idKpi);
	console.log("LABEL NAME: " + labelname);
	
	var ctx = document.getElementById('chart1').getContext('2d');
	ctx.clearRect(0, 0, ctx.width, ctx.height);
	ctx.width = 600;
	ctx.height = 400;

    dataArray=[];
    timeArray=[];
    
	var color = 'green';
	
	var timeUnit = $('#timeUnit').val();
	
	if(globalChart) {
		globalChart.destroy();
	}

	var chart = new Chart (ctx, {
		data: {
			labels: timeArray,
			datasets: [{
				label: labelname,
				backgroundColor: 'green',
				borderColor: 'green',
				data: dataArray,
				type: 'line',
				pointRadius: 0,
				fill: false,
				lineTension: 0,
				borderWidth: 2
			}]
		},
		options: {
			animation: {
				duration: 0
			},
			scales: {
				xAxes: [{
					type: 'time',
					distribution: 'series',
					offset: true,
					time: {
						unit: timeUnit
					}				
				}],
				yAxes: [{
					gridLines: {
						drawBorder: false
					},
					scaleLabel: {
						display: true,
						labelString: 'KPI Values'
					}
				}]
			},
			
			tooltips: {
				intersect: false,
				mode: 'index',
				callbacks: {
					label: function(tooltipItem, myData) {
						var label = myData.datasets[tooltipItem.datasetIndex].label || '';
						if (label) {
							label += ': ';
						}
						label += parseFloat(tooltipItem.yLabel).toFixed(2);
						return label;
					}
				}
			}
		}
	})
	    
	var input = new Object();
	input['idKpi'] = idKpi;
	input['city'] = city;
	input['type'] = type;
	
	
	$.ajax({
		async: false,
        type: "GET",
        url: "GetChartData",
        data: input,
        success: function(chartData){
        	
        	var json = JSON.parse(chartData);
    	  	console.log(json);
    	  	$("#body1").empty();
    	  	
    	  	$("#filterFrom").datepicker( { 
    	  		format: 'yyyy-mm-dd',
    	  		showClearBtn: true
    	  	});
    	  	$("#filterTo").datepicker( { 
    	  		format: 'yyyy-mm-dd',
    	  		showClearBtn: true
    	  	});  
    	  	
    	  	for (var i in json){
    	  		
    	  		var stringDate = (json[i].x).split(" ")[0];
    	  		
    	  		if(!isInDateRange(stringDate)) {
    	  			continue;
    	  		}
    	  		
    	  		timeArray.push(json[i].x);
    	  		dataArray.push(json[i].y);
    	  		chart.update();    	  		
    	  		
    	  		var tr = $("<tr></tr>").appendTo("#body1");
    	  		var td =  $("<td></td>").appendTo(tr);
    	  		td.text(json[i].x);
    	  		var td2 = $("<td></td>").appendTo(tr);
    	  		td2.text((json[i].y).toFixed(2));
    	  		var td3 = $("<td></td>").appendTo(tr);
    	  		td3.text(json[i].z);	
    	  	}
    	  	calculateVariance(dataArray);
    	  	calculateMin(dataArray);
    	  	calculateMax(dataArray);
    	  	calculateAvg(dataArray);
    	  	
    	  	globalChart = chart;
   	
        }
        });
	
}

$('#timeUnit').change(function(){
	generateData(current.idKpi,current.city,current.type);
	$("#chart1").show();
});

//chart.update();
//var cfg = 
//		
		
		
		//}


//var chart = new Chart(ctx, cfg);
//chart.update();
	




