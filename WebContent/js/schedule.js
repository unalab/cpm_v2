var cityString;
$(document).ready(function() {
	$('.dropdown-button').dropdown();
	var url = new URL(window.location.href);
	console.log("URL: " + url);

	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	
	console.log("URL CITY: " +city);
	switch(city){
	case "1" :
		cityString = "Genova";
		break;
	case "2":
		cityString ="Tampere";
		break;
	case "3":
		cityString ="Eindhoven";
		break;
	case "Eindhoven":
		cityString ="Eindhoven";
		break;
	case "Genova":
		cityString ="Genova";
		break;
	case "Tempere":
		cityString ="Eindhoven";
		break;
	}
	console.log("CITY STRING " + cityString);
	
	$("#titleCity").text("City Performance Monitor - "+ cityString);
	getKPIs();
	$('.tooltipped').tooltip();
	$('.modal').modal();
	$('.datepicker').datepicker({
		minDate: 0,
		format: 'dd/mm/yy'
	});
	$('.timepicker').timepicker({
		
		twelvehour: false,
		format: 'HH:ii:SS'
	});
	$('select').formSelect();
    
});

$('input[type=radio][name=radio-group').on("change", function () {
	var valore = $('input:radio[name=radio-group]:checked').val();
	console.log(valore);
	if(valore==="picker"){
		$('#date').removeClass("hide");
		$('#time').removeClass("hide");
	}
	else{
		$('#date').addClass("hide");
		$('#time').addClass("hide");
	}
});

function schedule(id,formula,name){
	emptyValues();
	$('#trigger-name').text("Schedule KPI: "+name);
	//$('#modalSched').modal();
	$("#subScheduleForm").click(function(){    
		var valore = $('input:radio[name=radio-group]:checked').val();
		console.log(valore);
		var date,time;
		if(valore==="picker"){
			date = $('.datepicker').val();
			time = $('.timepicker').val();
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;
			time = sHours + ":" + sMinutes+":00";
			console.log(date+" " +time);
		}
		
		var num = $('#num').val();
		var interval = $('#timeSelect').val();
//		$('#time').on("change",function(evt){
//			console.log(evt);
//			//interval = evt.target.value;
//		});
		console.log(num+" "+interval);
		
		var input = new Object();
		input['id_formula'] = id;
		input['start'] = valore;
		input['date'] = date;
		input['time'] = time;
		input['num'] = num;
		input['interval'] = interval;
		$.ajax({
			async: false,
			type: "POST",
			url: "saveJob",
			data: input,
			success: function(idForm){
				getKPIs();
				alert("Job saved!");
				startJobs(idForm,"false",valore,num,interval,date,time);			
			}
		});	
		
	});
	
}

function stopJob(id){
	var input = new Object();
	input['id_formula'] = id;
	
	$.ajax({
		async: false,
        type: "POST",
        url: "interrupt",
        data: input,
        success: function(){ 
        	alert ("JOB STOPPED!");
        	$("#icon" + id).text("play_arrow");
        	$("#link" + id).attr("data-tooltip", "Play KPI calculation");
        	$("#link" + id).attr("onclick", "startJobs("+id+", true)");
        }
	});
	
}
function startJobs(id,replay,start,num,interval,date,time){
	var input = new Object();
	input['id'] = id;
	input['replay'] = replay;
	input['start'] = start;
	input['num'] = num;
	input['interval'] = interval;
	input['date'] = date;
	input['time'] = time;
	$.ajax({
		async: false,
        type: "POST",
        url: "jobs",
        data: input,
        success: function(){ 
        	console.log("Started");
        	$("#icon" + id).text("stop");
			$("#link" + id).attr("data-tooltip", "Stop KPI calculation");
			$("#link" + id).attr("onclick", "stopJob("+id+")");
        }
	});
        
}

function getKPIs(){
	var input = new Object();
	input['city'] = cityString;
	$.ajax({
		async: false,
        type: "GET",
        data: input,
        url: "getKPIs",
        
        success: function(data){ 
        	console.log(data);
        	json = JSON.parse(data);
        	console.log(json);
        	$('#kpis').empty();
        	$('<li class="collection-header"><h4>KPI Scheduler</h4></li>').appendTo('#kpis');
        	console.log(json.length);
        	for (i=0; i<json.length; i++){
        		console.log(json[i]);
        		
        		var li = $('<li class="collection-item"></li>').appendTo('#kpis');
        		var div = $('<div></div>').appendTo(li);
        		
        		div.text(json[i].name);
        		console.log(json[i].name);
        		var a = $('<a  class="tooltipped secondary-content modal-trigger" data-position="top"></a>').appendTo(div);
        		var icon = $('<i class="material-icons"></i>').appendTo(a);
        		var status = json[i].status;
        		//console.log(json[i]['status']);
        		if(status == "NOT SCHEDULED"){
        			icon.text("access_time");
        			a.attr("data-tooltip", "Schedule KPI calculation");
        			$('.tooltipped').tooltip();
        			a.attr("href", "#modalSched");
        			a.attr("onclick", "schedule("+json[i].id+",'"+json[i].formula+"','"+json[i].name+"')");
        			
        		}
        		else if(status == "PLAYED"){
        			console.log("SONO PLAYED, DEVO METTERE L'ICONA DI STOP");
        			icon.attr("id", "icon" + json[i].id);
        			icon.text("stop");
        			a.attr("id", "link" + json[i].id);
        			a.attr("href", "#!");
        			a.attr("data-tooltip", "Stop KPI calculation");
        			a.attr("onclick", "stopJob("+json[i].id+")");
        			var a2 = $('<a href="#!" class="tooltipped secondary-content" data-position="top"></a>').appendTo(div);
            		var icon2 = $('<i class="material-icons"></i>').appendTo(a2);
            		icon2.text("edit");
            		a2.attr("data-tooltip", "Edit KPI schedulation");
        			a2.attr("onclick", "editScheduling("+json[i].id+",'"+json[i].name+"')");
            		
        		}
        		else if (status == "STOPPED"){
        			console.log("SONO STOPPED, DEVO METTERE L'ICONA DI PLAY");
        			icon.attr("id", "icon" + json[i].id);
        			icon.text("play_arrow");
        			a.attr("id", "link" + json[i].id);
        			a.attr("href", "#!");
        			a.attr("data-tooltip", "Play KPI calculation");
        			a.attr("onclick", "startJobs("+json[i].id+", true)");
        			var a2 = $('<a href="#!" class="tooltipped secondary-content" data-position="top"></a>').appendTo(div);
            		var icon2 = $('<i class="material-icons"></i>').appendTo(a2);
            		icon2.text("edit");
            		a2.attr("data-tooltip", "Edit KPI schedulation");
        			a2.attr("onclick", "editScheduling("+json[i].id+",'"+json[i].name+"')");
        		}
        		
        		
        	}
        	
        	
        }
        });
}

function editScheduling(id, name) {
	var input = new Object();
	input['id'] = id;
	$.ajax({
		async: false,
        type: "GET",
        url: "getJob",
        data: input,
        success: function(data){
        	var job = JSON.parse(data);
        	$('#trigger-name').text("Edit scheduled KPI: "+name);
        	$('#modalSched').modal('open');
        	if(job.start != "now") {
        		$('#picker').prop('checked', true);
        		$('#date').removeClass("hide");
        		$('#time').removeClass("hide");
        	} else {
        		$('#now').prop('checked', true);
        		$('#date').addClass("hide");
        		$('#time').addClass("hide");
        	}
        	var options = {day: 'numeric', month: 'numeric', year: 'numeric'};
        	var date = new Date(job.date);
        	date = date.toLocaleDateString('it-IT', options);
        	$('.datepicker').val(date);
        	if(job.time){
            	$('.timepicker').val(job.time.substring(0,5));
        	}
        	$('#num').val(job.num);
        	$('#timeSelect').val(job.interval);
        	
        	$("#subScheduleForm").click(function(){    
        		var valore = $('input:radio[name=radio-group]:checked').val();
        		console.log(valore);
        		var date,time;
        		if(valore==="picker"){
        			date = $('.datepicker').val();
        			time = $('.timepicker').val();
        			var hours = Number(time.match(/^(\d+)/)[1]);
        			var minutes = Number(time.match(/:(\d+)/)[1]);
        			var AMPM = time.match(/\s(.*)$/)[1];
        			if(AMPM == "PM" && hours<12) hours = hours+12;
        			if(AMPM == "AM" && hours==12) hours = hours-12;
        			var sHours = hours.toString();
        			var sMinutes = minutes.toString();
        			if(hours<10) sHours = "0" + sHours;
        			if(minutes<10) sMinutes = "0" + sMinutes;
        			time = sHours + ":" + sMinutes+":00";
        			console.log(date+" " +time);
        		}
        		
        		var num = $('#num').val();
        		var interval = $('#timeSelect').val();
//        		$('#time').on("change",function(evt){
//        			console.log(evt);
//        			//interval = evt.target.value;
//        		});
        		console.log(num+" "+interval);
        		
        		var input = new Object();
        		input['id'] = job.id;
        		input['replay'] = "true";
        		input['start'] = valore;
        		input['date'] = date;
        		input['time'] = time;
        		input['num'] = num;
        		input['interval'] = interval;
        		input['id_formula'] = id;
        		$.ajax({
        			async: false,
        			type: "POST",
        			url: "saveJob",
        			data: input,
        			success: function(){
        				//getKPIs();
        				alert("Job saved!");
        				startJobs(id,"true",valore,num,interval);
        			}
        		});	
        		
        	});
        }
	});
}

function emptyValues() {
	$('#now').prop('checked', true);
	$('.datepicker').val(null);
	$('.timepicker').val(null);
	$('#num').val(null);
	$('#timeSelect').val("minutes");
}