var htmlEditor;
var id;
var nameId;
var number = 0;
var currentLi = 0; 
var valid=false;
var unalabDB;
var cityString;
var idCPM=0;
var urlAjax = "";

function showUnalabDB(){
    
	var input = new Object();
	console.log("URL: " + DBurl);
	input['url'] = DBurl;
	
	$.ajax({
		async: false,
		type: "GET",
		url: "showDB",
		data: input,
		success: function(data){ 
			$('#unalabDB'+number).empty();
			var json = JSON.parse(data);
			console.log(json);
			var val = json[0];
			var op = $('<option></option>').appendTo('#unalabDB'+number);
			op.attr("value", json[0]);
			op.text(json[0]);
			
			for (i=1; i<json.length; i++){
				
				
				
				
					var op2 = $('<option></option>').appendTo('#unalabDB'+number);
					op2.attr("value", json[i]);
					op2.text(json[i]);
					
			}
			showMeas(val);
			$('select').formSelect();
		}
		});
}


$('#connector'+number).change(function(evt){
	//console.log("NUMBER:" + number);
	var value = evt.target.value;
	
	switch(value){
	case 'mongo':		
		htmlEditor.setOption("mode","javascript");
		htmlEditor.setValue('//example query\ndb.collection.find();');
		$('.formSql').addClass("hide");
		$('.formInflux').addClass("hide");
		$('.formMongo').removeClass("hide");
		$('.formUnalab').addClass("hide");
		$('.formRest').addClass("hide");
		$('.code-container').removeClass("hide");
		$('#headerToggle').addClass("hide");
		break;
		
	case 'influx':
		htmlEditor.setOption("mode","sql"); 
		htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n\nWHERE');
		$('.formSql').addClass("hide");
		$('.formInflux').removeClass("hide");
		$('.formMongo').addClass("hide");
		$('.formUnalab').addClass("hide");
		$('.formRest').addClass("hide");
		$('.code-container').removeClass("hide");
		$('#headerToggle').addClass("hide");
		break;
		
	case 'sql':
		htmlEditor.setOption("mode","sql");
		htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n\nWHERE');
		$('.formSql').removeClass("hide");
		$('.formInflux').addClass("hide");
		$('.formMongo').addClass("hide");
		$('.formUnalab').addClass("hide");
		$('.formRest').addClass("hide");
		$('.code-container').removeClass("hide");
		$('#headerToggle').addClass("hide");
		break;
	
	case 'unalab':
		htmlEditor.setOption("mode","sql");
		htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n\nWHERE');
		showUnalabDB();
		
		$('.formSql').addClass("hide");
		$('.formInflux').addClass("hide");
		$('.formMongo').addClass("hide");
		$('.formUnalab').removeClass("hide");
		$('.formRest').addClass("hide");
		$('.code-container').removeClass("hide");
		$('#headerToggle').addClass("hide");
		break;
		
	case 'rest':		
		$('.formSql').addClass("hide");
		$('.formInflux').addClass("hide");
		$('.formMongo').addClass("hide");
		$('.formUnalab').addClass("hide");
		$('.formRest').removeClass("hide");
		$('.code-container').addClass("hide");
		$('#headerToggle').removeClass("hide");
		break;
}
	
});


function showMeas(database){
	unalabDB = database;
	console.log(database);
	var input = new Object();
	input['database'] = database;
	input['url'] = DBurl;
	$.ajax({
		async: false,
		type: "GET",
		url: "showMeas",
		data: input,
		success: function(data){
			$('#unalabDBMeas'+number).empty();
			var json = JSON.parse(data);
			for (i=0; i<json.length; i++){
				
				
				if(i==0){
					var op = $('<option selected></option>').appendTo('#unalabDBMeas'+number);
					htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n'+json[i]+'\nWHERE');
				}
				else{
					var op = $('<option></option>').appendTo('#unalabDBMeas'+number);
				}
				op.attr("value", json[i]);
				op.text(json[i]);
			}
			$('select').formSelect();
		}
		});
}

$('#unalabDB'+number).change(function(evt){
	
	var value = evt.target.value;
	console.log("VALUE DB: " + value);
	
	showMeas(value);
});


$('#unalabDBMeas'+number).change(function(evt){
	
	var value = evt.target.value;
	console.log("VALUE MIS: " + value);
	htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n'+value+'\nWHERE');
	
});


$(document).ready(function() {
	$('.modal').modal();
    $('select').formSelect();
    $('.dropdown-button').dropdown();
	$('.collapsible').collapsible();
	
	var url = new URL(window.location.href);
	console.log("URL: " + url);

	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	idCPM = url_params.get("idCPM");
	console.log("URL CITY: " +city);
	switch(city){
	case "1" :
		cityString = "Genova";
		break;
	case "2":
		cityString ="Tampere";
		break;
	case "3":
		cityString ="Eindhoven";
		break;
	case "Eindhoven":
		cityString ="Eindhoven";
		break;
	case "Genova":
		cityString ="Genova";
		break;
	case "Tempere":
		cityString ="Eindhoven";
		break;
	}
	console.log("CITY STRING " + cityString);
	
	$("#titleCity").text("City Performance Monitor - "+ cityString);
	
	$("#measureCard").attr("href", "measure.jsp?city="+cityString+"&idCPM="+idCPM);
	$("#formulaCard").attr("href", "formula.jsp?city="+cityString+"&idCPM="+idCPM);
	$("#scheduleCard").attr("href", "schedule.jsp?city="+cityString+"&idCPM="+idCPM);
	
	$("#measure0").attr("href", "measure.jsp?city="+cityString+"&idCPM="+idCPM);
	$("#formula0").attr("href", "formula.jsp?city="+cityString+"&idCPM="+idCPM);
	$("#schedule0").attr("href", "schedule.jsp?city="+cityString+"&idCPM="+idCPM);
	
	$("#cardMeasure").attr("href","/cpm_v2/measure.jsp?city="+cityString);
	$("#cardFormula").attr("href","/cpm_v2/formula.jsp?city="+cityString);
    $('#code0').text("/* example query */\nSELECT\n\nFROM\n\nWHERE");
    
    htmlEditor = CodeMirror.fromTextArea(document.getElementById("code0"), {
    	lineNumbers: true,
    	mode: 'sql',
    	theme: 'default'
    });
    
    
    
    htmlEditor.refresh();
    
 
  });

function checkName(measure){
	var input = new Object();
	input['name'] = measure;
	$.ajax({
		async: false,
        type: "POST",
        url: "check",
        
        data: input,
        
        success: function(response){ 
        	
        	if (response === "1" || response === "-1"){
        		
        		valid = false;
        	}
        	
        	else if (response === "0" ){
        		
        		valid = true;
        	}
        	
        	
        }
        });
}

function save(){
	
	var measureName = $('#measureName'+number).val();
	var value = $('#connector'+number).val();
	var url;
	var username = "";
	var password = "";
	var query = "";
	var db;
	var method = "";
	var body = null;
	var headers = {};
	checkName(measureName);
	
	
	if(valid){
		
		
		switch(value){
		case "sql":
			url = $('#url'+number).val();
			username = $('#username'+number).val();
			password = $('#password'+number).val();
			query = htmlEditor.getValue();
			break;
		case "mongo":
			url = $('#urlMongo').val();
			break;
		case "influx":
			url = $('#urlInflux'+number).val();
			username = $('#usernameInflux'+number).val();
			password = $('#passwordInflux'+number).val();
			query = htmlEditor.getValue();
			break;
		case "unalab":
			var db2 = $('#unalabDB'+ number).val();
			url = DBurl+"/"+db2+"/";
			query = htmlEditor.getValue();
			break;
		case "rest":
			url = $('#urlRest'+number).val();
			query = $('#pathRest'+number).val();
			if(method == "POST") {
				body = $('#bodyRest'+number).val();
			}
			$('#headerBody div').each(function(i){
				var name = $(this).children(":nth-child(1)").attr("value");
				var value = $(this).children(":nth-child(2)").attr("value");
				headers[name] = value;
			});
			urlAjax = "restConnect";
			break;
		}
	
		console.log(url+", "+username + ", " + password + ", " + query+ ", "+ measureName);
	
		var input = new Object();
	
		input['url'] = url;
		input['username']=username;
		input['password']=password;
		input['query']=query;
		input['value'] = value;
		input['measure'] = measureName;
		input['city'] = cityString;
		if(urlAjax == "restConnect") {
			input['method']=method;
			input['body']=body;
			input['headers']=JSON.stringify(headers);
			input['case']=urlAjax;
		}
		
	
		$.ajax({
			async: false,
			type: "POST",
			url: "save",
			data: input,
			success: function(){ 
				if($(".customOpen").hasClass("active")){
					$(".collapsible-body").css("display","none");
				}
        	
				alert("Measure saved!");
				$('.saveButton').addClass("hide");
				$('#name'+number).text(measureName);
				$('#addMeasure').removeClass("hide");
			}	
		});
	}
	else {
		alert ("Name already existing or empty name");
	}
}


function addMeasure(){
	$('#addMeasure').addClass("hide");
	number = number +1; 
	var li = $('<li class="active customOpen"></li>').appendTo('.collapsible');
	
	var head = $('<div class="collapsible-header"></div>').appendTo(li);
	var icon = $('<i class="material-icons"></i>').appendTo(head);
	icon.text("keyboard_arrow_right");
	var aName = $('<a class="measure" name="987"/></a>').appendTo(head);
	aName.attr("id","name" + number);
	aName.text("New Measure");
	var body = $('<div class="collapsible-body"></div>').appendTo(li);
	var row = $('<div class="row"></div>').appendTo(body);
	var input = $('<div class="input-field col s2"></div>').appendTo(row);
	var select = $('<select id=""></select>').appendTo(input);
	select.attr("id", "connector"+number);
	var option1 = $('<option value="mongo" ></option>').appendTo(select);
	option1.text("MongoDB");
	var option2=$('<option value="sql" selected></option>').appendTo(select);
	option2.text("MySQL");
	var option3=$('<option value="influx"></option>').appendTo(select);
	option3.text("InfluxDB");
	var option4=$('<option value="unalab"></option>').appendTo(select);
	option4.text("UNaLab DB");
	var label = $('<label></label>').appendTo(input);
	label.text("Select Data Source");
	
	var divUnalab = $('<div class="input-field col s5 formUnalab hide"></div>').appendTo(row);
	divUnalab.attr("id","formUnalab1"+number);
	var selectUnalab = $('<select></select>').appendTo(divUnalab);
	selectUnalab.attr("id","unalabDB"+number);
	var labelUnalab = $('<label></label>').appendTo(divUnalab);
	labelUnalab.text("Select database");
	
	
	
	var divUnalab2 = $('<div class="input-field col s5 formUnalab hide">').appendTo(row);
	divUnalab2.attr("id","formUnalab2"+number);
	var selectUnalab2 = $('<select></select>').appendTo(divUnalab2);
	selectUnalab2.attr("id","unalabDBMeas"+number);
	var labelUnalab2 = $('<label></label>').appendTo(divUnalab2);
	labelUnalab2.text("Select measurement");
	
	
	showUnalabDB();
	
	var div1 = $('<div class="input-field col s5 formSql" id=""></div>').appendTo(row);
	div1.attr("id", "formSql1"+number);
	var input1 =$('<input value="jdbc:mysql://datasourceUrl:port/dbname" id="" name="url" type="text" class="validate sqlUrl">').appendTo(div1);
	input1.attr("id", "url"+number);
	var lab= $('<label class="active" for="url"></label>').appendTo(div1);
	lab.text("Data Source URL");
	
	var div2 = $('<div class="input-field col s2 formSql" id="" ></div>').appendTo(row);
	div2.attr("id", "formSql2"+number);
	var input2 =$('<input value="" id="" type="text" name="username" class="validate sqlUser">').appendTo(div2);
	input2.attr("id", "username"+number);
	var lab2= $('<label class="active" for="username"></label>').appendTo(div2);
	lab2.text("Username");
	
	var div3 = $('<div class="input-field col s2 formSql" id=""></div>').appendTo(row);
	div3.attr("id", "formSql3"+number);
	var input3 =$('<input value="" id="" name="password" type="password" class="validate sqlPass">').appendTo(div3);
	input3.attr("id", "password"+number);
	var lab3= $('<label class="active" for="password"></label>').appendTo(div3);
	lab3.text("Password");
	
	var div4 = $('<div class="input-field col s5 formMongo hide" id=""></div>').appendTo(row);
	div4.attr("id", "formMongo"+number);
	var input4 =$('<input value="jdbc:mongodb://datasourceUrl:port/dbname" id="" name="urlMongo" type="text" class="validate mongoUrl">').appendTo(div4);
	input4.text("id", "urlMongo"+number);
	var lab4= $('<label class="active" for="urlMongo"></label>').appendTo(div4);
	lab4.text("Data Source URL");
	
	var div5 = $('<div class="input-field col s5 formInflux hide" id="">').appendTo(row);
	div5.attr("id", "formInflux1"+number);
	var input5 =$('<input value="http://datasourceUrl:port/dbname" id="" name="urlInflux" type="text" class="validate influxUrl">').appendTo(div5);
	input5.attr("id", "urlInflux"+number);
	var lab5= $('<label class="active" for="urlInflux"></label>').appendTo(div5);
	lab5.text("Data Source URL");
	
	var div6 = $('<div class="input-field col s2 formInflux hide" id="">').appendTo(row);
	div6.attr("id", "formInflux2"+number);
	var input6 =$('<input value="" id="" type="text" name="usernameInflux" class="validate influxUser">').appendTo(div6);
	input6.attr("id", "usernameInflux"+number);
	var lab6= $('<label class="active" for="usernameInflux"></label>').appendTo(div6);
	lab6.text("Username");
	
	var div7 = $('<div class="input-field col s2 formInflux hide" id="">').appendTo(row);
	div7.attr("id", "formInflux3"+number);
	var input7 =$('<input value="" id="" type="password" name="passwordInflux" class="validate influxPass">').appendTo(div7);
	input7.attr("id", "passwordInflux"+number);
	var lab7= $('<label class="active" for="passwordInflux"></label>').appendTo(div7);
	lab7.text("Password");
	
	var cc = $('<div class="code-container"></div>').appendTo(body);
	var textarea= $('<textarea id=""></textarea>').appendTo(cc);
	textarea.attr("id","code"+number);
	var row2= $('<div class="row"></div>').appendTo(body);
	var aa = $('<a class="btn btn-right" onclick=""></a>').appendTo(row2);
	aa.attr("onclick","execute()");
	aa.text("Execute Query");
	
	var table= $('<table cellpadding="1" cellspacing="1" class="table table-hover sortable-table indexed tablesorter" id=""></table>').appendTo(body);
	table.attr("id", "result"+number);
	var thead = $('<thead></thead>').appendTo(table);
	var tr1 = $('<tr id=""></tr>').appendTo(thead);
	tr1.attr("id", "dynTableHead" + number);
	var tbody = $('<tbody id=""></tbody>').appendTo(table);
	tbody.attr("id", "resultTable" + number);
	$('<br />').appendTo(body);
	var saveButton = $('<div class="row saveButton hide" ></div>').appendTo(body);
	var aButton = $('<a id="" class="waves-effect waves-light btn modal-trigger" href="" onclick=""></a>').appendTo(saveButton);
	aButton.attr("href", "#modal1");
	aButton.attr("onclick", "getId(this.id)");
	aButton.text("Save");
	
	var modal = $('<div class="modal" id=""></div>').appendTo("body");
	modal.attr("id","modal"+number);
	var modalCont = $('<div class="modal-content"></div>').appendTo(modal);
	var inp = $('<div class="input-field col s3 "></div>').appendTo(modalCont);
	var inp2 = $('<input value="" type="text" class="validate measureName">').appendTo(inp);
	inp2.attr("id","measure"+number);
	var lab = $('<label class="active" for="measure"></label>').appendTo(inp);
	lab.text("Insert a name for this measure");
	var foot = $('<div class="modal-footer"></div>').appendTo(modalCont);
	var end = $('<a class="modal-close waves-effect waves-green btn-flat"></a>').appendTo(foot);
	end.attr("onclick", "save()");
	end.text("OK");
	
	inizialize();
	
	
}
function execute(){
	
	var value = $('#connector'+number).val();
	var url;
	var username = "";
	var password = "";
	var query = "";
	
	var method = "";
	var body = null;
	var headers = {};
	
	switch(value){
	case "sql":
		url = $('#url'+number).val();
		username = $('#username'+number).val();
		password = $('#password'+number).val();
		query = htmlEditor.getValue();
		urlAjax = "sqlConnect";
		break;
	case "mongo":
		url = $('#urlMongo'+number).val();
		break;
	case "influx":
		url = $('#urlInflux'+number).val();
		username = $('#usernameInflux'+number).val();
		password = $('#passwordInflux'+number).val();
		query = htmlEditor.getValue();
		urlAjax = "influxConnect";
		break;
	case "unalab":
		url = DBurl;
		query = htmlEditor.getValue();
		urlAjax = "unalabConnect";
		break;
	case "rest":
		url = $('#urlRest'+number).val();
		query = $('#pathRest'+number).val();
		method = $('#formRest30').find(":selected").val();
		if(method == "POST") {
			body = $('#bodyRest'+number).val();
		}
		$('#headerBody div').each(function(i){	
			var name = $(this).children(":nth-child(1)").attr("value");
			var value = $(this).children(":nth-child(2)").attr("value");
			headers[name] = value;
		});
		urlAjax = "restConnect";
		break;
	}
	
	console.log(url+", "+username + ", " + password + ", " + query);
	
	var input = new Object();
	
	input['url'] = url;
	input['username']=username;
	input['password']=password;
	input['query']=query;
	input['value']=value;
	
	if(urlAjax == "restConnect") {
		input['method']=method;
		input['body']=body;
		input['headers']=JSON.stringify(headers);
	}
	
	if(value == "unalab"){
		input['db'] =unalabDB;
	}
	
	$.ajax({
		async: false,
        type: "POST",
        url: urlAjax,
        
        data: input,
        
        success: function(data){ 
        	console.log(data);
        	
        	if(urlAjax == "restConnect"){
            	createJSTree(data);
            	$('.saveButton').removeClass("hide");
        		return;
        	}
        	
        	if (data === "ERROR: The query result must have only one column"){
        		alert ("ERROR: The query result must have only one column");
        	}
        	else if (data === "ERROR: query result must be numeric"){
        		
        		alert ("ERROR: query result must be numeric");
        	}
        	else {
        		$("resultTable").paginathing({
                perPage: 99999999999
        		});
         	    $('.pagination').hide();
        	
        	    var json=JSON.parse(data)
        		
        		if(json[0]){
            	  	var keys = Object.keys(json[0]);
        		} else {
        			var keys = Object.keys(json);
        		}
        	  	keys.sort();
        	  	var i;
        	  	var len = keys.length;
        	  	$('#dynTableHead'+number).empty();
        	  	$('#resultTable'+number).empty();
        	  	for(i=0; i<len; i++)
        	  	{
        	  		var key = keys[i];
        	  		
        	  	    
        	  	    var th = $('<th></th>').appendTo('#dynTableHead'+number);
        	  	    th.text(key);
        	  	  
        	  	 }
        	  	$.each(json, function(key, value){
        	  		
        	  		
        	  		var keys = Object.keys(value);
        	  		
          	  	  	i, len = keys.length;

        	  		keys.sort();
        	  		var row= $('<tr id="id" class="line"></tr>').appendTo('#resultTable'+number);
        		    for (i = 0; i < len; i++) {
        	  			k = keys[i];
        	  			var rowContent = $('<td></td>').appendTo(row);
    		    		rowContent.text(value[k]);
    		    		
        	  		}
        	  	});
        	
        	  	$("#resultTable"+number).paginathing({
                    
          			perPage: 5
        
				});
        	  	
				$('.saveButton').removeClass("hide");
				var y = $(window).scrollTop();  //your current y position on the page
				$(window).scrollTop(y+450);
        	}
        	},
        	error: function(xhr, status, error){
        		console.log(xhr.responseText);
        		if(xhr.status&&xhr.status==500){
                    if((xhr.responseText).indexOf("<!doctype html>")!== -1){
                    	alert("Impossible to connect to your datasource. Please, check parameters");
                    
                    }
                    else if (xhr.responseText==="Can not issue data manipulation statements with executeQuery()."){
                    	alert("Data manipulation queries are not allowed");
                    }
                  else {
                	  alert(xhr.responseText);
                    }
        			
               }else{
                   alert("Something went wrong");
        	}
        	}
        });
	
}

function getId(myId){
	id = myId;
	
	
}


function inizialize(){
	$('.modal').modal();
    $('select').formSelect(); 
    $('.collapsible').collapsible();
    $('#code'+number).text("/* example query */\nSELECT\n\nFROM\n\nWHERE");
    htmlEditor = CodeMirror.fromTextArea(document.getElementById("code"+number), {
    	lineNumbers: true,
    	mode: 'sql',
    	theme: 'default'
    });
    htmlEditor.refresh();
    var text = $('#url'+number).val();
    //    $('#url').on('input', function() {
    const regex = /jdbc:mysql:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\/[a-zA-Z0-9]{2,256}/g;
    	console.log(regex.exec(text));
    	if (regex.exec(text) === null){
    		
    		$('#url'+number).removeClass('valid');
 
    		$('#url'+number).addClass('notValid');
//    		
   	}
    	else if(regex.exec(text) !== null) {
    		
    			$('#url'+number).removeClass('notValid');
    			
    		
    		$('#url'+number).addClass('valid');
   
    		}
    	$('#connector'+number).change(function(evt){
    		console.log("NUMBER:" + number);
    		var value = evt.target.value;
    		console.log("VALUE: " + value);
    		switch(value){
    		case 'mongo':
    			htmlEditor.setOption("mode","javascript");
    			htmlEditor.setValue('//example query\ndb.collection.find();');
    			$('.formSql').addClass("hide");
    			$('.formInflux').addClass("hide");
    			$('.formMongo').removeClass("hide");
    			$('.formUnalab').addClass("hide");
    			
    		break;
    		case 'influx':
    			htmlEditor.setOption("mode","sql"); 
    			htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n\nWHERE');
    			$('.formSql').addClass("hide");
    			$('.formInflux').removeClass("hide");
    			$('.formMongo').addClass("hide");
    			$('.formUnalab').addClass("hide");
    			break;
    		case 'sql':
    			htmlEditor.setOption("mode","sql");
    			htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n\nWHERE');
    			$('.formSql').removeClass("hide");
    			$('.formInflux').addClass("hide");
    			$('.formMongo').addClass("hide");
    			$('.formUnalab').addClass("hide");
    			break;
    			
    		case 'unalab':
    			htmlEditor.setOption("mode","sql"); 
    			htmlEditor.setValue('/* example query */\nSELECT\n\nFROM\n\nWHERE');
    			$('.formSql').addClass("hide");
    			$('.formInflux').addClass("hide");
    			$('.formMongo').addClass("hide");
    			$('.formUnalab').removeClass("hide");
    			break;
    		}
    		
    	});
 
}

function createJSTree(data) {
	if(!data.startsWith("{") && !data.startsWith("[")) {
		alert("Response is not a valid json");
		return;
	}
	$('#jstree').remove();
	var results;
	var json = JSON.parse(data);
	if(Array.isArray(json)){
		results = handleArray(json);
	} else {
		results = handleObject(json);
	}
	
	$('#jstree_demo').append('<div id=\"jstree\"></div>');
	$('#jstree').jstree({
		"core":{
			"data":results,
			"multiple":false
		},
		"checkbox" : {
		    "tie_selection" : false
		},
		"plugins":["checkbox"]
	})
	.on('check_node.jstree', function(e, data){
		$('#pathRest0').val(data.node.original.path);
		$('#pathRest0Label').addClass("active");
	});
}

function handleArray(json) {
	var results = [{ "id" : "results", "parent" : "#", "text" : "results", "path":"$" }];
	if(checkType(json) == "number" || checkType(json) == "string") {
		for (var i = 0; i < json.length; i++) {
			var obj = {};
			obj.id = "" + i;
			obj.text = json[i];
			obj.parent = "results";
			results.push(obj);
		}
	} else {
		for(var i = 0; i < json.length; i++) {
			if(!json[i].hasOwnProperty("id")) {
				json[i].id = "" + i;
			}
			if(!json[i].hasOwnProperty("text")) {
				json[i].text = "object" + i;
			}
			json[i].parent = "results";
			json[i].path = "$.[" + i + "]";
			results.push(json[i]);
			for (attr in json[i]) {		
				if(attr != "id" && attr != "text" && attr != "parent") {
					var name = attr;
					var obj = {};
					obj.id = json[i].id + name;
					obj.text = name;
					obj.parent = json[i].id;
					obj.path = "$.." + name + "";
					results.push(obj);
				}
			}	
		}
	}
	
	return results;
}

function handleObject(json) {
	var results = [{ "id" : "results", "parent" : "#", "text" : "results", "path":"$" }];
	for (attr in json) {
		if(attr != "id" && attr != "text" && attr != "parent") {
			var name = attr;
			var obj = {};
			obj.id = name;
			obj.text = name;
			obj.parent = "results";
			obj.path = "$['" + name + "']";
			results.push(obj);
		}
	}
	return results;
}

function toggleHeaders() {
	$('#reqHeader').toggle();
}

function saveHeader() {
	var headerName = $('#headerRest0').val();
	var headerValue = $('#headerRest1').val();
	if(headerName != "" && headerValue != "") {	
		$('#headerBody').append(
			"<div id=\"" + headerName + "\">" +
				"<span value=\"" + headerName + "\" style=\"display:inline-block; vertical-align: middle;\">" + headerName + "</span>" +
				":<span value=\"" + headerValue + "\" style=\"display:inline-block; vertical-align: middle;\">" + headerValue + "</span>" +
				"<i class=\"material-icons\" style=\"vertical-align: middle\" onclick=\"removeHeader('" + headerName +"')\">delete</i>" +
			"</div>");
	}
	$('#activeHeaders').show();
	cancelHeader();	
}

function removeHeader(header) {
	$('#' + header).remove();
	if($('#headerBody').children().length == 0) {
		$('#activeHeaders').hide();
	}
}

function cancelHeader() {
	$('#headerRest0').val('');
	$('#headerRest0').focusout();
	$('#headerRest1').val('');
	$('#headerRest1').focusout();
	$('#reqHeader').toggle();
}

$('#formRest30').on('change', function() {
	if($(this).find(":selected").val() !== "POST") {
		$('#reqBody').addClass('hide');
	} else {
		$('#reqBody').removeClass('hide');
	}
})

function checkType(data) {
	if(data.every((el) => typeof el == "number")){
		return "number";
	} else if(data.every((el) => typeof el == "object")) {
		return "object";
	} else if(data.every((el) => typeof el == "string")) {
		return "string";
	}
}


