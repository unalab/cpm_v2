<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="main.java.Queries"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%  request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    response.getCharacterEncoding();
    response.setContentType("text/html; charset=UTF-8");
    %>

<% ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
  kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList");
%>


<%


String role = (String) request.getSession().getAttribute("user");
String [] array; 
String c;
Integer id;

%>
<!-- Page Layout here -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />


<link type="text/css" rel="stylesheet" href="css/custom.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/common.css"
	media="screen,projection" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css" />
<!-- <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />  -->

</head>

<body>

<c:set var="city" value="${param.city}" />
<c:set var="cat" value="${param.categories}" />

<% if(role != null) {
	array = role.split("_");
	c = array[0];
	id = Queries.getCityId(c);
	
	%>
<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E; font-size: 2.1rem !important;"></span>

				<span class="text_center hide">
				 	<span style="text-align: center" id="testo"></span>
				 </span>

		<ul class="right">
			
		
			<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			
			
		<% if (role == "admin"){%>
			<li><a href="/cpm_v2/dashboard.jsp?city=Eindhoven&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetAdminKPIList?city=3&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
			
		<%} else {
			
			
			%>		
			<li><a href="/cpm_v2/dashboard.jsp?city=<%=c%>&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/view.jsp?city=<%=id%>&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
 		 
	<% } %>
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-activates="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-activates="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			<%if (role == "admin"){%>
				<span class="val">cpm-admin</span>
				
			<% } else  { %>
				<span class="val"><%=c%>-admin</span> 
			<% } %>			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
		</ul>
	</div>
	</nav>
<% } else{ %>

<nav id="header">
	<div class="nav-wrapper white customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>



		<ul class="right">
		
		<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
		
		<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
		<li><a href="/cpm_v2/guest.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
		
		<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-activates="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
		<li class="right"><a class="username dropdown-button dropdown-user-big" href="/cpm_v2/login.jsp?lang=${lang}"><i
					class="material-icons left">input</i><span class="val"><i18n:message value="login"/></span> </a></li>
		</ul>
	</div>
	</nav>
	
	<%} %>

<div id="wrapper">
<div id="container">

 <div id="transitCard" class="hide"></div>
 
  <div class="row">
    <div class="col s12 m12 l3">
      
        <div class="nav-wrapper">
          <form>
           
             <input class="unalabInput" type="text" id="search" placeholder="<i18n:message value="search"/>" />
            
          </form>
        </div>
      
      
  		<input type="hidden" name="city" class="city" value="${param.city}"></input>     
       <ul class="collapsible" data-collapsible="expandable"> 

        <li>
          <div class="collapsible-header active"><i class="material-icons">label</i><i18n:message value="category"/></div>
          <div class="collapsible-body filter-container">

            <ul>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="cloud_queue" value="climate" />
                <label for="cloud_queue"> <i class="material-icons small left" style="font-size:1.5rem">cloud_queue</i> <i18n:message value="climate"/></label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter  filled-in" checked="checked" id="warning" value="airquality"  />
                <label for="warning"> <i class="material-icons small left" style="font-size:1.5rem">warning</i><i18n:message value="airquality"/></label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="euro_symbol" value="economic"/>
                <label for="euro_symbol"><i class="material-icons small left" style="font-size:1.5rem">euro_symbol</i> <i18n:message value="economic"/></label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="waves" value="water" />
                <label for="waves"><i class="material-icons small left" style="font-size:1.5rem">waves</i><i18n:message value="water"/></label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="nature_people" value="green" />
                <label for="nature_people"><i class="material-icons small left" style="font-size:1.5rem">nature_people</i><i18n:message value="green"/></label>
              </li>
            </ul>
            
            
            
         

          </div>
        </li>
      </ul>
    </div>

    <!-- Cards container -->
    
      
   
        <div id="pag-card" class="col s12 m12 l9">
        
      <c:set var="mykpi" value="${requestScope.kpiList}" /> 

	<c:forEach var="kpi" items= "${mykpi}" varStatus="i">
		<c:set var="isManual" value="${kpi.getIsManual()}"></c:set>
        <div class="col s12 m6 l3">
          <!-- Card 1 -->
          <div  class="card small ${kpi.getIcon()}Class">
            <div class="card-content white-text">
            
            <c:choose>
			<c:when test = "${isManual == 'manual'}">
            <div class="card__date">
                <span class="card__date__day"><a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getIsManual()}')" href="#chart1" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="<i18n:message value="viewkpi"/>" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">insert_chart</i></a></span>
                
              </div>
             
            </c:when>
            
            <c:when test = "${isManual == 'citymanual'}">
            <div class="card__date">
                <span class="card__date__day"><a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getIsManual()}')" href="#chart1" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="<i18n:message value="viewkpi"/>" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">insert_chart</i></a></span>
                
              </div>
             
            </c:when>
            <c:otherwise>
            
            
            <div class="card__date">
                <span class="card__date__day"><a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getIsManual()}')" href="#chart1" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="<i18n:message value="viewkpi"/>" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">insert_chart</i></a></span>
                
              </div>
             
            
            </c:otherwise>
            </c:choose>    
              <div class="icon-block ">
              <h4 class="unalabText"><i class="material-icons medium left">${kpi.getIcon()}</i></h4>
              </div>
         	  <span class="unalabOrange card-info-custom" style="display:inline; font-size:32px; line-height:2">	${kpi.getValue()}</span>
              <span class="unalabOrange card-title" style="font-size: 14px !important">&nbsp;${kpi.getUnit()}</span>
              <span class="card-title unalabText" id="title" style="font-size: 15px !important">${kpi.getName()} <a style="padding:0 !important" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="${kpi.getDescription()}" ><i class="material-icons">info</i></a></span>
              
              
              
            </div>
           
          </div>
          <!-- End of card -->
        </div>
         </c:forEach> 
         
         </div>
        
      
    </div>
   </div>
    
</div>

 <div class="z-depth-1" style="margin:40px; width: 95%"> 
  
        <canvas id="chart1" style="display:none; width:1000; height:300" class="chartjs-render-monitor"></canvas> 
        
        <table id="table1" style="display:none;">
        
            <thead>
          <tr>
              <th>Date</th>
              <th>Value</th>
              <th>Note</th>
              
          </tr>
        </thead>

        <tbody id="body1">
          
        </tbody>
        </table> 
    </div> 
<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
<!-- 		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div> -->
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px; float: left;">&copy; 2019 UNaLab All rights reserved</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>


<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.0/Chart.min.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>

<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/dist/html2pdf.bundle.min.js"></script>
<script type="text/javascript" src="js/paginathing.js"></script>	
	
    
  <script>
  $(document).ready(function(){
          $("#pag-card").paginathing({
              
              perPage: 12
            
          });
          
         
      });
  
  </script>
  
  <script>
function dashboardParams(idKpi, city, type){
//	var id = idKpi;
//	console.log(id);
//	console.log("CITY FROM MANUAL " + city );
//	var c = city;
	
	generateData(idKpi,city,type);
//	var dashboard = prefix+manualkpidashName+suffix+param1+id+param2+c;
//	console.log(dashboard);
//	$("#cockpit").attr("src",dashboard);
//	console.log($("#cockpit").attr("src",dashboard));
	if($("#icon_"+idKpi).text()==="insert_chart"){
		$(".eyes").text("insert_chart");
		$("#icon_"+idKpi).text("visibility_off");
		$("#chart1").show();
		$("#chart1").removeClass("hide");
		$("#table1").show();
		$("#table1").removeClass("hide");
		$("#pag").hide();
	}
	else{
		$("#icon_"+idKpi).text("insert_chart");
		$("#chart1").hide();
		$("#table1").hide();
		$("#pag").show();
	}
	
}
	
	




</script>
  
  <script>
 
  $('.event-type-filter').on("change", function(){
	  
	  $("#pag-card").paginathing({
          perPage: 99999999999
      });
   	  $('.pagination').hide();
	  
	  var id= $(this).prop('id');
	  console.log(id+" "+$(this).prop("checked"));
	  
	  
		if($(this).prop("checked")){
			console.log("IF")
			
			var cardCol = $("#transitCard ."+id+"Class").parent().detach();
			cardCol.prependTo("#pag-card");
			
			console.log("CARD COL "+cardCol);
			console.log(cardCol);
			console.log("IF end")
		}  
		else{
			console.log("else")

			var cardCol = $("."+id+"Class").parent().detach();
			cardCol.appendTo("#transitCard");
			
			console.log("else end")
		}
		

		$("#pag-card").paginathing({
            perPage: 12
        });
		
  });
  
  

</script>
  
  <script>
$(document).ready(function(){
$("#search").on("keyup", function() {
    var value = $(this).val();

    console.log("VALUE: " + value);
    
    var set =  $(".card");
    var length = set.length;
    
    set.each(function(index){
        if (index != -1 && index < length) {

		   var id = $(this).find("#title").text();
    	  
           if(value!=""){
        	   $('#pag').hide();
        	   if (id.toLowerCase().indexOf(value.toLowerCase()) == -1) {
           		$(this).parent().hide();
           		} 
           		else {
           			console.log(id);
            		
           		$(this).parent().show();
           		}
           }
           else if (value=="") {
        	  		 $('#pag').empty();
           			$('#pag').show();
        			
        			$("#pag-card").paginathing({
        	              
        	              perPage: 12
        	              
        	          });
        }
        
        
    }
});

});
});



</script>

<script>
var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

$( window ).on( "load", function(){
    if(!isChrome){
    		$('.text_center').removeClass("hide");
   		if($("testo")){
   			
   			$('#testo').text("Please, use Google Chrome to have best performances");
   			
   		}
    		
    }
});

</script>

  
  </body>
</html>