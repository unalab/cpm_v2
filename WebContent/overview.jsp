<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="main.java.Queries"%>
<%@page import="java.util.ArrayList"%>

<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--  ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>(); --%>
<%--    kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList"); --%>
  
 <% String role = (String) request.getSession().getAttribute("user");
  String [] array = role.split("_");
  String c= array[0];
  Integer id = Queries.getCityId(c);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		

<link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<link type="text/css" rel="stylesheet" href="css/customEngine.css" media="screen,projection" />

<link type="text/css" rel="stylesheet" href="css/codemirror.css" media="screen,projection" /> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  				
</head>


<% //In case, if Admin session is not set, redirect to Login page
if((request.getSession(false).getAttribute("user")== null) )
{
%>
<jsp:forward page="login.jsp"></jsp:forward>
<%} 


%>
<body>

<!-- <nav id="header"> -->
<!-- 	<div class="nav-wrapper customW"> -->
<!-- 		<a href="index.html" class="brand-logo"><img -->
<!-- 			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo" -->
<!-- 			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text" -->
<!-- 			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span> -->

<!-- 		<span class="text_center hide"> -->
<!-- 			<span style="text-align: center" id="testo"></span> -->
<!-- 		</span> -->
<!-- 	</div> -->
<!-- </nav> -->
	
<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo" style="width: 42%"></a> 
		<span id="titleCity" class="brand-logo secondary-text" style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>
		<span class="text_center hide">
			<span style="text-align: center" id="testo"></span>
		</span>

		<ul class="right">
			
			<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<% if (role == "admin"){%>
			
			<li><a href="/cpm_v2/GetAdminKPIList?city=3&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
			
		<%} else {
			
			
			%>		
			
			<li><a href="/cpm_v2/view.jsp?city=<%=id%>&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
 		 
	<% } %>
			
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			
				<span class="val"><%=c%>-admin</span> 
			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
			
		</ul>
	</div>
	</nav>
  
 <section>
	<div class="container">
		<div class="row">
		
		
				<h4>Create a measure or build the formula for your indicator</h4>
				<hr></hr>
				
			  	<div class="col s12 m6" style="margin-left:25%">
			  	
			  	<a id="cardMeasure">
					<div class="card horizontal">
						
						<div class="card-stacked">
							<div class="card-content">
								<p class="blackText">Click to select a data source for your measure</p>
							</div>
							
						</div>
						
						
						<div class="card-image">
							<img style="max-width:65%"src="/cpm_v2/img/measure.png">
						</div>
					</div>
					</a>
					
					
					<a id="cardFormula" >
					<div class="card horizontal">
					<div class="card-stacked">
							<div class="card-content">
								<p class="blackText">click to build the formula to calculate the KPI</p>
							</div>
							
						</div>
						
						<div class="card-image">
							<img style="max-width:65%" src="/cpm_v2/img/formula.png">
						</div>
					</div>
					</a>
				</div>

			</div>
		</div>
  	
</section> 
 </body>
	
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/customEngine.js"></script>



