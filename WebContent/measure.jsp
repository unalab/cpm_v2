<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="main.java.Queries"%>
<%@page import="java.util.ArrayList"%>

<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


  
 <% String role = (String) request.getSession().getAttribute("user");
  String [] array = role.split("_");
  String c= array[0];
  Integer id = Queries.getCityId(c);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		

<link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<link type="text/css" rel="stylesheet" href="css/customEngine.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/codemirror.css" media="screen,projection" /> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
			
</head>

<% //In case, if Admin session is not set, redirect to Login page
if((request.getSession(false).getAttribute("user")== null) )
{
%>
<jsp:forward page="login.jsp"></jsp:forward>
<%} 


%>

<body>

<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo" style="width: 42%"></a> 
		<span id="titleCity" class="brand-logo secondary-text" style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>
		<span class="text_center hide">
			<span style="text-align: center" id="testo"></span>
		</span>

		<ul class="right">
			
			<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<% if (role == "admin"){%>
			
			<li><a href="/cpm_v2/GetAdminKPIList?city=3&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
			
		<%} else {
			
			
			%>		
			
			<li><a href="/cpm_v2/view.jsp?city=<%=id%>&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
 		 
	<% } %>
			
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			
				<span class="val"><%=c%>-admin</span> 
			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
			
		</ul>
	</div>
	</nav>
	
<nav id="bread">
    <div class="nav-wrapper">
      <div class="col s12">
        <a id="measure0"  class="breadcrumb selectedBread">Select measures</a>
        <a id="formula0" class="breadcrumb">Build KPI Formula</a>
        <a id="schedule0" class="breadcrumb">Schedule KPI Calculation</a>
      </div>
    </div>
  </nav>
  
 <section>

  
 
	<div class="container">
	<ul class="collapsible" data-collapsible="expandable">
            <li class="active customOpen">
              <div class="collapsible-header"><i class="material-icons">keyboard_arrow_right</i><a class="measure" id="name0" name="987"/>New Measure</a></div>
              <div class="collapsible-body">
              
              	<div class="row">
				<div class="input-field col s2">
				
  				<select id="connector0"> 
    
     				<option value="mongo" >MongoDB</option> 
    				<option value="sql" selected>MySQL</option> 
     				<option value="influx">InfluxDB</option>
     				<option value="unalab">UNaLab DB</option>
     				<option value="rest">REST</option>
   				</select> 
  				<label>Select data source</label>
				</div>
				
				<div class="input-field col s5 formUnalab hide" id="formUnalab10">
				
  				<select id="unalabDB0"> 
    
   				</select> 
  				<label>Select database</label>
				</div>
				
				<div class="input-field col s5 formUnalab hide" id="formUnalab20">
				
  				<select id="unalabDBMeas0"> 
    

   				</select> 
  				<label>Select measurement</label>
				</div>

    			<div class="input-field col s5 formSql" id="formSql10">
      			<input value="jdbc:mysql://datasourceUrl:port/dbname"  name= "url" id="url0" type="text" class="validate sqlUrl">
      			<label class="active" for="url">Data Source URL</label>
    			</div>
    
    			<div class="input-field col s2 formSql" id="formSql20" >
      			<input value="" id="username0" name="username" type="text" class="validate sqlUser">
      			<label class="active" for="username">Username</label>
   				 </div>
   				 
    			<div class="input-field col s2 formSql" id="formSql30">
      			<input value="" id="password0" name="password" type="password" class="validate sqlPass">
      			<label class="active" for="password">Password</label>
    			</div>
    
    			<div class="input-field col s5 formMongo hide" id="formMongo0">
      			<input value="jdbc:mongodb://datasourceUrl:port/dbname" id="urlMongo0" name="urlMongo" type="text" class="validate mongoUrl">
      			<label class="active" for="urlMongo">Data Source URL</label>
    			</div>
    
    			<div class="input-field col s5 formInflux hide" id="formInflux10">
      			<input value="http://datasourceUrl:port/dbname" id="urlInflux0" name="urlInflux" type="text" class="validate influxUrl">
      			<label class="active" for="urlInflux">Data Source URL</label>
    			</div>
    
    			<div class="input-field col s2 formInflux hide" id="formInflux20">
      			<input value="" id="usernameInflux0" type="text" name="usernameInflux" class="validate influxUser">
      			<label class="active" for="usernameInflux">Username</label>
    			</div>
    			
    			<div class="input-field col s2 formInflux hide" id="formInflux30">
      			<input value="" id="passwordInflux0" type="password" name="passwordInflux" class="validate influxPass">
      			<label class="active" for="passwordInflux">Password</label>
    			</div>
    			
    			<div class="input-field col s4 formRest hide" id="formRest10">
      			<input value="http://datasetUrl:port" id="urlRest0" type="text" name="urlRest" class="validate restUrl" required>
      			<label class="active" for="urlRest">DataSet Url</label>
      			<span class="helper-text" data-error="This field is required"></span>
    			</div>
    			
    			<div class="input-field col s2 formRest hide" id="formRest30">
	  				<select id="typeRest0"> 
	     				<option value="GET" selected>GET</option> 
	    				<option value="POST" >POST</option> 
	     				<!-- <option value="PUT">PUT</option>
	     				<option value="DELETE">DELETE</option> -->
	   				</select> 
	  				<label>HTTP Method</label>
    			</div>
    			
    			<a id="headerToggle" class="btn btn-right col s2 input-field hide" onclick="toggleHeaders()">Add headers</a>
    
 			</div>
 			
 			<div class="row" id="jsonPath">
 			    <div class="input-field col s10 formRest hide" id="formRest20">
      				<input value="" id="pathRest0" type="text" name="pathRest" class="validate restPath">
      				<label id="pathRest0Label" for="pathRest">Json path</label>
    			</div>
    		</div>
 			
 			<div class="row hide" id="reqBody">
	 			<div class="input-field col s12 formRest hide" id="formRest40">
	 				<input value="" id="bodyRest0" type="text" name="bodyRest" class="validate restBody">
	      			<label class="active" for="bodyRest">Request Body</label>
	 			</div>
 			</div>
 			
 			<ul class="row collapsible" id="activeHeaders" style="display:none">
	 			<li>
			      <div id="headerTitle" class="collapsible-header"><i class="material-icons">keyboard_arrow_down</i>Active headers</div>			 
			      <div class="collapsible-body"><span id="headerBody"></span></div>
			    </li>
 			</ul>
 			
 			<div class="row" id="reqHeader" style="display:none">
 				<div class="input-field col s3 formRest" id="formRest50">
	 				<input value="" id="headerRest0" type="text" name="headerRest0" class="validate restHeader">
	      			<label class="active" for="headerRest0">Header Name</label>
	 			</div>
	 			<div class="input-field col s3 formRest" id="formRest60">
	 				<input value="" id="headerRest1" type="text" name="headerRest1" class="validate restHeader">
	      			<label class="active" for="headerRest1">Header Value</label>
	 			</div>
	 			<div class="col s1 input-field" style="margin-right:2%">
	 				<a id="headerSave" class="btn btn-right" onclick="saveHeader()">Save</a>
	 			</div>
	 			<div class="col s1 input-field">
	 				<a id="headerCancel" class="btn btn-right" onclick="cancelHeader()">Cancel</a>
	 			</div>
 			</div>
 			
 			<div id="jstree_demo">
 				
 			</div>	
 			
			<div class="code-container">
			<textarea id="code0">
 
			</textarea>
	
			</div>
			<div class="row">
			
				<a class="btn btn-right" onclick="execute()">Execute Query</a>
			</div>
			
			
			<!-- ricomincio da qui  -->
			<table cellpadding="1" cellspacing="1" class="table table-hover sortable-table indexed tablesorter" id="result0">
				<thead>
					<tr id="dynTableHead0">
						
					</tr>
				</thead>
				<tbody id="resultTable0">
		
				</tbody>
			</table>
			
			<br />
			
			<div class="row saveButton hide" >
				 <a id="" class="waves-effect waves-light btn modal-trigger" href="#modal0" onclick="getId(this.id)">Save</a>
			</div>
			
            </div>
            </li>
           
        </ul>
        <div id="addMeasure" class="row hide">
			
			<a class="btn btn-right" onclick="addMeasure()">Add Another Measure</a>
		</div>
	</div>
</section> 


<!-- Modal Trigger -->
 

  <!-- Modal Structure -->
  <div id="modal0" class="modal">
    <div class="modal-content">
      <div class="input-field col s3 ">
      	<input value="" id="measureName0" type="text" name="measureName" class="validate measureName">
      	<label class="active" for="measureName">Insert a name for this measure</label>
   	 </div>
      
    </div>
    <div class="modal-footer">
      <a onclick="save()" class="modal-close waves-effect waves-green btn-flat">OK</a>
    </div>
  </div>
 
 </body>
	
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/paginathing.js"></script>

<script type="text/javascript" src="js/codemirror.js"></script>
<script type="text/javascript" src="js/htmlmixed.js"></script>
<script type="text/javascript" src="js/css.js"></script>

<script type="text/javascript" src="https://codemirror.net/mode/javascript/javascript.js"></script>
<script type="text/javascript" src="https://codemirror.net/mode/sql/sql.js"></script>
<script type="text/javascript" src="js/dbConfiguration.js"></script>
<script type="text/javascript" src="js/customEngine.js"></script>



