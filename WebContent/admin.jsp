<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>

<%@page import="java.util.ArrayList"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%  request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    response.getCharacterEncoding();
    response.setContentType("text/html; charset=UTF-8");
    %>

<% ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
  kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList");
  
  
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/> 
<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/common.css" media="screen,projection" /> 
 <!-- <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />  -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> -->
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> --> 
 <script type="text/javascript" src="js/materialize.js"></script> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.0/Chart.min.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/paginathing.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/admin.js"></script>





</head>


<body>
<%-- <c:import url="/GetKPIList2" /> --%>
<c:set var="city" value="${param.city}" />

	<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>
			
			<span class="text_center hide">
				 	<span style="text-align: center" id="testo"></span>
				 </span>
		<ul class="right">
			<li><a id="dwn-btn" class="white-text text-darken-4" onClick="generateJson(${param.city})">Download indicators</a></li>
		    <li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content testStyle'>

				<li>
				<a class="" href="/cpm_v2/GetAdminKPIList?city=${param.city}&level=task&category=all&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetAdminKPIList?city=${param.city}&level=task&category=all&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetAdminKPIList?city=${param.city}&level=task&category=all&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/GetAdminKPIList?city=${param.city}&level=task&category=all&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			<span class="val">cpm-admin</span>
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content testStyle'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
			
		</ul>
	</div>
	</nav>
	
	
<!-- <div class = "row"> -->
<!-- <div class="col s2" style="float:right;"> -->
<%-- <a style="margin-top: 15px; margin-left: 80%;" class="btn-floating btn-medium waves-effect waves-light unalabGreen modal-trigger tooltipped" data-position="bottom" data-tooltip="<i18n:message value="addkpi"/>" href="#modal1"><i class="material-icons">add</i></a> --%>
<!-- </div> -->
<!-- <div class="col s12"> -->
<%-- <h5 class="center" ><i18n:message value="expert"/></h5> --%>
<div id="transitCard" style="display:none;"></div>

<!-- </div> -->
<!-- </div> -->
<div class="row">
<div class="col s11">
<h5 class="center">Expert view
</h5>
</div>
<div class="col s1">
    <a style="margin-top: 10px;margin-left: 50%;" class="btn-floating btn-medium waves-effect waves-light unalabGreen modal-trigger tooltipped" data-position="bottom" data-tooltip="Add indicator" href="#modal1"><i class="material-icons">add</i></a>
</div>
</div>

	<div class="row">
	
	
<div class="col s12 m12 l3">
      
        <div class="nav-wrapper">
        
<%--         <form id="selectForm" action="/cpm/GetKPIList?city=${param.city}&lang=${lang}" --%>
<!-- 			 method="post">	   -->
      
<%-- <input type="hidden" name="city" class="city" value="${param.city}"></input>   --%>

<!-- <ul class="collapsible" data-collapsible="expandable">  -->

<!--         <li> -->
<%--           <div class="collapsible-header active"><i class="material-icons">label</i><i18n:message value="category"/></div> --%>
<!--           <div class="collapsible-body filter-container"> -->

<!--  <p> -->
<%--     <input class="checkbox-unalab with-gap" name="radio1" type="radio" id="radio1"  value="common" <%if(request.getParameter("level").equals("common")){ %> checked <%} %>checked /> --%>
<!--     <label for="radio1">Common Indicators</label> -->
<!--   </p> -->
<!--   <p> -->
<%--     <input class="checkbox-unalab with-gap" name="radio1" type="radio" id="radio2" value="city" <%if(request.getParameter("level").equals("city")){ %> checked <%} %>/> --%>
<!--     <label for="radio2">City-Level indicators</label> -->
<!--   </p> -->
<!-- <p> -->
<%--     <input class="checkbox-unalab with-gap" name="radio1" type="radio" id="radio3" value="project" <%if(request.getParameter("level").equals("project")){ %> checked <%} %>/> --%>
<!--     <label for="radio3">Project-Level Indicators</label> -->
<!--   </p> -->
<!-- 	</li> -->
<!-- 	</ul> -->
<!-- </form> -->
<ul class="collapsible" data-collapsible="expandable" id="collapsibleOne" > 

        <li>
          <div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="category"/></div>
          <div class="collapsible-body filter-container">

 <ul>
       <li style="padding: 2px !important">
    			<p>
      			<label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="cloud_queue" value="climate" />
                <span class="cloud_queue"> <i class="material-icons small left" style="font-size:1.5rem">cloud_queue</i> <i18n:message value="climate"/></label>
              
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one  filled-in" checked="checked" id="warning" value="airquality"  />
                <span class="warning"> <i class="material-icons small left" style="font-size:1.5rem">warning</i><i18n:message value="airquality"/></label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="euro_symbol" value="economic"/>
                <span class="euro_symbol"><i class="material-icons small left" style="font-size:1.5rem">euro_symbol</i> <i18n:message value="economic"/></label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="waves" value="water" />
                <span class="waves"><i class="material-icons small left" style="font-size:1.5rem">waves</i><i18n:message value="water"/></label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="nature_people" value="green" />
                <span class="nature_people"><i class="material-icons small left" style="font-size:1.5rem">nature_people</i><i18n:message value="green"/></label>
              </label>
              </p>
              </li>
              
            </ul>
	</div>
	</li>
	</ul>


<%-- <ul class="collapsible" style="display:none;" data-collapsible="expandable" id="collapsibleTwo" <%if(request.getParameter("level").equals("city")||request.getParameter("level").equals("project")){ %> style="display: block !important;"<%} %>>  --%>

<!--         <li> -->
<%--           <div class="collapsible-header active"><i class="material-icons">label</i><i18n:message value="category"/></div> --%>
<!--           <div class="collapsible-body filter-container"> -->

<!--  <ul> -->
<!--               <li> -->
<!--                 <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="group" value="people" /> -->
<!--                 <label for="group"> <i class="material-icons small left" style="font-size:1.5rem">group</i>People</label> -->
<!--               </li> -->
<!--               <li> -->
<!--                 <input type="checkbox" name="event-type-filter2" class="event-type-filter-two  filled-in" checked="checked" id="public" value="airquality"  /> -->
<!--                 <label for="public"> <i class="material-icons small left" style="font-size:1.5rem">public</i>Planet</label> -->
<!--               </li> -->
<!--               <li> -->
<!--                 <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="attach_money" value="economic"/> -->
<!--                 <label for="attach_money"><i class="material-icons small left" style="font-size:1.5rem">attach_money</i> Prosperity</label> -->
<!--               </li> -->
<!--               <li> -->
<!--                 <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="graphic_eq" value="water" /> -->
<!--                 <label for="graphic_eq"><i class="material-icons small left" style="font-size:1.5rem">graphic_eq</i>Propagation</label> -->
<!--               </li> -->
<!--               <li> -->
<!--                 <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="gavel" value="green" /> -->
<!--                 <label for="gavel"><i class="material-icons small left" style="font-size:1.5rem">gavel</i>Governance</label> -->
<!--               </li> -->
<!--             </ul> -->
<!-- 	</div> -->
<!-- 	</li> -->
<!-- 	</ul> -->


</div>
  </div>

				

	
		<div class="col s9" style="padding-left:35px">
			<input class="unalabInput" type="text" id="search" placeholder="<i18n:message value="typetosearch"/>" />
			<div class="tableContent">
			<table cellpadding="1" cellspacing="1" class="table table-hover sortable-table indexed" id="myTable2">
				<thead>
					<tr class="sorter-header">
						<th onclick="sortTable(0)" class="unalabTd no-sort"><u>ID</u></th>
						<th onclick="sortTable(1)" class="unalabTd no-sort"><u><i18n:message value="indicator"/></u></th>
<!-- 						<th class="unalabTd no-sort">NBS title</th> -->
						
						<th class="unalabTd no-sort">Category</th>
						<th class="unalabTd no-sort"><i18n:message value="value"/></th>
						<th onclick="sortTable(5)" class="unalabTd no-sort"><u><i18n:message value="unit"/></u></th>
						
						<th onclick="sortTable(6)" class="unalabTd is-date ascending"><u>Last Update</u></th>
						<th class="unalabTd no-sort"><i18n:message value="actions"/></th>

					</tr>
				</thead>
				<tbody id="myTable">
		<c:set var="mykpi" value="${requestScope.kpiList}" />

			<c:forEach var="kpi" items= "${mykpi}" varStatus="i">
			        
				<c:set var="isManual" value="${kpi.getIsManual()}"></c:set> 
					
					
					

					
					<tr class="${kpi.getIcon()}Class">
					   <td class="unalabTd">${kpi.getTableId()}</td>
					   <td class="unalabTd"><a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip="${kpi.getDescription()}"> <i class="tiny material-icons">info</i></a> ${kpi.getName()} (entityID=${kpi.getEntity()})</td>
<%-- 					    <td class="unalabTd">${kpi.getNbs()}</td> --%>
					   <td class="unalabTd"><a style="padding:0 4px !important" href=# class="btn-flat"> <i class="tiny material-icons">${kpi.getIcon()}</i></a> </td>
					<c:choose>
						<c:when test = "${isManual == 'automatic'}">
						<td class="unalabTd"><a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip="value computed by data management tool"> <i class="tiny material-icons">info</i></a>${kpi.getValue()}</td>
						</c:when>
						<c:otherwise>
						<td class="unalabTd"><a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip="${kpi.getMotivation()}"> <i class="tiny material-icons">info</i></a>${kpi.getValue()}</td>
						</c:otherwise>
						</c:choose>
						<td class="unalabTd">${kpi.getUnit()}</td>
						
						<td class="unalabTd">${kpi.getDate()}</td>
						
						<c:choose>
						<c:when test = "${isManual == 'manual'}">
						<td class="unalabTd">
							
							<a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getIsManual()}')" href="#chart1" 
							class="tooltipped btn-flat view" data-position="top" data-tooltip="<i18n:message value="viewkpi"/>" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">insert_chart</i></a>
 							<a style="padding:0 !important" href="#modal3" id="btnMio2" onclick="myParamAdminInfo('${kpi.getIdKPI()}','${kpi.getKpiLevel()}','${kpi.getKpiCategory()}','${kpi.getIdCity()}', '${kpi.getName()}','${kpi.getTableId()}','${kpi.getDescription()}','${kpi.getUnit()}','${kpi.getIsManual()}','${kpi.getNbs()}')"
							class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="<i18n:message value="editkpi"/>" ><i class="material-icons">edit</i></a>
							
 							</td> 
					</c:when>
					
					<c:when test = "${isManual == 'citymanual'}">
						<td class="unalabTd">
							<a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getIsManual()}')" href="#chart1" 
							class="tooltipped btn-flat view" data-position="top" data-tooltip="<i18n:message value="viewkpi"/>" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">insert_chart</i></a>
 							<a style="padding:0 !important" href="#modal3" id="btnMio2" onclick="myParamInfo('${kpi.getIdKPI()}','${kpi.getKpiLevel()}','${kpi.getKpiCategory()}','${kpi.getIdCity()}', '${kpi.getName()}','${kpi.getTableId()}','${kpi.getDescription()}','${kpi.getUnit()}','${kpi.getIsManual()}','${kpi.getNbs()}')"
							class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="<i18n:message value="editkpi"/>" ><i class="material-icons">edit</i></a>
					
 							</td> 
					</c:when>
					
					<c:otherwise>
					<td class="unalabTd">
					<a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getIsManual()}')" href="#chart1" 
							class="tooltipped btn-flat view" data-position="top" data-tooltip="<i18n:message value="viewkpi"/>" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">insert_chart</i></a>
					<a style="padding:0 !important" href="#modal3" id="btnMio2" onclick="myParamInfo('${kpi.getIdKPI()}','${kpi.getKpiLevel()}','${kpi.getKpiCategory()}','${kpi.getIdCity()}', '${kpi.getName()}','${kpi.getTableId()}','${kpi.getDescription()}','${kpi.getUnit()}','${kpi.getIsManual()}','${kpi.getNbs()}')"
							class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="<i18n:message value="editkpi"/>" ><i class="material-icons">edit</i></a>
					
					</td>
					
					</c:otherwise>
					</c:choose>
					
					</tr>
					
			</c:forEach>
			
			
				</tbody>
			</table>
			</div>
			
			
<!-- 			<div id="pag" class="col-md-12 center text-center" style="position:fixed;bottom:30px;left:0;width:100%;"> -->
<!-- 				<span class="center" id="total_reg"></span> -->
<!-- 				<ul class="pagination pager" id="myPager"></ul> -->
<!-- 			</div> -->
		</div>
		
		

	</div>


<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
<!-- 		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div> -->
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px; float: left;">&copy; 2019 UNaLab All rights reserved</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>

<!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
    
    <div id="manualForm">
      <h5><i18n:message value="addcommon"/></h5>

      <div class="row">
        <form action="/cpm_v2/AddAdminKPI?lang=${lang}" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="id" name="id" type="number" class="validate unalabInput" required>
              <label class="unalabLabel" for="id">ID</label>
              <span id="idError" class="red-text">This field is required</span> 
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="name" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label for="name"><i18n:message value="name"/></label>
              <span id="newNameError" class="red-text">This field is required</span> 
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="description" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label for="description"><i18n:message value="description"/></label>
              <span id="newDescError" class="red-text">This field is required</span> 
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unit" type="text" name="unit" class="validate unalabInput" required>
              <label for="unit"><i18n:message value="unit"/></label>
              <span id="newUnitError" class="red-text">This field is required</span> 
            </div>
          </div> 
              
          <div class="row modal-form-row">  
             
			<div class="input-field col s12">
						<input name="city" type="hidden" value="${param.city}"></input> 
<%-- 						<span><i18n:message value="level"/></span>  --%>
<!-- 						<select -->
<!-- 							id="level" name="level" > -->
<%-- 							<option value="task"><i18n:message value="common"/></option> --%>
<!-- <!-- 							<option value="city">City-Level Indicators</option> --> 
<!-- <!-- 							<option value="project">Project-Level Indicators</option> --> 
<!-- 						</select>  -->
							
							</div>
							
							<div class="input-field col s12">
							<span><i18n:message value="category"/></span> 
							<select id="cat1" name="cat1" >

							<option value="climate"><i18n:message value="climate"/></option>
							<option value="airquality"><i18n:message value="airquality"/></option>
							<option value="economic"><i18n:message value="economic"/></option>
							<option value="water"><i18n:message value="water"/></option>
							<option value="green"><i18n:message value="green"/></option>

						</select> 
<!-- 						<select id="cat2" name="cat2"> -->

<!-- 							<option value="people">People</option> -->
<!-- 							<option value="planet">Planet</option> -->
<!-- 							<option value="prosperity">Prosperity</option> -->
<!-- 							<option value="governance">Governance</option> -->
<!-- 							<option value="propagation">Propagation</option> -->


<!-- 						</select> -->
						</div>
						</div>
						
						 <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="add"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>
						
				</form>
      </div>
      </div>
      
      

      </div>
      </div>


<!-- Modal Structure -->
  <div id="modal3" class="modal">
    <div class="modal-content">
      <h5 id="titlemodale" class="title"></h5>

	
		
      <div class="row" id="overLapping">
        <form action="/cpm_v2/ModifyAdminKPI?lang=${lang}" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
            
              <input id="tableId" name="tableId" type="number" class="validate unalabInput" disabled>
              <label style="margin-top: -20px" class="unalabLabel active"  for="tableId">ID</label>
            </div>
          </div>
       

          
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="nameAdminEdit" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="name"><i18n:message value="name"/></label>
              <span id="nameError" class="hide red-text">This field is required</span>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="descAdminEdit" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="description"><i18n:message value="description"/></label>
              <span id="descError" class="hide red-text">This field is required</span>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unitAdminEdit" type="text" name="unit" class="validate unalabInput" required>
              <label style="margin-top: -20px" class="unalabLabel active" for="unit"><i18n:message value="unit"/></label>
              <span id="unitError" class="hide red-text">This field is required</span>
            </div>
          </div> 
         
          <c:out value="${param['id']}"></c:out>
	    
		
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="KPI_id" />
		
		<div class="modal-footer">
   
      <button type="submit" id="submitAdminEdit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="confirm"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>       
       
          </form>
          
          </div>
          </div>
          </div>   
           
 <div class="z-depth-1" style="margin:40px; width: 95%"> 
  
        <canvas id="chart1" style="display:none; width:1000; height:300" class="chartjs-render-monitor"></canvas> 
        
        <table id="table1" style="display:none;">
        
            <thead>
          <tr>
              <th>Date</th>
              <th>Value</th>
              <th>Note</th>
              
          </tr>
        </thead>

        <tbody id="body1">
          
        </tbody>
        </table> 
    </div> 
     
      
      
      
      
<!--   <!-- Modal Structure --> 
<!--   <div id="modal2" class="modal"> -->
<!--     <div class="modal-content"> -->
<!--       <h5 class="title"></h5> -->

<!--       <div class="row"> -->
<%--         <form action="/cpm_v2/EditKPI?lang=${lang}" method="post"> --%>
<!--           <div class="row modal-form-row" id="modal-body"> -->
          
<%--           <c:out value="${param['id']}"></c:out> --%>
<!-- 	    <input type="hidden" name="title" /> -->
<!-- 		<input type="hidden" name="KPI_id" /> -->
<!-- 		<input type="hidden" name="city" /> -->
<!-- 		<input type="hidden" name="level" /> -->
<!-- 		<input type="hidden" name="category" /> -->
<!-- 		<input type="hidden" name="tableId" /> -->
<!--           </div> -->
<!--           <div class="row"> -->
<!--             <div class="input-field col s12"> -->
           
<!--               <input class="unalabInput" id="value" name="value" type="number" placeholder="0.00" min="0" value="0" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" required> -->
<%--               <label for="value"><i18n:message value="value"/></label> --%>
<!--             </div> -->
<!--           </div> -->
          
<!--           <div class="modal-footer"> -->
   
<!--       <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Set</button> -->
<!--        <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a> -->
<!--     </div>        -->
<!--        </form> -->
<!--       </div> -->
<!--     </div> -->
    
<!--   </div> -->


</body>


</html>