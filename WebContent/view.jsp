<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="main.java.Queries"%>
<%@page import="java.util.ArrayList"%>

<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


 <% String role = (String) request.getSession().getAttribute("user");
  String [] array = role.split("_");
  String c= array[0];
  Integer id = Queries.getCityId(c);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/common.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>	




				
</head>

<% //In case, if Admin session is not set, redirect to Login page
if((request.getSession(false).getAttribute("user")== null) )
{
%>
<jsp:forward page="login.jsp"></jsp:forward>
<%} 


%>
<body>
<%-- <c:import url="/GetKPIList2" /> --%>
<c:set var="city" value="${param.city}" />

	<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>

<span class="text_center hide">
				 	<span style="text-align: center" id="testo"></span>
				 </span>

		<ul class="right">
			<li><a id="dwn-btn" class="white-text text-darken-4" onClick="generateJson(${param.city})">Download indicators</a></li>
			
			<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			
				<span class="val"><%=c%>-admin</span> 
			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
			
		</ul>
	</div>
	</nav>
	
	
	
<div class="row">
<div class="col s9">
<h5 style="padding-left:60%">Expert view
</h5>
</div>
<div class="col s3">
    <a href="#modal1" style="margin-top: 10px;margin-left: 25%;" class=" modal-trigger btn-floating btn-medium waves-effect waves-light unalabGreen tooltipped" data-position="bottom" data-tooltip="Add indicator" ><i class="material-icons">add</i></a>
	<a id="measureButton"  style="margin-top: 10px;" class=" modal-trigger btn-floating btn-medium waves-effect waves-light unalabGreen tooltipped" data-position="bottom" data-tooltip="Select a measure from a data source" ><i class="material-icons">youtube_searched_for</i></a>
	<a id="formulaButton"  style="margin-top: 10px;" class=" modal-trigger btn-floating btn-medium waves-effect waves-light unalabGreen tooltipped" data-position="bottom" data-tooltip="Build an indicator formula" ><i class="material-icons">build</i></a>
	<a id="scheduleButton"  style="margin-top: 10px;" class=" modal-trigger btn-floating btn-medium waves-effect waves-light unalabGreen tooltipped" data-position="bottom" data-tooltip="Schedule indicators calculation" ><i class="material-icons">schedule</i></a>
</div>
</div>

<!-- <ul id='chooseType' class='dropdown-content'> -->
<!--     <li><a class="modal-trigger" href="#modal1">Add a new manual indicator</a></li> -->
<%--     <li><a href="overview.jsp?city=${param.city}">Add a new computed indicator</a></li> --%>
<!-- </ul> -->
<!-- <div class = "row"> -->
<!-- <div class="col s2" style="float:right;"> -->
<%-- <a style="margin-top: 15px; margin-left: 80%;" class="btn-floating btn-medium waves-effect waves-light unalabGreen modal-trigger tooltipped" data-position="bottom" data-tooltip="<i18n:message value="addkpi"/>" href="#modal1" ><i class="material-icons">add</i></a> --%>
<!-- </div> -->
<!-- <div class="col s10"> -->
<%-- <h5 class="center" ><i18n:message value="expert"/></h5> --%>
<div id="transitCard" style="display:none;"></div>

<!-- </div> -->
<!-- </div> -->

	<div class="row">
<div class="col s12 m12 l3">
      
        <div class="nav-wrapper">
        
 <form id="selectForm" action="/cpm_v2/GetKPIList?city=${param.city}&lang=${lang}"
			 method="post">	  
      
	<input type="hidden" name="city" class="city" value="${param.city}"></input>  

	<ul class="collapsible" data-collapsible="expandable"> 

        <li>
          <div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="level"/></div>
          <div class="collapsible-body filter-container">
				<ul class="collection">
					<li class="collection-item" style="border-bottom: 0px;">
 						<label>
    						<input class="radiobtn with-gap" name="radio2" type="radio" id="radio1"  value="common" <%if(request.getParameter("level").equals("common")){ %> checked="checked" <%} %>checked="checked" />
    						<span class="radio1">Common Indicators</span>
  						</label>
  					</li>
  					<li class="collection-item" style="border-bottom: 0px;">
  						<label>
    						<input class="radiobtn with-gap" name="radio2" type="radio" id="radio2" value="city" <%if(request.getParameter("level").equals("city")){ %> checked="checked" <%} %>/>
    						<span class="radio2">City-Level indicators</label>
  						</label>
  					</li>
  					<li class="collection-item" style="border-bottom: 0px;">
						<label>
							<input class="radiobtn with-gap" name="radio2" type="radio" id="radio3" value="project" <%if(request.getParameter("level").equals("project")){ %> checked="checked" <%} %>/>
							<span class="radio3">Project-Level Indicators</label>
						</label>
					</li>
				</ul>
			</div>
		</li> <!-- chiuso li collpasible -->
	</ul> <!-- chiuso ul collpasible -->
</form>
<ul class="collapsible" data-collapsible="expandable" id="collapsibleOne" > 

        <li>
          <div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="category"/></div>
          <div class="collapsible-body filter-container">

 <ul>
       <li style="padding: 2px !important">
    			<p>
      			<label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="cloud_queue" value="climate" />
                <span class="cloud_queue"> <i class="material-icons small left" style="font-size:1.5rem">cloud_queue</i> <i18n:message value="climate"/></label>
              
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one  filled-in" checked="checked" id="warning" value="airquality"  />
                <span class="warning"> <i class="material-icons small left" style="font-size:1.5rem">warning</i><i18n:message value="airquality"/></label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="euro_symbol" value="economic"/>
                <span class="euro_symbol"><i class="material-icons small left" style="font-size:1.5rem">euro_symbol</i> <i18n:message value="economic"/></label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="waves" value="water" />
                <span class="waves"><i class="material-icons small left" style="font-size:1.5rem">waves</i><i18n:message value="water"/></label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="nature_people" value="green" />
                <span class="nature_people"><i class="material-icons small left" style="font-size:1.5rem">nature_people</i><i18n:message value="green"/></label>
              </label>
              </p>
              </li>
              
            </ul>
	</div>
	</li>
	</ul>


<ul class="collapsible"  data-collapsible="expandable" id="collapsibleTwo" <%if(request.getParameter("level").equals("city")||request.getParameter("level").equals("project")){ %> style="display: block !important;"<%} else{ %> style="display: none !important;"<%}%>> 

        <li>
          <div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="category"/></div>
          <div class="collapsible-body filter-container">

 <ul>
              <li style="padding: 2px !important">
              
             <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="group" value="people" />
                <span class="group"> <i class="material-icons small left" style="font-size:1.5rem">group</i>People</label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two  filled-in" checked="checked" id="public" value="airquality"  />
                <span class="public"> <i class="material-icons small left" style="font-size:1.5rem">public</i>Planet</label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="attach_money" value="economic"/>
                <span class="attach_money"><i class="material-icons small left" style="font-size:1.5rem">attach_money</i> Prosperity</label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="graphic_eq" value="water" />
                <span class="graphic_eq"><i class="material-icons small left" style="font-size:1.5rem">graphic_eq</i>Propagation</label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="gavel" value="green" />
                <span class="gavel"><i class="material-icons small left" style="font-size:1.5rem">gavel</i>Governance</label>
              </label>
              </p>
              </li>
            </ul>
	</div>
	</li>
	</ul>


</div>
  </div>

				
	
	 	<div class="col s9" style="padding-left:35px">
			<input class="unalabInput" type="text" id="search" placeholder="<i18n:message value="typetosearch"/>" />
			<div class="tableContent">
			<table cellpadding="1" cellspacing="1" class="table table-hover sortable-table indexed" id="myTable2">
				<thead>
					<tr class="sorter-header">
						<th onclick="sortTable(0)" class="unalabTd no-sort"><u>ID</u></th>
						<th onclick="sortTable(1)" class="unalabTd no-sort"><u><i18n:message value="indicator"/></u></th>
						<th class="unalabTd no-sort" id="nbsTitle">NBS title</th>
						<th class="unalabTd no-sort">Category</th>

						<th class="unalabTd no-sort"><i18n:message value="value"/></th>
						<th onclick="sortTable(5)" class="unalabTd no-sort"><u><i18n:message value="unit"/></u></th>
						
						<th onclick="sortTable(6)" class="unalabTd is-date ascending"><u>Last Update</u></th>
						<th class="unalabTd no-sort"><i18n:message value="actions"/></th>

					</tr>
				</thead>
				<tbody id="myTable">

			
				</tbody>
			</table>
			</div>
			
			

		</div>
		
		

	</div>

<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
<!-- 		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div> -->
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px; float: left;">&copy; 2019 UNaLab All rights reserved</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>

<!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
    
<!--     <div class="switch center"> -->
<!--     <label> -->
<%--       <i18n:message value="manual"/> --%>
<!--       <input id="manualComputedSwitch" type="checkbox"> -->
<!--       <span class="lever customSwitch"></span> -->
<%--       <i18n:message value="computed"/> --%>
<!--     </label> -->
<!--   </div> -->
    <div id="manualForm">
      <h5><i18n:message value="addmanual"/></h5>

      <div class="row">
        <form action="/cpm_v2/AddKPI?lang=${lang}" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="id" name="id" type="number" class="invalid validate unalabInput" required>
              <label class="unalabLabel" for="id">ID</label>
              <span id="idError" class="red-text">This field is required</span>   
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="name" name="name" type="text" class="invalid validate unalabInput materialize-textarea" required></textarea>
              <label for="name"><i18n:message value="name"/></label>
              <span id="newNameError" class="red-text">This field is required</span>   
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="description" name="desc" type="text" class="invalid materialize-textarea validate unalabInput" required></textarea>
              <label for="description"><i18n:message value="description"/></label>
              <span id="newDescError" class="red-text">This field is required</span>   
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unit" type="text" name="unit" class="invalid validate unalabInput" required>
              <label for="unit"><i18n:message value="unit"/></label>
              <span id="newUnitError" class="red-text">This field is required</span>   
            </div>
          </div>   
          
            <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="nbs" type="text" name="nbs" class="invalid validate unalabInput" required>
              <label for="nbs">NBS Title</label>
              <span id="newNbsError" class="red-text">This field is required</span>   
            </div>
          </div>   
          <div class="row modal-form-row">  
             
			<div class="input-field col s12">
						<input id="c" name="city" type="hidden" value="${param.city}"></input> <span><i18n:message value="level"/></span> 
						<select
							id="level" name="level">
<!-- 							<option value="task">Common Indicators</option> -->
							<option value="city"><i18n:message value="citylevel"/></option>
							<option value="project"><i18n:message value="projectlevel"/></option>
						</select> 
							
							</div>
							
							<div class="input-field col s12">
							<span><i18n:message value="category"/></span> 


						 
						<select id="cat2" name="cat2">

							<option value="people"><i18n:message value="people"/></option>
							<option value="planet"><i18n:message value="planet"/></option>
							<option value="prosperity"><i18n:message value="prosperity"/></option>
							<option value="governance"><i18n:message value="governance"/></option>
							<option value="propagation"><i18n:message value="propagation"/></option>


						</select>
						</div>
						</div>
						
						 <div class="row modal-form-row">  
             
			<div class="input-field col s6">
						 <span>Choose the indicator type</span> 
						<select 
							id="indType" name="indType">
<!-- 							<option value="task">Common Indicators</option> -->
							<option value="citymanual">Manual</option>
							<option value="computed">Computed</option>
						</select> 
							
				</div>			
<%-- 							<span><i18n:message value="category"/></span>  --%>

			<div id="autoSelect" class="input-field col s6 hide">
						 <span>Choose an already existing computed indicator to be added</span> 
						<select id="auto" name="auto">



						</select> 
						</div>
						</div>
						
						<div id="createAuto" class="row modal-form-row hide">  
						<span>If the indicator you want to add is not in the computed indicators list, click on button to create it.</span>
						<button onclick="createFromKPIEngine()" type="button" id="submit2" name="submit2" style="color: white" class=" modal-action modal-close waves-effect waves-green unalabGreen btn-flat" >CREATE</button>
      
						</div>
						
						 <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="add"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>
						
				</form>
      </div>
      </div>
      
      
      <!-- Computed Indicator Adding Form -->
      <div id="computedForm">
      <h5><i18n:message value="addcomputed"/></h5>

      <div class="row">
        <form action="/cpm_v2/AddComputedKPI?lang=${lang}" method="post">
        
          <div class="row modal-form-row">
          
          <div class="dropdownSelect">
					
         
		   <a class='dropdown-trigger btn-flat truncate' href='#' data-target='compKPI'><i class="material-icons right">arrow_drop_down</i> <i18n:message value="knowage"/></a> 
            	<ul id="compKPI"  class='dropdown-content'>

			</ul>
			
          </div>
          <span style="padding-left:20px" id="indicatorValue" class="dropdownValue"><i18n:message value="none"/></span>
          </div>
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="idComputed" name="id" type="number" class="validate unalabInput" required>
              <label class="unalabLabel" for="id">ID</label>
            </div>
          </div>
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="nameComputed" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label for="name"><i18n:message value="name"/></label>
            </div>
          </div>
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="descriptionComputed" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label for="description"><i18n:message value="description"/></label>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unitComputed" type="text" name="unit" class="validate unalabInput" required>
              <label for="unit"><i18n:message value="unit"/></label>
            </div>
          </div>  
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="nbsComputed" type="text" name="nbs" class="validate unalabInput" required>
              <label for="nbs">NBS Title</label>
            </div>
          </div>     
          <div class="row modal-form-row">  
             
			<div class="input-field col s12" id="compForm">
						
						<input name="city" type="hidden" value="${param.city}"></input> 
						<span><i18n:message value="level"/></span> 
						<select
							id="computedLevel" name="level">

							<option value="city"><i18n:message value="citylevel"/></option>
							<option value="project"><i18n:message value="projectlevel"/></option>
						</select> 
							
							</div>
							
							<div class="input-field col s12">
							<span><i18n:message value="category"/></span> 

						<select id="computedCat2" name="cat2">

							<option value="people"> <i18n:message value="people"/></option>
							<option value="planet"><i18n:message value="planet"/></option>
							<option value="prosperity"><i18n:message value="prosperity"/></option>
							<option value="governance"><i18n:message value="governance"/></option>
							<option value="propagation"><i18n:message value="propagation"/></option>


						</select>
						</div>
						</div>
						
						 <div class="modal-footer">
   
      <button type="submit" id="submitComputed" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="add"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>
						
				</form>
      </div>
      </div>
      
      
    </div>
   
  </div>
  
  
  <!-- Modal Structure -->
  <div id="modal3" class="modal">
    <div class="modal-content">
      <h5 id="titlemodale" class="title"></h5>

	
		
      <div class="row" id="overLapping">
        <form action="/cpm_v2/ModifyCityKPI?lang=${lang}" method="post" id="editForm">
        <div class="row modal-form-row">
            <div class="input-field col s12">
            
              <input id="tableId" name="tableId" type="number" class="validate unalabInput" disabled>
              <label style="margin-top: -20px" class="unalabLabel active"  for="tableId">ID</label>
            </div>
          </div>
       
       <div class="row modal-form-row"> 
       <div class="input-field col s6">  
          <p >
      		<label class="tooltipped" data-position="bottom" data-tooltip="<i18n:message value="check"/>">
      			
        			<input id="checkCityComputed" type="checkbox" name="setComp" value="automatic"/>
        			<span><i18n:message value="computed"/></span>
        			
      		</label>
    		</p>
    		</div>
    	  
    	  
    	  
    	  
          
          <div id="autoSelect2" class="input-field col s6 hide">
						 <span>COMPUTED INDICATORS</span> 
						<select id="auto2" name="auto2">
							


						</select> 
			</div>
          
          </div>
          
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="nameEdit" name="name" type="text" class="unalabInput materialize-textarea"></textarea>
              
              
              <label style="margin-top: -20px" class="unalabLabel active" for="name"><i18n:message value="name"/></label>
			<span id="nameError" class="hide red-text">This field is required</span>            
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="descEdit" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="description"><i18n:message value="description"/></label>
              <span id="descError" class="hide red-text">This field is required</span>   
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unitEdit" type="text" name="unit" class="validate unalabInput" required>
              <label style="margin-top: -20px" class="unalabLabel active" for="unit"><i18n:message value="unit"/></label>
              <span id="unitError" class="hide red-text">This field is required</span>   
            </div>
          </div> 
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="nbsEdit" type="text" name="nbs" class="validate unalabInput" required>
              <label style="margin-top: -20px" class="unalabLabel active" for="nbs">NBS Title</label>
              <span id="nbsError" class="hide red-text">This field is required</span>   
            </div>
          </div> 
          <c:out value="${param['id']}"></c:out>
	    
		
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="KPI_id" />
		
		
		
		
		<div class="modal-footer">
   
      <button type="submit" id="submitEdit" name="submit" class="enabled modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="confirm"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>       
       
          </form>
          
          </div>
          </div>
          </div>   
           
  <div id="dashboardContainer" class="row z-depth-1 hide" style="margin:40px; width: 95%;">
  
  	 <div class="row">
  
  		<div id="canvasContainer" class="col s6">
  			<canvas id="chart1" style="display:none; width:1000; height:300" class="chartjs-render-monitor"></canvas> 
  		</div>
  		
  		<div class="col s2" style="margin-top: 4%;">
	  		<div class="input-field col s12">
		      	<select id="timeUnit">
					<option value="second">Second</option>
					<option value="minute">Minute</option>
					<option value="hour">Hour</option>
					<option value="day" selected>Day</option>
					<option value="month">Month</option>
					<option value="year">Year</option>
				</select>
				<label>Choose date format</label>
			</div>
			<div class="col s12">
				<div class="varianceContainer">
					<i class="material-icons" style="font-size:40px; color:orange;">import_export</i>
					<p class="kpiText">Variance:<span id="varianceValue"></span></p>
				</div>
				<div class="varianceContainer">
					<i class="material-icons" style="font-size:40px; color:red;">trending_down</i>
					<p class="kpiText">Minimum:<span id="minValue"></span></p>
				</div>
				<div class="varianceContainer">
					<i class="material-icons" style="font-size:40px; color:green;">trending_up</i>
					<p class="kpiText">Maximum:<span id="maxValue"></span></p>
				</div>	
				<div class="varianceContainer">
					<i class="material-icons" style="font-size:40px; color:blue;">sync_alt</i>
					<p class="kpiText">Average:<span id="avgValue"></span></p>
				</div>		
			</div>
  		</div>
  		
 		
        
        <div class="col s4">
        	<table id="table1" class="responsive-table" style="display:none;">
        
            <thead>
	          <tr>
	              <th>Date</th>
	              <th>Value</th>
	              <th>Note</th>	              
	          </tr>
	        </thead>

	        <tbody id="body1">
	          
	        </tbody>
        </table> 
        
        </div>
      </div>
      <div class="row" style="padding-left: 6%;">
      	<div class="input-field col s2">
      		<input id="filterFrom" type="text" class="datepicker"/>
      		<label for="filterFrom">Filter data from</label>
      	</div>
      	<div class="input-field col s2">
      		<input id="filterTo" type="text" class="datepicker"/>
      		<label for="filterTo">Filter data to</label>
      	</div>
      </div>
           
    </div> 
    
      
      
      
      
      
      
  <!-- Modal Structure -->
  <div id="modal2" class="modal">
    <div class="modal-content">
      <h5 class="title"></h5>

      <div class="row">
        <form action="/cpm_v2/EditKPI?lang=${lang}" method="post">
          <div class="row modal-form-row" id="modal-body">
          
          <c:out value="${param['id']}"></c:out>
	    <input type="hidden" name="title" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="tableId" />
          </div>
          <div class="row">
            <div class="input-field col s12">
            
              <input class="unalabInput" id="value" name="value" type="number" placeholder="0.00" min="0" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" required>
              <label for="value"><i18n:message value="value"/></label>
              
            </div>
            
            <div class="input-field col s12">
              <textarea id="motivation" name="motivation" type="text" class="materialize-textarea validate unalabInput"></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="description"><i18n:message value="note"/></label>
            </div>
          </div>
          
          <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="set"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>       
       </form>
      </div>
    </div>
    
  </div>


<!-- Modal Structure -->
  <div id="modalcity" class="modal">
    <div class="modal-content">
      <h5 class="title"></h5>

      <div class="row">
        <form action="/cpm_v2/EditCityKPI?lang=${lang}" method="post">
          <div class="row modal-form-row" id="modal-body">
          
          <c:out value="${param['id']}"></c:out>
	    <input type="hidden" name="title" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="tableId" />
          </div>
          <div class="row">
            <div class="input-field col s12">
            
              <input class="unalabInput" id="value" name="value" type="number" placeholder="0.00" min="0" step="0.01" pattern="^\d+(?:\.\d{1,2})?$" required>
              <label for="value"><i18n:message value="value"/></label>
            </div>
            
             <div class="input-field col s12">
              <textarea id="motivation" name="motivation" type="text" class="materialize-textarea validate unalabInput" ></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="description"><i18n:message value="note"/></label>
            </div>
          </div>
          
          <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" ><i18n:message value="set"/></button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" ><i18n:message value="cancel"/></a>
    </div>       
       </form>
      </div>
    </div>
    
  </div>

<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<script type="text/javascript" src="js/materialize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.0/Chart.min.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/paginathing.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<!-- <script type="text/javascript" src="js/table-sorter.js"></script> -->
<script type="text/javascript" src="js/calculations.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/view.js"></script>
</body>
</html>