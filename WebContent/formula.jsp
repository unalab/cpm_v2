<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="main.java.Queries"%>
<%@page import="java.util.ArrayList"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<% String role = (String) request.getSession().getAttribute("user");
   String [] array = role.split("_");
   String c= array[0];
   Integer id = Queries.getCityId(c);
   
   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
      <meta charset="ISO-8859-1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>City Performance Monitor</title>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
      <link type="text/css" rel="stylesheet" href="css/customEngine.css" media="screen,projection" />
      <link type="text/css" rel="stylesheet" href="css/codemirror.css" media="screen,projection" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   	  <!-- <link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> -->
      <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" />
      <!-- <link type="text/css" rel="stylesheet" href="css/common.css" media="screen,projection" /> -->
   	  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
   </head>
   <% //In case, if Admin session is not set, redirect to Login page
      if((request.getSession(false).getAttribute("user")== null) )
      {
      %>
   <jsp:forward page="login.jsp"></jsp:forward>
   <%} 
      %>
   <body>
      <nav id="header">
         <div class="nav-wrapper customW">
            <a href="index.jsp" class="brand-logo"><img src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo" style="width: 42%"></a> 
            <span id="titleCity" class="brand-logo secondary-text" style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>
            <span class="text_center hide">
            <span style="text-align: center" id="testo"></span>
            </span>
            <ul class="right">
               <li>
                  <a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4">
                     <i18n:message value="help"/>
                  </a>
               </li>
               <li>
                  <a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4">
                     <i18n:message value="citydashboard"/>
                  </a>
               </li>
               <li>
                  <a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4">
                     <i18n:message value="citizenview"/>
                  </a>
               </li>
               <% if (role == "admin"){%>
               <li>
                  <a href="/cpm_v2/GetAdminKPIList?city=3&level=task&category=all&lang=${lang}" class="white-text text-darken-4">
                     <i18n:message value="expert"/>
                  </a>
               </li>
               <%} else {
                  %>		
               <li>
                  <a href="/cpm_v2/view.jsp?city=<%=id%>&level=task&category=all&lang=${lang}" class="white-text text-darken-4">
                     <i18n:message value="expert"/>
                  </a>
               </li>
               <% } %>
               <li>
                  <a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
                  <span class="activeLang ">${lang}</span>
                  <i class="material-icons left ">language</i>
                  <i class="material-icons right">arrow_drop_down</i>
                  </a>
                  <ul id="languagemenu_big"  class='dropdown-content'>
                     <li>
                        <a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=en">
                           <i id="en" class="inline material-icons hide">done</i>
                           <i18n:message value="english"/>
                        </a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=it">
                           <i id="it" class="inline material-icons hide">done</i>
                           <i18n:message value="italian"/>
                        </a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=fi">
                           <i id="fi" class="inline material-icons hide">done</i>
                           <i18n:message value="finnish"/>
                        </a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=du">
                           <i id="du" class="inline material-icons hide">done</i>
                           <i18n:message value="dutch"/>
                        </a>
                     </li>
                  </ul>
               </li>
               <li class="right">
                  <a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
                  <i class="material-icons left">perm_identity</i>
                  <span class="val"><%=c%>-admin</span> 
                  <i class="material-icons right">arrow_drop_down</i>
                  </a>
                  <ul id="userdropdown_big"  class='dropdown-content'>
                     <li>
                        <a href="/cpm_v2/LogoutServlet">
                           <i18n:message value="logout"/>
                        </a>
                     </li>
                  </ul>
               </li>
            </ul>
         </div>
      </nav>
      <nav class="clean" id="bread">
         <div class="nav-wrapper">
            <div class="col s12">
               <a id="measureO" class="breadcrumb">Select measures</a>
               <a id="formulaO" class="breadcrumb selectedBread">Build KPI Formula</a>
               <a id="scheduleO" class="breadcrumb">Schedule KPI Calculation</a>
            </div>
         </div>
      </nav>
      <div class="row">
         <div class="col s12 m12 l2">
            <div class="nav-wrapper"> 
                  <ul class="collapsible" data-collapsible="expandable">
                     <li>
                        <div class="collapsible-header active managerFilter">
                           <i class="material-icons">label</i>
                           <span>Show</span>
                        </div>
                        <div class="collapsible-body filter-container">
                           <ul class="collection">
                              <li class="collection-item" style="border-bottom: 0px;">
                                 <label>
                                 <input class="radiobtn with-gap" name="radio1" type="radio" id="radio1"  value="measures" checked="checked" />
                                 <span class="radio1">Measures</span>
                                 </label>
                              </li>
                              <li class="collection-item" style="border-bottom: 0px;">
                                 <label>
                                 <input class="radiobtn with-gap" name="radio1" type="radio" id="radio2" value="formulas" />
                                 <span class="radio1">Formulas</span>
                                 </label>
                              </li>
                           </ul>
                        </div>
                     </li>
                  </ul>
               <ul class="collapsible" data-collapsible="expandable" id="collapsibleOne" >
                   <li>
                     <div class="collapsible-header active managerFilter">
                        <i class="material-icons">label</i>
                        <span>Filters</span>
                     </div>
                     <div class="collapsible-body filter-container">
                        <ul>
                           <li style="padding: 2px !important">
                              <p>
                                 <label>
                                    <input type="checkbox" name="measure-filter" class="filled-in" id="custom" value="custom" checked="checked"/>
                                    <span><i class="material-icons small left" style="font-size:1.5rem">folder_open</i>Custom measures</span>
                                 </label>
                              </p>
                           </li>
                           <li style="padding: 2px !important">
                              <p>
                                 <label>
                                    <input type="checkbox" name="measure-filter" class="filled-in" id="kpi" value="kpi" checked="checked"/>
                                    <span><i class="material-icons small left" style="font-size:1.5rem">insert_chart_outlined</i>KPI measures</span>
                                 </label>
                              </p>
                           </li>
                           <li style="padding: 2px !important">
                              <p>
                                 <label>
                                    <input type="checkbox" name="measure-filter" class="filled-in" id="iot" value="iot" checked="checked"/>
                                    <span><i class="material-icons small left" style="font-size:1.5rem">settings_remote</i>IoT measures</span>
                                 </label>
                              </p>
                           </li>                         
                        </ul>
                     </div>
                  </li> 
               </ul>			            
            </div>
         </div>
         <div id="measures" class="col s12 m12 l5">
            <h5 class="center"><span id="measuresShow"></span></h5>
            <!--       		<div class="card-panel measureCard"> -->
            <!--         		<div class="card-content black-text"> -->
            <!--           	     	<fieldset> -->
            <!--     					<div class="chip-group" id="chips" tabindex="-1"> -->
            <!--       					</div> -->
            <!--   					</fieldset> -->
            <!--      			</div> -->
            <!--      		</div> -->
            <input class="unalabInput col s10 m10 l10" type="text" id="search3" placeholder=""></input>
            <div class="col s2 m2 l2">
               <i id="sorter" class="material-icons small sortIcon" onclick="orderByName()">keyboard_arrow_down</i>         
            </div>
            <div class="col s12 m12 l12">
            	<ul class="collection collectionMeasuresWide"></ul>
            </div>
            <!--      		 <form action="" class="browser-default right"> -->
            <!--         <input id="search-input" placeholder="Search measures" type="text" class="browser-default search-field" name="q" value="" autocomplete="off" aria-label="Search box"> -->
            <!--          <label for="search-input"> -->
            <!--          <i class="material-icons search-icon">search</i> -->
            <!--          </label>   -->
            <!--          <i class="material-icons search-close-icon">cancel</i>  -->
            <!--         <div class="search-popup"> -->
            <!--           <div class="search-content"> -->
            <!--             <label class="search-heading">Most Recent</label> -->
            <!-- <!--             <ul class="popup-list"> --> 
            <!-- <!--             </ul> --> 
            <!--           </div> -->
            <!--         </div> -->
            <!--       </form> -->
            <!--      		<div id="operators" class=""> -->
            <!--    			<h4 class="center">Operators</h4> -->
            <!--       		<div class="card-panel operatorsCard"> -->
            <!--         		<div class="card-content black-text"> -->
            <!--           	     	<fieldset> -->
            <!--     					<div class="chip-group" id="ops" tabindex="-1"> -->
            <!--       						<div class="chip chip-checkbox" aria-labelledby="sum" tabindex="0" role="checkbox" aria-checked="false"> -->
            <!--         						<input type="checkbox" name="checkEx2" /> -->
            <!--         						<span id="sum">SUM</span> -->
            <!--         					</div> -->
            <!--         					<div class="chip chip-checkbox" aria-labelledby="avg" tabindex="0" role="checkbox" aria-checked="false"> -->
            <!--         						<input type="checkbox" name="checkEx2" /> -->
            <!--         						<span id="avg">AVG</span> -->
            <!--         					</div> -->
            <!--         					<div class="chip chip-checkbox" aria-labelledby="min" tabindex="0" role="checkbox" aria-checked="false"> -->
            <!--         						<input type="checkbox" name="checkEx2" /> -->
            <!--         						<span id="min">MIN</span> -->
            <!--         					</div> -->
            <!--         					<div class="chip chip-checkbox" aria-labelledby="max" tabindex="0" role="checkbox" aria-checked="false"> -->
            <!--         						<input type="checkbox" name="checkEx2" /> -->
            <!--         						<span id="max">MAX</span> -->
            <!--         					</div> -->
            <!--       						<div class="chip chip-checkbox" aria-labelledby="count" tabindex="0" role="checkbox" aria-checked="false"> -->
            <!--         						<input type="checkbox" name="checkEx2" /> -->
            <!--         						<span id="count">COUNT</span> -->
            <!--         					</div> -->
            <!--       					</div> -->
            <!--   					</fieldset> -->
            <!--      			</div> -->
            <!--      		</div> -->
            <!--   		</div> -->
         </div>
         <div id="textarea" class="col s12 m12 l5">
            <div class="wrapper">
               <fieldset id="operatori">
                  <div class="chip-group" id="ops" tabindex="-1">
                     <h5 id="operatorsTitle">Operators (drag and drop) :     </h5>
                     <div class="chip chip-checkbox movable" aria-labelledby="sum" tabindex="0" role="checkbox" aria-checked="false">
                        <span id="sum">SUM</span>
                     </div>
                     <div class="chip chip-checkbox movable" aria-labelledby="avg" tabindex="0" role="checkbox" aria-checked="false">
                        <span id="avg">AVG</span>
                     </div>
                     <div class="chip chip-checkbox movable" aria-labelledby="min" tabindex="0" role="checkbox" aria-checked="false">
                        <span id="min">MIN</span>
                     </div>
                     <div class="chip chip-checkbox movable" aria-labelledby="max" tabindex="0" role="checkbox" aria-checked="false">
                        <span id="max">MAX</span>
                     </div>
                     <div class="chip chip-checkbox movable" aria-labelledby="count" tabindex="0" role="checkbox" aria-checked="false">
                        <span id="count">COUNT</span>
                     </div>
                  </div>
               </fieldset>
<!--                <input id="oldFormula" class="hide" value="" name="oldFormula"></input>
 -->               <p id="oldFormula" class="hide" style="padding-left: 3%;">Editing formula: <span id="editFormulaName"></span></p>
               <textarea id="formula" ondrop="dropText(event)" ondragover="allowDrop(event)"></textarea>
               <div id="clearFormula">
				  <a class="btn btn-right formulabtn modal-trigger" onclick="clearFormula()">Clear</a>
			   </div>
               <div id="saveFormula">
                  <a id="formulaBtn" class="btn btn-right formulabtn modal-trigger disabled" href="#saveRES">Save Formula</a>
               </div>
               <div id="preview">
                  <a class="btn btn-right formulabtn modal-trigger" href="#modalRES" onclick="getFormula()">Preview</a>
               </div>
            </div>
         </div>
      </div>
      <div id="modalRES" class="modal">
         <div class="modal-content">
            <h5>Formula Result Preview</h5>
            <p class="ris"></p>
         </div>
         <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
         </div>
      </div>
      <div id="saveRES" class="modal">
         <div class="modal-content">
            <div class="input-field col s3 ">
               <input value="" id="formulaName" type="text" class="validate formulaName">
               <label id="measureLabel" for="measure">Insert a name for this formula</label>
            </div>
         </div>
         <div class="modal-footer">
            <a onclick="saveFormula()" class="modal-close waves-effect waves-green btn-flat">Save</a>
            <a onclick="clearFormula()" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
         </div>
      </div>
   </body>
   <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/materialize.js"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/6.6.0/math.min.js"></script>
   <script type="text/javascript" src="js/formula.js"></script>
   <script type="text/javascript" src="js/pagination.js"></script>
   <script type="text/javascript" src="js/paginathing.js"></script>
   <script type="text/javascript" src="js/variable.js"></script>