package main.kpiEngine.service;


import java.util.List;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;

import main.java.PropertyManager;
import main.java.manualKPIProperties;

public abstract class InfluxManager {
	
	
	
	
	
	
	public static void createInfluxDb(String dataset) {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		InfluxDB influxDB = InfluxDBFactory.connect(url);
	
		influxDB.createDatabase(dataset);
		//QueryResult q = influxDB.query(new Query("CREATE DATABASE \"" + dataset + "\"", null ));
		influxDB.close();
		
	}

	public static void createInfluxCityMeasurement(String motivation, String dataset, String deviceId, Double value,String chiave) throws Exception {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		InfluxDB influxDB = InfluxDBFactory.connect(url);
		
	    influxDB.setDatabase(dataset);

	    influxDB.write(Point.measurement(chiave)
	    		.tag("name", deviceId)
	    		.tag("motivation", motivation)
	    	    .addField("value", value)
	    	    .build());
	    
	    influxDB.close();
		
	}
	
	public static void createInfluxMeasurement(String dataset, String deviceId, Double value,String chiave) throws Exception {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		InfluxDB influxDB = InfluxDBFactory.connect(url);
		
	    influxDB.setDatabase(dataset);

	    influxDB.write(Point.measurement(chiave)
	    		.tag("name", deviceId)
	    		.tag("motivation", "computed value")
	    	    .addField("value", value)
	    	    .build());
	    
	    influxDB.close();
		
	}

	
	public static Boolean checkInfluxDatabase(String name) {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		
		InfluxDB influxDB = InfluxDBFactory.connect(url);
		boolean isDatabaseExists = false;
		Query q = new Query ("SHOW DATABASES", "influxDb");
		System.out.println("QUERY: " + q.toString());
		QueryResult queryResult = influxDB.query(q);
			
		List<Result> res = queryResult.getResults();
		//System.out.println(res.get(0).getSeries().get(0).getValues().get(0).get(1).toString());
		for (int i = 0; i<res.get(0).getSeries().get(0).getValues().size(); i++) {
			String value = res.get(0).getSeries().get(0).getValues().get(i).get(0).toString();
			System.out.println(value);
			if (value.equalsIgnoreCase(name)) {
				isDatabaseExists = true;
			}
		}
		
		influxDB.close();
		System.out.println(isDatabaseExists);
		return isDatabaseExists;
	}

	public static void createInfluxMeasurementManual(String motivation, String database, String name, double value,
			String chiave) {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		InfluxDB influxDB = InfluxDBFactory.connect(url);
		influxDB.setDatabase(database);
//	    Measure measure = new Measure();
//	    measure.setDateObserved(dateObserved);
//	    measure.setName(deviceId);
//	    measure.setValue(value);
	    influxDB.write(Point.measurement(chiave)
	    		.tag("name", name)
	    		.tag("motivation", motivation)
	    	    .addField("value", value)
	    	    .build());
	    
	    influxDB.close();
		
	}
}
