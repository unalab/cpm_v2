package main.kpiEngine.service;

import org.quartz.JobExecutionException;


import java.util.Iterator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import java.beans.Expression;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;

import org.quartz.JobExecutionContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.quartz.Job;

import main.java.DBConnectionEngine;
import main.java.PropertyManager;
import main.java.manualKPIProperties;
import main.utils.*;

public class CalculateKPI implements Job
{
    
    public void execute(final JobExecutionContext jExeCtx) throws JobExecutionException {
        
        JSONArray kpis = null;
		try {
			kpis = Queries.getKPIs();
			for (int i = 0; i <kpis.length(); i++) {
	        	JSONObject kpi = null;
	        	kpi = (JSONObject) kpis.get(i);
	        	String status = null;
	        	status = (String) kpi.get("status");
	        	if("SCHEDULED".equalsIgnoreCase(status)) {
	        		Queries.updateFormulaStatus((Integer) kpi.get("id"), "PLAYED");
	        		status = "PLAYED";
	        	}
	        	if(status.equalsIgnoreCase("PLAYED")) {
	        		Integer id = null;
	        		id = (Integer) kpi.get("id");
	        		String formula = null;
	        		formula = (String) kpi.get("formula");
	        		String name = null;
	        		name = (String) kpi.get("name");
	        		String city = kpi.getString("city");
	        		calculateFormula(id, formula, name, city);	
	        	}
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
			
        	
       
    }

	public void calculateFormula(Integer id, String formula, String name, String city) throws Exception {
		
		System.out.println("FORMULA: " + formula);
		String[] pieces = formula.split("[+-/*]");
		
		for (int i=0; i<pieces.length; i++) {
			boolean isNumber = isNumeric(pieces[i]);
			if(!isNumber) {
				 int j = pieces[i].indexOf("(");
				 if(j > 0) {
					String measure = pieces[i].substring(j+1).replace(")", "");
					String funzione = pieces[i].substring(0, j);
					Stat result = Queries.execMeasure(measure);
					ArrayList<Double> d = new ArrayList<Double>();
					
					String type = result.getType();
					System.out.println("TYPE OF FORMULA: " + type);
					switch(type) {
					case "sql":						
						d = Queries.getSqlValues(result.getUrl(),result.getPassword(),result.getUsername(),result.getQuery());						
						break;
					case "influx":
						d=Queries.getInfluxValues(result.getUrl(),result.getPassword(),result.getUsername(),result.getQuery());
						break; 
				 	case "rest":
						d=Queries.getRestValues(result.getUrl(), result.getQuery(), result.getHeaders());
						break;
					case "unalab":
						d=Queries.getInfluxValues(result.getUrl(),result.getPassword(),result.getUsername(),result.getQuery());
						break;
					}
					
					System.out.println(d);
					Double res = Utils.executeFunction(d,funzione);
					formula = formula.replace(pieces[i], res.toString());
					System.out.println(formula);
				 }
				
			}
		}
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");        
		Double result = (Double) engine.eval(formula);
		System.out.println("RISULTATO: "+result);
		String fiware_service = null;
        String fiware_servicepath = null;
        fiware_service = city.toLowerCase();
        fiware_servicepath = "/"+city.toLowerCase()+"_kpi/harmonized";
        final Integer idEntity = id;
        System.out.println("idEntity= " + idEntity);
        
        
    
        SubscriptionManager.updateEntity(idEntity.toString()+"_kpiEngine", result.toString(), fiware_service, fiware_servicepath);
        String database = city.toLowerCase()+"_KPI"+idEntity.toString();
        boolean isExists = InfluxManager.checkInfluxDatabase(database);
        if(!isExists) {
        	InfluxManager.createInfluxDb(database);
        	InfluxManager.createInfluxMeasurement(database, name, result, "KPI_Values");
     		saveAsMeasure(database,name, city.toLowerCase());
	    }
		
	}
	
	public static boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}
	
	
	public static void saveAsMeasure(String db, String measurement, String city) {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		url = url+db;
		
		String query = "SELECT value FROM KPI_Values";
		String type = "influx";
		String measure = measurement;
	
		
//		String storageURL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String storageUser ="root";
//		String storagePass = "mysql";
		String storageQuery = "INSERT INTO statement (type, url, query, measureName, city) VALUE ('"+type+"', '"+url+"', '"+query+"', '"+measure+"' , '"+city+"')";
		System.out.println(storageQuery);
		Connection connection;				
		
        Statement st=null;
        ResultSet rs = null;
        try {
        	connection = DBConnectionEngine.createConnection();
        	String valid = checkName(connection, measure);
 			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
 			st = connection.createStatement();
// 			if(Boolean.parseBoolean(valid)) {
 	 			st.executeUpdate(storageQuery);
// 			} else {
// 				System.out.println("Measure with name:" + measure + " already exists!");
// 			}
 			connection.close();
 		} catch (SQLException e) {		
 				e.printStackTrace();		
 		}
	}
	
	private static String checkName(Connection connection, String measure) {
		Statement st=null;
		ResultSet rs = null;
		String valid = null;
		String checkQuery = "SELECT EXISTS(SELECT * from statement WHERE measureName=\""+measure+"\") as valid";
		try {
			st = connection.createStatement();
			rs = st.executeQuery(checkQuery);
			if (rs != null) {
                while (rs.next()) {
                    valid = rs.getString("valid");
                }
            }
            //connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return valid;
	}
}
