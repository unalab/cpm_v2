package main.kpiEngine.service;

import java.net.URL;

public abstract class CommonUtils
{
    public static boolean isValidURL(final String urlString) {
        boolean out = false;
        try {
            final URL url = new URL(urlString);
            url.toURI();
            out = true;
        }
        catch (Exception exception) {
            out = false;
        }
        return out;
    }
}
