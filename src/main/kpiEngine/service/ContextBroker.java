package main.kpiEngine.service;

import org.json.JSONObject;

import java.util.Set;

import main.bean.CBEntity;
import main.tools.*;

public abstract class ContextBroker extends Tool
{
    protected ContextBroker(final String baseurl) throws Exception {
        super(baseurl);
    }

    protected abstract String getEntity(final String p0, final String p1, final String p2);

    protected abstract String getEntities(final String p0, final String p1);

    protected abstract JSONObject postEntities(final String p0, final String p1, final Set<? extends CBEntity> p2);
}
