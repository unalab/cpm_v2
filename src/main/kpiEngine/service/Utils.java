package main.kpiEngine.service;

import java.util.ArrayList;

public class Utils {

	public static Double executeFunction(ArrayList<Double> d, String function) {
		Double res = null;
		switch(function) {
		case "SUM":
			res = executeSum(d);
			break;
		case "AVG": 
			res =executeAvg(d);
			break;
		case "MIN":
			res = executeMin(d);
			break;
		case "MAX":
			res = executeMax(d);
			break;
		case "COUNT":
			res = executeCount(d);
			break;
		}
		
		return res;
	}

	private static Double executeCount(ArrayList<Double> d) {
		Double count;
		count = (double) d.size();
		return count;
	}

	private static Double executeMax(ArrayList<Double> d) {
		Double max = -1000.00;
		for (int i=0; i<d.size(); i++) {
			max = Math.max(max, d.get(i));
		}
		return max;
		
	}

	private static Double executeMin(ArrayList<Double> d) {
		Double min = 1000.00;
		for (int i=0; i<d.size(); i++) {
			min = Math.min(min, d.get(i));
		}
		return min;
		
	}

	private static Double executeAvg(ArrayList<Double> d) {
		Double avg;
		Double sum = executeSum(d);
		
		avg = sum / d.size();
		return avg;
	}

	private static Double executeSum(ArrayList<Double> d) {
		Double sum = 0.0;
		for (int i=0; i<d.size(); i++) {
			sum = sum + d.get(i);
		}
		return sum;
	}

}
