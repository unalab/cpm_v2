package main.kpiEngine;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import main.kpiEngine.service.CalculateKPI;
import main.kpiEngine.service.JobBean;
import main.kpiEngine.service.Queries;


@WebServlet("/jobs")
public class Jobs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public Jobs() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String replay = request.getParameter("replay");
		String start, num, interval, date, time;
		int id_formula = 0;
		start = num = interval = date = time = "";
		if("true".equalsIgnoreCase(replay)) {
			JobBean job = fetchJob(Integer.parseInt(id));
			if(job != null) {
				id = Integer.toString(job.getId());
				start = job.getStart();
				num = Integer.toString(job.getNum());
				interval = job.getInterval();
				date = job.getDate();
				time = job.getTime();
				id_formula = job.getId_formula();
			} else {
				System.out.println("Starting of the KPI was unsuccessful.");
				return;
			}		
		} else {
			start = request.getParameter ("start");
			num = request.getParameter("num");
			interval = request.getParameter("interval");
			date = request.getParameter("date");
			time = request.getParameter("time");
			id_formula = Integer.parseInt(request.getParameter("id"));
		}		
		System.out.println("JOB EXECUTION...");
		if ("now".equalsIgnoreCase(start)) {
			final JobDetail job = JobBuilder.newJob((Class)CalculateKPI.class).withIdentity(id).build();
			System.out.println("KEY JOB:" +job.getKey());
			Queries.updateJob(id, job.getKey().getName());
			int seconds;
			Trigger trigger = null;
			seconds = Integer.parseInt(num);
			switch(interval) {
			case "seconds":
				
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(seconds).repeatForever()).build();
				break;
			case "minutes":
				
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(seconds).repeatForever()).build();
				break;
			case "hours":
				
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			case "days":
				
				seconds = seconds*24;
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			case "months":
				
				seconds = seconds*24*30;
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			}
			
	        Queries.updateFormulaStatus(id_formula, "PLAYED");		
			 
			final SchedulerFactory schFactory = (SchedulerFactory)new StdSchedulerFactory();
			Scheduler sch = null;
			try {
				sch = schFactory.getScheduler();
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sch.start();
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sch.scheduleJob(job, trigger);
				
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			final JobDetail job = JobBuilder.newJob((Class)CalculateKPI.class).withIdentity(id).build();
			System.out.println("KEY JOB:" +job.getKey());
			Queries.updateJob(id, job.getKey().getName());
			int seconds;
			Trigger trigger = null;
			seconds = Integer.parseInt(num);
			String[] dateArr;
			Date startDate;
			String[] timeArr = time.split(":");
			if("true".equalsIgnoreCase(replay)) {
				dateArr = date.split("-");
				startDate = DateBuilder.dateOf(Integer.parseInt(timeArr[0]), Integer.parseInt(timeArr[1]), Integer.parseInt(timeArr[2]), Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0]));
			} else {
				dateArr = date.split("/");
				String year = "";
				if(dateArr[2].length() > 2) {
					year = dateArr[2];
				} else {
					year = "20" + dateArr[2];
				}
				startDate = DateBuilder.dateOf(Integer.parseInt(timeArr[0]), Integer.parseInt(timeArr[1]), Integer.parseInt(timeArr[2]), Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(year));
			}
			System.out.println(startDate);
			switch(interval) {
			case "seconds":			
				trigger = TriggerBuilder.newTrigger().startAt(startDate).withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(seconds).repeatForever()).build();
				break;
			case "minutes":				
				trigger = TriggerBuilder.newTrigger().startAt(startDate).withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(seconds).repeatForever()).build();
				break;
			case "hours":				
				trigger = TriggerBuilder.newTrigger().startAt(startDate).withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			case "days":			
				seconds = seconds*24;
				trigger = TriggerBuilder.newTrigger().startAt(startDate).withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			case "months":				
				seconds = seconds*24*30;
				trigger = TriggerBuilder.newTrigger().startAt(startDate).withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			}
			
			Queries.updateFormulaStatus(id_formula, "SCHEDULED");
			
			final SchedulerFactory schFactory = (SchedulerFactory)new StdSchedulerFactory();
			Scheduler sch = null;
			try {
				sch = schFactory.getScheduler();
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
			try {
				sch.start();
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
			try {
				sch.scheduleJob(job, trigger);			
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}
	
	private JobBean fetchJob(Integer id) {
		Integer id_job = Queries.getIdJob(id);
		JobBean fetchedJob = Queries.getJobInfo(id_job);
		return fetchedJob;
	}

}
