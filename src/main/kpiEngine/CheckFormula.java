package main.kpiEngine;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.DBConnectionEngine;
import main.kpiEngine.service.MySqlConnection;

/**
 * Servlet implementation class CheckName
 */
@WebServlet("/checkFormula")
public class CheckFormula extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public CheckFormula() {
        super();
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String formula = request.getParameter("name");
		String valid = null;
		
		System.out.println("INSERED NAME: " + formula);
		
		if(formula.equalsIgnoreCase("")) {
			valid = "-1";
		}
		
		Connection connection;
         Statement st=null;
         ResultSet rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
 			st = connection.createStatement();
 			String query = "SELECT EXISTS(SELECT * from formula WHERE name=\""+formula+"\") as valid";
            st = connection.createStatement();
            rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    valid = rs.getString("valid");
                    System.out.println("IS VALID: " + valid);
                }
            }
            connection.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
         
       
         response.getWriter().write(valid);
	}

}
