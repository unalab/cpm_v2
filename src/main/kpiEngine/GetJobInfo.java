package main.kpiEngine;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import main.kpiEngine.service.JobBean;
import main.kpiEngine.service.Queries;

@WebServlet("/getJob")
public class GetJobInfo extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		Integer id_formula = Integer.parseInt(request.getParameter("id"));
		Integer id_job = Queries.getIdJob(id_formula);
		JobBean job = Queries.getJobInfo(id_job);
		
		JSONObject jsonJob = new JSONObject();
		jsonJob.put("id", id_job);
		jsonJob.put("start", job.getStart());
		jsonJob.put("interval", job.getInterval());
		jsonJob.put("date", job.getDate());
		jsonJob.put("time", job.getTime());
		jsonJob.put("num", job.getNum());
		
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(jsonJob.toJSONString());
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

}
