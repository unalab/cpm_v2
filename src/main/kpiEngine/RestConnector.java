package main.kpiEngine;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.jayway.jsonpath.JsonPath;

import main.utils.RestUtils;

@WebServlet("/restConnect")
public class RestConnector extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RestConnector() {
		super();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String url = request.getParameter("url");
		String query = request.getParameter("query");
		String method = request.getParameter("method");
		String body = request.getParameter("body");
		String headers = request.getParameter("headers");
		
		Object parsed = null;
		JSONObject jsonObj = null;
		JSONArray jsonArr = null;
		String resp = "";
		JSONParser parser = new JSONParser();
		Map<String, String> headerMap = new HashMap<String,String>();
		
		if(headers != null) {
			JSONObject headerObj = null;
			try {
				headerObj = (JSONObject) parser.parse(headers);
				Set<String> headerSet = headerObj.keySet();
				Iterator<String> iterator = headerSet.iterator();
				while (iterator.hasNext()) {
					String key = iterator.next();
					String value = (String) headerObj.get(key);
				    headerMap.put(key, value);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if("POST".equalsIgnoreCase(method)) {
			try {
				resp = RestUtils.consumePost(url, body, headerMap);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		} else if ("GET".equalsIgnoreCase(method)) {
			try {
				resp = RestUtils.consumeGet(url, headerMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}			
		
		if(query != null && query != "") {
			parsed = JsonPath.read(resp, query);
		}
		
		response.setCharacterEncoding("UTF-8");
		
		try {
			if(parsed != null) {
				jsonObj = (JSONObject) parser.parse(parsed.toString());
			} else {
				jsonObj = (JSONObject) parser.parse(resp);
			}
		} catch (ParseException | ClassCastException e) {
			try {
				if(parsed != null) {
					jsonArr = (JSONArray) parser.parse(parsed.toString());
				} else {
					jsonArr = (JSONArray) parser.parse(resp);
				}
			} catch (ParseException d) {
				response.getWriter().write("Response is not a json");
			}
		}
		
		if(jsonObj != null) {
			response.getWriter().write(jsonObj.toJSONString());
		} else if(jsonArr != null) {
			response.getWriter().write(jsonArr.toJSONString());
		}
		
	}

}
