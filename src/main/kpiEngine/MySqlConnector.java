package main.kpiEngine;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;

import main.kpiEngine.service.MySqlConnection;

@WebServlet("/sqlConnect")
public class MySqlConnector extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MySqlConnector() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		// TODO Auto-generated method stub
		String url = request.getParameter("url");
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String query = request.getParameter("query");
	
		
		Connection connection = null;
				
		
		System.out.println("url: " + url);
         
         Statement st=null;
         ResultSet rs = null;
         ResultSetMetaData rsMetaData = null;
         int numColonne = 0;
         JSONArray results = new JSONArray();
         String jsonString = null;
         
         
		try {
			connection = MySqlConnection.createConnection(url, username, password);
			st = connection.createStatement();
			rs = st.executeQuery(query);
			rsMetaData = rs.getMetaData();
			numColonne = rsMetaData.getColumnCount();
			
				
		} catch (SQLException e) {
			
				e.printStackTrace();
				response.setStatus(500);
				response.getWriter().write(e.getMessage());
			
			
		}
		
		
		if (numColonne == 1) {	
		
			if (rs != null) {
				try {
					while (rs.next()) {
						JSONObject obj = new JSONObject();
//						for (int i = 1; i<= numColonne; i++) {
						
							String type = rsMetaData.getColumnTypeName(1);
							String columnName = rsMetaData.getColumnName(1);
						
							if(type.equalsIgnoreCase("boolean") || type.equalsIgnoreCase("date") || type.equalsIgnoreCase("varchar") || type.equalsIgnoreCase("timestamp")) {
								
								jsonString = "ERROR: query result must be numeric";
								
							}
							
							else {
								if (type.equalsIgnoreCase("double")) {
									obj.put(columnName, rs.getDouble(columnName));
								}
								else if (type.equalsIgnoreCase("float")) {
									obj.put(columnName, rs.getFloat(columnName));
								}
								else if (type.equalsIgnoreCase("integer")) {
									obj.put(columnName, rs.getInt(columnName));
								}
								results.add(obj);
								
								jsonString = new Gson().toJson(results);
								
							}
						}
						
					connection.close();
					
					//}
					
				} catch (SQLException e) {			
					e.printStackTrace();
					response.setStatus(500);
					response.getWriter().write(e.getMessage());
				
			}
            
            
				
        
			}
		}
		
		else {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jsonString = "ERROR: The query result must have only one column";
		}
			
		System.out.println(jsonString);		 
		response.setCharacterEncoding("UTF-8");
		
		response.getWriter().write(jsonString);
			
		
		

	}

}
