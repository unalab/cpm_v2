package main.kpiEngine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

/**
 * Servlet implementation class showDB
 */
@WebServlet("/showMeas")
public class showMeas extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public showMeas() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getParameter("url");
		String database = request.getParameter("database");
		System.out.println(url);
		System.out.println(database);
		InfluxDB influx = InfluxDBFactory.connect(url);
		QueryResult queryResult = influx.query(new Query("SHOW MEASUREMENTS", database));
		List<Result> res = queryResult.getResults();
		ArrayList<String> mis = new ArrayList<String>();
		//System.out.println(res.get(0).getSeries().get(0).getValues().get(0).get(1).toString());
		for (int i = 0; i<res.get(0).getSeries().get(0).getValues().size(); i++) {
			String value = res.get(0).getSeries().get(0).getValues().get(i).get(0).toString();
			mis.add(value);
	
		}
		influx.close();
		Gson gson = new Gson();
		String jsonString = gson.toJson(mis);
		response.getWriter().write(jsonString);
	}

	

}
