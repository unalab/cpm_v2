package main.kpiEngine;

import java.io.IOException;

import java.util.Objects;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.StdSchedulerFactory;

import main.kpiEngine.service.Queries;

@WebServlet("/interrupt")
public class interruptJob extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id_formula = Integer.parseInt(request.getParameter("id_formula"));
		System.err.println("------- Shutting Down --------");
		
        Integer id_job = Queries.getIdJob(id_formula);
       
        
        String jobName = Queries.getJobName(id_job);
        JobKey jobKey = null;
        Scheduler scheduler = null;
		try {
			scheduler = new StdSchedulerFactory().getScheduler();
		} catch (SchedulerException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		};
		try {
			for (JobExecutionContext runningJob : scheduler.getCurrentlyExecutingJobs()) {
			    if (Objects.equals(jobName, runningJob.getJobDetail().getKey().getName())) {
			        jobKey = runningJob.getJobDetail().getKey();
			    }
			}
		} catch (SchedulerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        System.err.println(jobKey);
        
        try {
			scheduler.interrupt(jobKey);
			scheduler.deleteJob(jobKey);
			scheduler.shutdown();
			
			Queries.updateFormulaStatus(id_formula, "STOPPED");
			Queries.updateJob(Integer.toString(id_job), null);
		} catch (UnableToInterruptJobException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
       
        System.err.println("------- Shutting Down ");

        try {
			scheduler.shutdown(false);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.err.println("------- Shutdown Complete ");
	

}
}
