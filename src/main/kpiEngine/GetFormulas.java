package main.kpiEngine;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import main.bean.FormulaBean;
import main.kpiEngine.service.Queries;

@WebServlet({"/getFormulas"})
public class GetFormulas extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ArrayList<FormulaBean> formulaList = new ArrayList<FormulaBean>();
		formulaList = Queries.getSavedFormulas();
		
		String jsonString = new Gson().toJson(formulaList);   		
		System.out.println("GETFORMULAS: " + jsonString);	 
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonString);
	}

}
