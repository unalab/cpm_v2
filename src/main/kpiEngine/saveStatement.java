package main.kpiEngine;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.DBConnectionEngine;
import main.kpiEngine.service.MySqlConnection;

/**
 * Servlet implementation class saveStatement
 */
@WebServlet("/save")
public class saveStatement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public saveStatement() {
        super();
        
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = request.getParameter("url");
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String query = request.getParameter("query");
		String type = request.getParameter("value");
		String measure = request.getParameter("measure");
		String city = request.getParameter("city");
		String dataType = request.getParameter("case");
		String method = request.getParameter("method");
		String body = request.getParameter("body");
		String headers = request.getParameter("headers");
		
		System.out.println("DATA TYPE: " + dataType);
		if("restConnect".equalsIgnoreCase(dataType)) {
			query = "{\"jsonPath\":\"" + query + "\",\"method\":\"" + method + "\",\"body\":\"" + body + "\"}";
		}
	
		
//		String storageURL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String storageUser ="root";
//		String storagePass = "mysql";
		String storageQuery = "INSERT INTO statement (type, url, username, password, query, measureName, city, headers) VALUE ('"+type+"', '"+url+"', '"+username+"', '"+password+"', '"+query+"', '"+measure+"' , '"+city+"' , '"+headers+"')";
		System.out.println(storageQuery);
		Connection connection;
				
         
         Statement st=null;
         
         try {
 			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
        	 connection = DBConnectionEngine.createConnection();
 			st = connection.createStatement();
 			st.executeUpdate(storageQuery);
 			connection.close();
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
         
       
	}

}
