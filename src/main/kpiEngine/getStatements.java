package main.kpiEngine;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.google.gson.Gson;

import main.kpiEngine.service.Queries;

@WebServlet("/getStatements")
public class getStatements extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public getStatements() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String city = request.getParameter("city");
		
		ArrayList<String> statements = new ArrayList();
		statements = Queries.getStatsName(city);
		Gson gson = new Gson();
		String jsonString = gson.toJson(statements);
		response.getWriter().write(jsonString);
	}

}
