package main.tools;

import com.sun.jersey.api.client.ClientResponse;
import javax.ws.rs.core.MediaType;
import java.util.Calendar;
import org.json.JSONArray;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import main.bean.CBEntity;
import org.apache.http.client.utils.URLEncodedUtils;
import java.nio.charset.Charset;
import org.apache.http.message.BasicNameValuePair;
import java.util.Set;
import java.util.Map;
import main.utils.RestUtils;
import java.util.HashMap;
import java.text.ParseException;
import java.util.Date;
import main.java.PropertyManager;
import main.java.manualKPIProperties;
import main.kpiEngine.service.ContextBroker;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

public class Orion extends ContextBroker
{
    private static final Logger LOGGER;
    private static final SimpleDateFormat sdf;
    private String path_entities;
    private String path_subscribe;

    static {
        LOGGER = Logger.getLogger(Orion.class.getName());
        sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.00'Z'");
    }

    public Orion() throws Exception {
        super(String.valueOf(PropertyManager.getProperty(manualKPIProperties.OrionProtocol)) + PropertyManager.getProperty(manualKPIProperties.OrionHost));
        this.path_entities = PropertyManager.getProperty(manualKPIProperties.OrionEnt);
        this.path_subscribe = PropertyManager.getProperty(manualKPIProperties.OrionSub);
    }

    public String getPathEntities() {
        return this.path_entities;
    }

    public String getPathSubscribe() {
        return this.path_subscribe;
    }

    public static String dateFormat(final Date date) {
        return Orion.sdf.format(date);
    }

    public static Date dateParse(final String date) throws ParseException {
        return Orion.sdf.parse(date);
    }

    public String getEntity(final String fiwareservice, final String fiwareservicepath, final String entityid) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        String resp = "";
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "/" + entityid;
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        try {
            resp = RestUtils.consumeGet(url, (Map)headers);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    public String getEntities(final String fiwareservice, final String fiwareservicepath) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        String resp = "";
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "?limit=1000";
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        try {
            resp = RestUtils.consumeGet(url, (Map)headers);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    public String getEntities(final String fiwareservice, final String fiwareservicepath, final Set<BasicNameValuePair> queryParams) {
        final BasicNameValuePair limitParam = new BasicNameValuePair("limit", "1000");
        queryParams.add(limitParam);
        final String queryString = "?" + URLEncodedUtils.format((Iterable)queryParams, Charset.defaultCharset());
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        String resp = "";
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + queryString;
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        try {
            resp = RestUtils.consumeGet(url, (Map)headers);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    public JSONObject postEntities(final String fiwareservice, final String fiwareservicepath, final Set<? extends CBEntity> entities) {
        final JSONObject out = new JSONObject();
        for (final CBEntity entity : entities) {
            final boolean esito = this.postEntity(fiwareservice, fiwareservicepath, entity);
            try {
                out.put(entity.getId(), esito);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return out;
    }

    private boolean postEntity(final String fiwareservice, final String fiwareservicepath, final CBEntity entity) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities;
        final Map<String, String> configHeaders = new HashMap<String, String>();
        configHeaders.put("fiware-service", fiwareservice);
        configHeaders.put("fiware-servicepath", servicepath);
        boolean out = true;
        try {
            RestUtils.consumePost(url, (Object)this.getGson().toJson((Object)entity), (Map)configHeaders);
        }
        catch (Exception e) {
            e.printStackTrace();
            out = false;
        }
        return out;
    }

    public JSONObject postEntities(final String fiwareservice, final String fiwareservicepath, final JSONArray entities) {
        final JSONObject out = new JSONObject();
        for (int i = 0; i < entities.length(); ++i) {
            JSONObject j = null;
            try {
                j = entities.getJSONObject(i);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            final boolean esito = this.postEntity(fiwareservice, fiwareservicepath, j);
            try {
                out.put(j.getString("id"), esito);
            }
            catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return out;
    }

    public boolean postEntity(final String fiwareservice, final String fiwareservicepath, final JSONObject entity) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities;
        final Map<String, String> configHeaders = new HashMap<String, String>();
        configHeaders.put("fiware-service", fiwareservice);
        configHeaders.put("fiware-servicepath", servicepath);
        boolean out = true;
        try {
            RestUtils.consumePost(url, (Object)entity.toString(), (Map)configHeaders);
        }
        catch (Exception e) {
            e.printStackTrace();
            out = false;
        }
        return out;
    }

    public boolean updateEntityAttribute(final String fiwareservice, final String fiwareservicepath, final String entityid, final String attr, final Object newval) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "/" + entityid + "/attrs";
        final Map<String, String> configHeaders = new HashMap<String, String>();
        configHeaders.put("fiware-service", fiwareservice);
        configHeaders.put("fiware-servicepath", servicepath);
        boolean out = true;
        try {
            final JSONObject body = new JSONObject();
            final JSONObject val = new JSONObject();
            val.put("value", (Object)newval.toString());
            body.put(attr, (Object)val);
            final JSONObject dateModified = new JSONObject();
            dateModified.put("value", (Object)dateFormat(Calendar.getInstance().getTime()));
            body.put("dateModified", (Object)dateModified);
            final MediaType mediatype = MediaType.APPLICATION_JSON_TYPE;
            RestUtils.consumePost(url, (Object)body, mediatype, (Map)configHeaders);
        }
        catch (Exception e) {
            e.printStackTrace();
            out = false;
        }
        return out;
    }

    public boolean updateEntityAttributeMetadata(final String fiwareservice, final String fiwareservicepath, final String entityid, final String attr, final String jsonSchema) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "/" + entityid + "/attrs";
        final Map<String, String> configHeaders = new HashMap<String, String>();
        configHeaders.put("fiware-service", fiwareservice);
        configHeaders.put("fiware-servicepath", servicepath);
        boolean out = true;
        try {
            final JSONObject entity = new JSONObject();
            final JSONObject jsonSchemaObj = new JSONObject();
            JSONObject metavalue = new JSONObject();
            try {
                metavalue = new JSONObject(jsonSchema);
            }
            catch (JSONException je) {
                je.printStackTrace();
            }
            jsonSchemaObj.put("type", (Object)"object");
            jsonSchemaObj.put("value", (Object)metavalue);
            final JSONObject jsonschema = new JSONObject();
            jsonschema.put("jsonschema", (Object)jsonSchemaObj);
            final JSONObject metadata = new JSONObject();
            metadata.put("metadata", (Object)jsonschema);
            entity.put(attr, (Object)metadata);
            final MediaType mediatype = MediaType.APPLICATION_JSON_TYPE;
            RestUtils.consumePost(url, (Object)entity, mediatype, (Map)configHeaders);
        }
        catch (Exception e) {
            e.printStackTrace();
            out = false;
        }
        return out;
    }

    public boolean subscribe(final String fiwareservice, final String fiwareservicepath, final String body) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_subscribe;
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        boolean out = true;
        try {
            RestUtils.consumePost(url, (Object)body, (Map)headers);
        }
        catch (Exception e) {
            e.printStackTrace();
            out = false;
        }
        return out;
    }

    public boolean deleteEntity(final String fiwareservice, final String fiwareservicepath, final String entity_id) throws Exception {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "/" + entity_id;
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        final ClientResponse cr = RestUtils.consumeDelete(url, (Map)headers);
        return cr.getStatus() <= 299;
    }

    public String getEntityAttributes(final String fiwareservice, final String fiwareservicepath, final String entityid) {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        String resp = "";
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "/" + entityid + "/attrs";
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        try {
            resp = RestUtils.consumeGet(url, (Map)headers);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }

    public boolean deleteEntityAttribute(final String fiwareservice, final String fiwareservicepath, final String entityid, final String attributename) throws Exception {
        final String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : ("/" + fiwareservicepath);
        final String url = String.valueOf(this.getBaseUrl()) + this.path_entities + "/" + entityid + "/attrs/" + attributename;
        final Map<String, String> headers = new HashMap<String, String>();
        headers.put("fiware-service", fiwareservice);
        headers.put("fiware-servicepath", servicepath);
        final ClientResponse cr = RestUtils.consumeDelete(url, (Map)headers);
        return true;
    }
}
