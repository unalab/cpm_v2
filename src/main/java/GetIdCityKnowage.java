//package main.java;
//
//import java.io.IOException;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import org.apache.log4j.Logger;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//
//@WebServlet({ "/GetIdCityKnowage" })
//public class GetIdCityKnowage extends HttpServlet
//{
//    private static final long serialVersionUID = 1L;
//    static Logger logger;
//
//    static {
//        GetIdCityKnowage.logger = Logger.getLogger(GetIdCityKnowage.class.getName());
//    }
//
//    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
//        String city = request.getParameter("city");
//        Integer category_id;
//        if (city.matches(".*\\d.*")) {
//            final Integer id = Integer.parseInt(city);
//            city = Queries.getCityName(id);
//            category_id = Queries.getCategoryId(city.toUpperCase(), "KPI_KPI_CATEGORY");
//        }
//        else {
//            category_id = Queries.getCategoryId(city.toUpperCase(), "KPI_KPI_CATEGORY");
//        }
//        final String s = category_id.toString();
//        response.getWriter().write(s);
//    }
//
//    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
//        this.doGet(request, response);
//    }
//}
