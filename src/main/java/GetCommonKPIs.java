package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;

import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetCommonKPIs" })
public class GetCommonKPIs extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public GetCommonKPIs() {
        this.logger = Logger.getLogger(GetCommonKPIs.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        ArrayList<KPIBean> commonKpi = null;
//        ArrayList<KPIBean> intermediate = null;
//        final ArrayList<KPIBean> kpicomputedList = null;
        ArrayList<KPIBean> cityKpi = null;
        final ArrayList<KPIBean> results = new ArrayList<KPIBean>();
        //ArrayList<Integer> ids = null;
        final String city = request.getParameter("city");
        Integer id_city = 0;
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        
        System.out.println("LANGUAGE: " + lang);
        String cityS;
        if (city.matches(".*\\d.*")) {
            id_city = Integer.parseInt(city);
            cityS = Queries.getCityName(id_city);
        }
        else {
            cityS = city;
            id_city = Queries.getCityId(cityS);
        }
        
        System.out.println("  "+id_city);
        commonKpi = (ArrayList<KPIBean>)Queries.getCommonKPI("task", "all", id_city, "manual");
//        final Integer category_city = Queries.getCategoryId(cityS.toUpperCase(), "KPI_KPI_CATEGORY");
//        intermediate = (ArrayList<KPIBean>)Queries.getComputedKPI("task", "all", id_city);
//        ids = (ArrayList<Integer>)Queries.getIds((ArrayList)intermediate, category_city);
        //cityKpi = (ArrayList<KPIBean>)Queries.getKPI("task", "all", id_city, "citymanual");
        results.addAll(commonKpi);
//        if (cityKpi != null) {
//            results.addAll(cityKpi);
//        }
        request.setAttribute("kpiList", (Object)results);
        final String nextJsp = "commonKPI.jsp?city=" + id_city + "&lang=" + lang;
        request.getRequestDispatcher(nextJsp).forward((ServletRequest)request, (ServletResponse)response);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
