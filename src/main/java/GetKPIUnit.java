package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetKPIUnit" })
public class GetKPIUnit extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String idKpi = request.getParameter("idKpi");
        
        String unit = Queries.getUnit(Integer.parseInt(idKpi));
        
        response.getWriter().write(unit);
    }
}
