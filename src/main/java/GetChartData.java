package main.java;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetChartData" })
public class GetChartData extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
       String city = request.getParameter("city");
       String id = request.getParameter("idKpi");
       String type =  request.getParameter("type");
       System.out.println("CITYYY: " +city);
       String cityString = Queries.getCityName(Integer.parseInt(city));
        String database = "";
       switch(type) {
       case "manual":
    	   database = cityString.toLowerCase()+"_CommonKPI"+id;
    	   break;
       case "citymanual":
    	   database = cityString.toLowerCase()+"_ManualKPI"+id;
    	   break;
       case "computed":
    	   Integer idEngine = Queries.getIdEngine(id);
    	   database = cityString.toLowerCase()+"_KPI"+idEngine;
    	   break;
       }
       JSONArray results = new JSONArray();
       try {
		results = Queries.getInfluxValues(database,id);
		
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//        if (idcity.matches(".*\\d.*")) {
//            id = Integer.parseInt(idcity);
//            city = Queries.getCityName(id);
//        }
//        else {
//            city = idcity;
//        }
 catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
       
       
       String jsonString = new Gson().toJson(results);
       jsonString = jsonString.replace("{\"myArrayList\":", "");
       jsonString = jsonString.replace("{\"map\":", "");
       jsonString = jsonString.replace("}}", "}");
       jsonString = jsonString.replace("}}]}", "}]");
       jsonString = jsonString.replace("]}", "]");
       
        System.out.println(jsonString);
        response.getWriter().write(jsonString);
    }
    
    public static JSONArray getInfluxValues(String database, String id) throws JSONException, ParseException{


    	JSONArray values = new JSONArray();
    	System.out.println("DB: "+database);
    	String measurement="KPI_Values";
    	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
    	InfluxDB influx = InfluxDBFactory.connect(url);
    	Query q = new Query ("SELECT * from " + measurement,database);
    	QueryResult queryResult = influx.query(q);
    	System.out.println(queryResult);	
    	List<Result> res = queryResult.getResults();
    	for(int i = 0; i<res.get(0).getSeries().get(0).getValues().size(); i++) {
    		JSONObject j =  new JSONObject();
    		Double value = (Double) res.get(0).getSeries().get(0).getValues().get(i).get(3);
    		
    		String time = (String) res.get(0).getSeries().get(0).getValues().get(i).get(0);
    		SimpleDateFormat dt = new SimpleDateFormat("yyyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); 
    		Date date = dt.parse(time); 
    		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    		
    		String motivation = (String) res.get(0).getSeries().get(0).getValues().get(i).get(1);
    		String unit = Queries.getUnit(Integer.parseInt(id));
    		j.put("x", dt1.format(date));
    		j.put("y", value);
    		j.put("z", motivation);
    		j.put("a", unit);
    		values.put(j);
    		
    	}


    	influx.close();
    	return values;

    	}

    
    
}
