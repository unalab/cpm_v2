//package main.java;
//
//import java.io.IOException;
//import javax.servlet.ServletException;
//import org.json.JSONObject;
//import java.util.ArrayList;
//import com.google.gson.Gson;
//import java.util.List;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import org.apache.log4j.Logger;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//
//@WebServlet({ "/GetCalculatedIndicators" })
//public class GetCalculatedIndicators extends HttpServlet
//{
//    private static final long serialVersionUID = 1L;
//    static Logger logger;
//
//    static {
//        GetCalculatedIndicators.logger = Logger.getLogger(GetCalculatedIndicators.class.getName());
//    }
//
//    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
//        final String city_id = request.getParameter("city");
//        Integer category_id;
//        if (city_id.matches(".*\\d.*")) {
//            final Integer id = Integer.parseInt(city_id);
//            final String city = Queries.getCityName(id);
//            category_id = Queries.getCategoryId(city.toUpperCase(), "KPI_KPI_CATEGORY");
//        }
//        else {
//            category_id = Queries.getCategoryId(city_id.toUpperCase(), "KPI_KPI_CATEGORY");
//        }
//        final List<Integer> idInKnowage = (List<Integer>)Queries.getComputedIds(category_id);
//        for (int i = 0; i < idInKnowage.size(); ++i) {
//            GetCalculatedIndicators.logger.debug((Object)("Id in KNOWAGE: " + idInKnowage.get(i)));
//        }
//        final List<Integer> presenti = (List<Integer>)Queries.verifyId((List)idInKnowage);
//        final Gson gson = new Gson();
//        List<JSONObject> filters = new ArrayList<JSONObject>();
//        if (presenti.size() > 0) {
//            filters = (List<JSONObject>)Queries.getComputedIndicators((List)presenti);
//            GetCalculatedIndicators.logger.debug((Object)("FILTERS: " + filters));
//        }
//        String jsonString;
//        if (filters.isEmpty()) {
//            jsonString = null;
//        }
//        else {
//            jsonString = gson.toJson((Object)filters);
//            GetCalculatedIndicators.logger.debug((Object)("JSON STRING: " + jsonString));
//            jsonString = jsonString.replace("{\"map\":", "");
//            jsonString = jsonString.replace("}}", "}");
//            jsonString = jsonString.replace(",{}", "");
//            jsonString = jsonString.replace("{},", "");
//            GetCalculatedIndicators.logger.debug((Object)("INDICATORS TABLE: " + jsonString));
//        }
//        response.getWriter().write(jsonString);
//    }
//
//    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
//        this.doGet(request, response);
//    }
//}
