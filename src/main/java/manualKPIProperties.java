package main.java;

public enum manualKPIProperties
{
    DB_HOST("DB_HOST", 0, "DB_HOST"),
    DB_HOST_CALCULATED("DB_HOST_CALCULATED", 1, "DB_HOST_CALCULATED"),
    DB_NAME("DB_NAME", 2, "DB_NAME"),
    DB_NAME_CALCULATED("DB_NAME_CALCULATED", 3, "DB_NAME_CALCULATED"),
    DB_USERNAME("DB_USERNAME", 4, "DB_USERNAME"),
    DB_PASSWORD("DB_PASSWORD", 5, "DB_PASSWORD"),
    OrionProtocol("OrionProtocol", 6, "orion.protocol"),
    OrionHost("OrionHost", 7, "orion.host"),
    OrionSub("OrionSub", 8, "orion.v2.subscriptions"),
    OrionTrust("OrionTrust", 9, "orion.iot.trust"),
    OrionService("OrionService", 10, "orion.headers.service"),
    OrionServicePath("OrionServicePath", 11, "orion.headers.servicepath"),
    OrionEnt("OrionEnt", 12, "orion.v2.entities"),
    OrionContext("OrionContext", 13, "orion.v1.updateContext"),
    OrionGetType("OrionGetType", 14, "orion.get.content.type"),
    OrionDeleteType("OrionDeleteType", 15, "orion.delete.content.type"),
    MapZoom("MapZoom", 16, "orion.mapcenter.zoom"),
    LimitSearch("LimitSearch", 17, "orion.limit.search"),
    DmeProtocol("DmeProtocol", 18, "dme.protocol"),
    DmeHost("DmeHost", 19, "dme.host"),
    DmeMashupGenova("DmeMashupGenova", 20, "dme.invoke.mashup.genova"),
    DmeMashupEindhoven("DmeMashupEindhoven", 21, "dme.invoke.mashup.eindhoven"),
    DmeMashupTampere("DmeMashupTampere", 22, "dme.invoke.mashup.tampere"),
    JsonDir("JsonDir", 23, "json.directory"),
	CKAN_BASE_URL("CkanBaseUrl",24,"ckan.baseurl"),
	CKAN_BASE_URL_DATASTORE("CkanBaseUrlDatasore", 25, "ckan.baseurl.datastore"),
	CKAN_DEFAULT_USER("CkanDefaultUser",26,"ckan.default.user"),
	CKAN_DEFAULT_USER_APIKEY("CkanDefaultUserApiKey",27,"ckan.default.user.apikey"),
	CKAN_SECONDS("CkanJobSeconds",28,"ckan.job.seconds"),
    DB_KPIENGINE_NAME("kpiEngine_name",29,"kpiEngine_name"),
	DB_KPIENGINE_HOST("DB_KPIENGINE_HOST",30,"DB_KPIENGINE_HOST"),
	INFLUX_DB("INFLUX_DB",31,"INFLUX_DB");
    
    
    
    private final String text;

    private manualKPIProperties(final String name, final int ordinal, final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
