package main.java;

import java.io.IOException;
import javax.servlet.ServletException;

import main.kpiEngine.service.InfluxManager;

import main.utils.SubscriptionManager;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/EditKPI" })
public class EditKPI extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public EditKPI() {
        this.logger = Logger.getLogger(EditKPI.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String tableId = request.getParameter("tableId");
        String nameKPI = request.getParameter("title");
        final String newValueP = request.getParameter("value");
        final String motivation = request.getParameter("motivation");
        final String idCityP = request.getParameter("city");
        final String idKPIP = request.getParameter("KPI_id");
        final String level = request.getParameter("level");
        final String category = request.getParameter("category");
//        InvokeMashup mashup = null;
//        GenerateData ckan = new GenerateData();
//		try {
//			mashup = new InvokeMashup();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
        this.logger.debug((Object)category);
        final float newValue = Float.parseFloat(newValueP);
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        Integer idCity;
        String cityname;
        if (idCityP.matches(".*\\d.*")) {
            idCity = Integer.parseInt(idCityP);
            cityname = Queries.getCityName(idCity);
        }
        else {
            cityname = idCityP;
            idCity = Queries.getCityId(cityname);
        }
        final Integer idKPI = Integer.parseInt(idKPIP);
        //nameKPI = nameKPI.replaceAll("\\s+", "");
        //nameKPI = nameKPI.toLowerCase();
        String fiware_service = null;
        String fiware_servicepath = null;
        fiware_service = cityname.toLowerCase();
        fiware_servicepath = "/" + cityname.toLowerCase() + "_kpi/harmonized";
        final String idEntity = idKPIP+"_manual_v2";
        System.out.println("idEntity= " + idEntity);
        String influxDb;
        if(level.equalsIgnoreCase("task")) {
        	influxDb = cityname.toLowerCase()+"_CommonKPI"+idKPIP;
        }
        else {
        	influxDb = cityname.toLowerCase()+"_ManualKPI"+idKPIP;
        }
        try {
			InfluxManager.createInfluxMeasurementManual(motivation,influxDb, nameKPI, (double) newValue, "KPI_Values");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //Queries.editKPIValue(idCity, idKPI, newValue, motivation);
        SubscriptionManager.updateEntity(idEntity, newValueP, fiware_service, fiware_servicepath);
//        try {
//			//mashup.invokeDme(idCity);
//			ckan.invokeCkan(cityname);
//			
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        final String nextJSP = "/cpm_v2/view.jsp?city=" + idCity + "&level=" + level + "&category=" + category + "&lang=" + lang;
        response.sendRedirect(nextJSP);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
