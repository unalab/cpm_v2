package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/ModifyAdminKPI" })
public class ModifyAdminKPI extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public ModifyAdminKPI() {
        this.logger = Logger.getLogger(ModifyAdminKPI.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String name = request.getParameter("name");
        String isManual = request.getParameter("setComp");
        final String desc = request.getParameter("desc");
        final String unit = request.getParameter("unit");
        final String idCityP = request.getParameter("city");
        final String level = request.getParameter("level");
        final String category = request.getParameter("category");
        final String KPI_idP = request.getParameter("KPI_id");
        
        String nbs = request.getParameter("nbs");
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        Integer idCity;
        if (idCityP.matches(".*\\d.*")) {
            idCity = Integer.parseInt(idCityP);
        }
        else {
            idCity = Queries.getCityId(idCityP);
        }
        final Integer KPI_id = Integer.parseInt(KPI_idP);
        
        
            
        isManual = "manual";
        
        
        Queries.updateKPI(KPI_id, name, desc, unit, isManual, Integer.valueOf(0), nbs);
        final String nextJSP = "/cpm_v2/GetAdminKPIList?city=" + idCity + "&level=task&category=all&lang=" + lang;
        response.sendRedirect(nextJSP);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
