package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import java.util.Collection;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetAdminKPIList" })
public class GetAdminKPIList extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public GetAdminKPIList() {
        this.logger = Logger.getLogger(GetAdminKPIList.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        ArrayList<KPIBean> kpiList = null;
        final ArrayList<KPIBean> results = new ArrayList<KPIBean>();
        String level = request.getParameter("radio1");
        this.logger.debug((Object)level);
        final String city = request.getParameter("city");
        this.logger.debug((Object)("selected pilot: " + city));
        final Integer id_city = Integer.parseInt(city);
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        String category;
        if (level == null) {
            level = request.getParameter("level");
            category = request.getParameter("category");
            this.logger.debug((Object)("category post edit:" + category));
        }
        else if (level.equalsIgnoreCase("task")) {
            category = request.getParameter("two");
            this.logger.debug((Object)("category post edit:" + category));
        }
        else {
            category = request.getParameter("third");
            this.logger.debug((Object)("category post edit:" + category));
        }
        kpiList = (ArrayList<KPIBean>)Queries.getCommonKPI(level, category, 0, "manual");
        results.addAll(kpiList);
        request.setAttribute("kpiList", (Object)results);
        final String nextJsp = "admin.jsp?city=3&level=" + level + "&category=" + category + "&lang=" + lang;
        this.logger.debug((Object)("nextJSP: " + nextJsp));
        request.getRequestDispatcher(nextJsp).forward((ServletRequest)request, (ServletResponse)response);
//        
//        String jsonString = new Gson().toJson(results);
//   		
//		 System.out.println(jsonString);		 
//		 response.setCharacterEncoding("UTF-8");
//		 response.getWriter().write(jsonString);
  
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
