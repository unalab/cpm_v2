package main.java;

import java.io.IOException;
import java.util.Properties;

public class PropertyManager
{
    private static Properties props;

    static {
        PropertyManager.props = null;
        PropertyManager.props = new Properties();
        try {
            PropertyManager.props.load(PropertyManager.class.getClassLoader().getResourceAsStream("config.properties"));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(final manualKPIProperties propName) {
        return PropertyManager.props.getProperty(propName.toString());
    }
}
