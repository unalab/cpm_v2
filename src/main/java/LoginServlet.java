package main.java;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import java.util.ArrayList;
import main.bean.LoginBean;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/LoginServlet" })
public class LoginServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public LoginServlet() {
        this.logger = Logger.getLogger(LoginServlet.class.getName());
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String userName = request.getParameter("username");
        this.logger.debug((Object)("EMAIL: " + userName));
        final String password = request.getParameter("password");
        final LoginBean loginBean = new LoginBean();
        final String level = "task";
        final String category = "all";
        loginBean.setMail(userName);
        loginBean.setPassword(password);
        ArrayList<KPIBean> kpiInitialList = new ArrayList<KPIBean>();
        final ArrayList<KPIBean> kpicomputedList = null;
        final ArrayList<KPIBean> intermediate = null;
        final ArrayList<KPIBean> results = new ArrayList<KPIBean>();
        final ArrayList<KPIBean> cityKpi = null;
        final ArrayList<Integer> ids = null;
        String nextJsp = "";
        final String s = "city_manager";
        String[] extracted_city = null;
        String city = "";
        final Integer category_city = 0;
        try {
            final String userValidate = Queries.authenticateUser(loginBean);
            if (userValidate.equals("admin")) {
            	System.out.println("user: " + userValidate);
                final HttpSession session = request.getSession();
                session.setMaxInactiveInterval(300);
                session.setAttribute("user", (Object)userValidate);
               kpiInitialList = (ArrayList<KPIBean>)Queries.getCommonKPI(level, category, Integer.valueOf(3), "manual");
                nextJsp = "/GetAdminKPIList?city=3&level=" + level + "&category=" + category+"&lang=en";
               request.setAttribute("kpiList", (Object)kpiInitialList);
                response.sendRedirect(String.valueOf(request.getContextPath()) + nextJsp);
                
                //String jsonString = new Gson().toJson(kpiInitialList);
                
                //jsonString = jsonString.replace("[{", "{\"redirect\":\"admin.jsp\", [{");
                //jsonString = jsonString.replace("}]", "}]}");
               
        		 //System.out.println(jsonString);		 
        		 //response.setCharacterEncoding("UTF-8");
        		 //response.getWriter().write(jsonString);
            }
            else if (userValidate.toLowerCase().contains(s.toLowerCase())) {
                System.out.println("City Manager");
                extracted_city = userValidate.split("_");
                city = extracted_city[0].toLowerCase();
                final Integer city_id = Queries.getCityId(city);
                final HttpSession session = request.getSession();
                session.setMaxInactiveInterval(1200);
                session.setAttribute("user", (Object)userValidate);
                kpiInitialList = (ArrayList<KPIBean>)Queries.getCommonKPI(level, category, city_id, "manual");
                results.addAll(kpiInitialList);
                nextJsp = "/view.jsp?city=" + city_id + "&level=task&category=all&lang=en";
                request.setAttribute("kpiList", (Object)results);
                response.sendRedirect(String.valueOf(request.getContextPath()) + nextJsp);
            }
            else {
                System.out.println("Error message = " + userValidate);
                request.setAttribute("errMessage", (Object)userValidate);
                request.getRequestDispatcher("login.jsp").forward((ServletRequest)request, (ServletResponse)response);
            }
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
