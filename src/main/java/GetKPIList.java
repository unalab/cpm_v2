package main.java;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import java.util.Collection;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetKPIList" })
public class GetKPIList extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public GetKPIList() {
        this.logger = Logger.getLogger(GetKPIList.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        
        final ArrayList<KPIBean> results = new ArrayList<KPIBean>();
        
        ArrayList<KPIBean> commonKPI = null;
        ArrayList<KPIBean> cityKpi = null;
        ArrayList<KPIBean> computedKpi = null;
        String level = request.getParameter("radio2");
        this.logger.debug((Object)level);
        final String city = request.getParameter("city");
        final Integer id_city = Integer.parseInt(city);
        System.out.println("LEVEL " + level);
      
     
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        if (level == null || level == "") {
            level = request.getParameter("level");
//            //category = request.getParameter("category");
       }
        if(level.equalsIgnoreCase("common")) {
        	level = "task";
        }
        String category="all";
        
        
        
//        else if (level.equalsIgnoreCase("task")) {
//            //category = request.getParameter("two");
//        }
//        else {
//            category = request.getParameter("third");
//        }
        System.out.println("LEVEL POST: "+ level);
        //kpiList = (ArrayList<KPIBean>)Queries.getKPI(level, category, id_city, "manual");
        //intermediate = (ArrayList<KPIBean>)Queries.getComputedKPI(level, category, id_city);
        //ids = (ArrayList<Integer>)Queries.getIds((ArrayList)intermediate, category_city);
        //kpicomputedList = (ArrayList<KPIBean>)Queries.getComputedValues((ArrayList)intermediate, (ArrayList)ids);
        //if (kpiList != null) {
            //results.addAll(kpiList);
        //}
//        if (kpicomputedList != null) {
//            results.addAll(kpicomputedList);
//        }
        commonKPI = Queries.getCommonKPI(level, category, id_city, "manual");
        if(commonKPI!=null) {
        	results.addAll(commonKPI);
        }
        cityKpi = (ArrayList<KPIBean>)Queries.getManualKPI(level, category, id_city, "citymanual");
        if (cityKpi != null) {
            results.addAll(cityKpi);
        }
        ArrayList<KPIBean> checkedComputedKPI = new ArrayList<KPIBean>();
        computedKpi = (ArrayList<KPIBean>)Queries.getComputedKPI(level, category, id_city);
        for(int i = 0; i< computedKpi.size(); i++) {
        	KPIBean kpiInflux = new KPIBean();
        	Integer checkId = computedKpi.get(i).getIdKPI();
        	Integer idEngine = Queries.checkInfluxData(checkId);
        	if(idEngine!=0) {
        		double value = Queries.getInfluxKPI(level, category, id_city, idEngine);
        		String ent = Queries.getOrionEntityById(idEngine);
        		System.out.println("SEARCHING KPI VALUE___");
        		kpiInflux.setIdKPI(computedKpi.get(i).getIdKPI());
        		kpiInflux.setValue((float) value);
        		kpiInflux.setDate(computedKpi.get(i).getDate());
        		kpiInflux.setDescription(computedKpi.get(i).getDescription());
        		kpiInflux.setName(computedKpi.get(i).getName());
        		kpiInflux.setUnit(computedKpi.get(i).getUnit());
        		kpiInflux.setIdCity(id_city);
        		kpiInflux.setNbs(computedKpi.get(i).getNbs());
        		kpiInflux.setKpiCategory(computedKpi.get(i).getKpiCategory());
        		kpiInflux.setIdKPI(computedKpi.get(i).getIdKPI());
        		kpiInflux.setTableId(computedKpi.get(i).getTableId());
        		kpiInflux.setIsManual("computed");
        		kpiInflux.setMotivation("last computed value");
        		kpiInflux.setIcon(computedKpi.get(i).getIcon());
        		kpiInflux.setEntity(ent);
        		checkedComputedKPI.add(kpiInflux);
        	}
        	else {
        		
        		
        		
        		kpiInflux.setValue(0);
        		kpiInflux.setDate(computedKpi.get(i).getDate());
        		kpiInflux.setDescription(computedKpi.get(i).getDescription());
        		kpiInflux.setName(computedKpi.get(i).getName());
        		kpiInflux.setUnit(computedKpi.get(i).getUnit());
        		kpiInflux.setIdCity(computedKpi.get(i).getIdCity());
        		kpiInflux.setNbs(computedKpi.get(i).getNbs());
        		kpiInflux.setKpiCategory(computedKpi.get(i).getKpiCategory());
        		kpiInflux.setIdKPI(computedKpi.get(i).getIdKPI());
        		kpiInflux.setTableId(computedKpi.get(i).getTableId());
        		kpiInflux.setIsManual("computed");
        		kpiInflux.setMotivation("computed kpi not already scheduled or stopped");
        		kpiInflux.setIcon(computedKpi.get(i).getIcon());
        		kpiInflux.setEntity("not available");
        		checkedComputedKPI.add(kpiInflux);
        	}
        }
        
        
        System.out.println(checkedComputedKPI);
        if (checkedComputedKPI != null) {
            results.addAll(checkedComputedKPI);
        }
        this.logger.debug((Object)("RESULTS: " + results.size()));
//        request.setAttribute("kpiList", (Object)results);
//        final String nextJsp = "view.jsp?city=" + id_city + "&level=" + level + "&category=" + category + "&lang=" + lang;
//        this.logger.debug((Object)("nextJSP: " + nextJsp));
//        request.getRequestDispatcher(nextJsp).forward((ServletRequest)request, (ServletResponse)response);
        
         String jsonString = new Gson().toJson(results);
   		
		 System.out.println("GETKPILIST: " + jsonString);
		 
		 response.setCharacterEncoding("UTF-8");
		 response.getWriter().write(jsonString);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
