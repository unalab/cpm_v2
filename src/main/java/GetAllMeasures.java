package main.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import main.kpiEngine.service.Queries;

@WebServlet({"/getAllMeasures"})
public class GetAllMeasures extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String city = request.getParameter("city");
		
		Integer city_id = main.java.Queries.getCityId(city);		
		
		List<String> statements = Queries.getStatsName(city);
		List<String> kpiNames = main.java.Queries.getAllKPI(city_id);
		List<String> iotDatabases = main.java.Queries.getInfluxIoTDatabaseNames(city);
		
		JSONObject data = new JSONObject();
		data.put("statements", statements);
		data.put("kpiNames", kpiNames);
		data.put("iotDatabases", iotDatabases);
		
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(data.toJSONString());
	}	
}
