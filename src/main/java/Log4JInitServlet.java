package main.java;

import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import java.io.File;
import javax.servlet.ServletConfig;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet(urlPatterns = { "/Log4jInitServlet" }, initParams = { @WebInitParam(name = "log4j-properties-location", value = "/WEB-INF/log4j.properties") }, loadOnStartup = 1)
public class Log4JInitServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    static Logger logger;

    static {
        Log4JInitServlet.logger = Logger.getLogger(Log4JInitServlet.class.getName());
    }

    public void init(final ServletConfig config) throws ServletException {
        final String log4jLocation = config.getInitParameter("log4j-properties-location");
        final ServletContext sc = config.getServletContext();
        final String webappPath = sc.getRealPath("/");
        final String log4jProp = String.valueOf(webappPath) + log4jLocation;
        final File fileDiPropLog4j = new File(log4jProp);
        if (fileDiPropLog4j.exists()) {
            PropertyConfigurator.configure(log4jProp);
        }
        else {
            BasicConfigurator.configure();
        }
        super.init(config);
    }
}
