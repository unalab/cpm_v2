package main.java;

import main.bean.LoginBean;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import main.bean.City;
import main.kpiEngine.service.InfluxManager;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Statement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.apache.log4j.Logger;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;

public class Queries
{
    static Logger logger;
    

    static {
        Queries.logger = Logger.getLogger(Queries.class.getName());
    }

    
    
    public static double getInfluxKPI(String level, final String category, final Integer id_city, Integer idEngine){
    	
    	
    	String db;
    	String measurement;
    	String city = getCityName(id_city);
    	
		db = city.toLowerCase()+"_KPI"+idEngine.toString();
		System.out.println("DB: " + db);
    	measurement="KPI_Values";
    	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
    	InfluxDB influx = InfluxDBFactory.connect(url);
    	Query q = new Query ("SELECT LAST(value) from " + measurement,db);
		QueryResult queryResult = influx.query(q);
			
		List<Result> res = queryResult.getResults();
		double value = (double) res.get(0).getSeries().get(0).getValues().get(0).get(1);
		influx.close();
		System.out.println("VALUE: " + value);
        return value;
    	
    }
    
public static double getInfluxManualKPI(String level, final String category, final Integer id_city, Integer idKPI){
    	
    	
    	String db;
    	String measurement;
    	String city = getCityName(id_city);
    	
		db = city.toLowerCase()+"_ManualKPI"+idKPI.toString();
		System.out.println("DB: "+db);
    	measurement="KPI_Values";
    	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
    	InfluxDB influx = InfluxDBFactory.connect(url);
    	Query q = new Query ("SELECT LAST(value), motivation from " + measurement,db);
		QueryResult queryResult = influx.query(q);
			
		List<Result> res = queryResult.getResults();
		double value = (double) res.get(0).getSeries().get(0).getValues().get(0).get(1);
		influx.close();
		System.out.println(value);
	
        return value;
    	
    }

public static String getInfluxManualMotivation(String level, final String category, final Integer id_city, Integer idKPI){
	
	
	String db;
	String measurement;
	String city = getCityName(id_city);
	
	db = city.toLowerCase()+"_ManualKPI"+idKPI.toString();
	System.out.println("DB: "+db);
	measurement="KPI_Values";
	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
	InfluxDB influx = InfluxDBFactory.connect(url);
	Query q = new Query ("SELECT LAST(value), motivation from " + measurement,db);
	QueryResult queryResult = influx.query(q);
		
	List<Result> res = queryResult.getResults();
	String value = (String) res.get(0).getSeries().get(0).getValues().get(0).get(2);
	System.out.println(value);
	influx.close();
    return value;
	
}

public static double getInfluxCommonKPI(String level, final String category, final Integer id_city, Integer idKPI){
	
	
	String db;
	String measurement;
	String city = getCityName(id_city);
	
	db = city.toLowerCase()+"_CommonKPI"+idKPI.toString();
	System.out.println("DB: "+db);
	measurement="KPI_Values";
	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
	InfluxDB influx = InfluxDBFactory.connect(url);
	Query q = new Query ("SELECT LAST(value), motivation from " + measurement,db);
	QueryResult queryResult = influx.query(q);
		
	List<Result> res = queryResult.getResults();
	double value = (double) res.get(0).getSeries().get(0).getValues().get(0).get(1);
	influx.close();
	System.out.println(value);

    return value;
	
}

public static String getInfluxCommonMotivation(String level, final String category, final Integer id_city, Integer idKPI){

	String db;
	String measurement;
	String city = getCityName(id_city);
	
	db = city.toLowerCase()+"_CommonKPI"+idKPI.toString();
	System.out.println("DB: "+db);
	measurement="KPI_Values";
	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
	InfluxDB influx = InfluxDBFactory.connect(url);
	Query q = new Query ("SELECT LAST(value), motivation from " + measurement,db);
	QueryResult queryResult = influx.query(q);
		
	List<Result> res = queryResult.getResults();
	String value = (String) res.get(0).getSeries().get(0).getValues().get(0).get(2);
	System.out.println(value);
	influx.close();
	return value;

}

	public static ArrayList<String> getAllKPI(Integer city_id) {
		Connection conn = null;
        ArrayList<String> kpiList = new ArrayList<String>();
        try {
            conn = DBConnection.createConnection();
            String query = "SELECT name from kpi WHERE city="+city_id;
                
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                	String name = rs.getString("name");
                    kpiList.add(name);
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return kpiList;
	}

    public static ArrayList<KPIBean> getKPI(final String level, final String category, final Integer id_city, final String type) {
        Connection conn = null;
        final ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
        
        try {
            conn = DBConnection.createConnection();
            if (!category.equalsIgnoreCase("all")) {
                final String query = "SELECT a.lTS, b.*\nFROM (SELECT idkpi, MAX(date) as lTS\n\tfrom kpilist \n\twhere level='" + level + "' AND category='" + category + "' AND idcity=" + id_city + " AND type='" + type + "'\n" + "\tGROUP BY idkpi) a JOIN kpilist b\n" + "      ON a.idkpi = b.idkpi\n" + "WHERE b.level='" + level + "' AND b.category='" + category + "' AND b.idcity=" + id_city + " AND b.type='" + type + "' AND\n" + "      a.lTS = b.date;";
                final Statement st = conn.createStatement();
                final ResultSet rs = st.executeQuery(query);
                KPIBean kpi = null;
                if (rs != null) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    while (rs.next()) {
                        kpi = new KPIBean();
                        kpi.setName(rs.getString("name"));
                        kpi.setDescription(rs.getString("description"));
                        kpi.setUnit(rs.getString("unit"));
                        kpi.setValue(rs.getFloat("value"));
                        final Timestamp t = rs.getTimestamp("date");
                        kpi.setDate(sdf.format(t));
                        kpi.setIdCity(id_city);
                        kpi.setKpiLevel(level);
                        kpi.setNbs(rs.getString("nbs"));
                        kpi.setKpiCategory(category);
                        kpi.setIdKPI(rs.getInt("idkpi"));
                        kpi.setTableId(rs.getInt("tableid"));
                        kpi.setIsManual(type);
                        kpi.setMotivation(rs.getString("motivation"));
                        kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                        kpiList.add(kpi);
                    }
                }
            }
            else {
                final String query = "SELECT a.lTS, b.*\nFROM (SELECT idkpi, MAX(date) as lTS\n\tfrom kpilist \n\twhere level='" + level + "' AND idcity=" + id_city + " AND type='" + type + "'\n" + "\tGROUP BY idkpi) a JOIN kpilist b\n" + "      ON a.idkpi = b.idkpi\n" + "WHERE b.level='" + level + "' AND b.idcity=" + id_city + " AND type='" + type + "' AND\n" + "      a.lTS = b.date;";
                final Statement st = conn.createStatement();
                final ResultSet rs = st.executeQuery(query);
                KPIBean kpi = null;
                if (rs != null) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    while (rs.next()) {
                        kpi = new KPIBean();
                        kpi.setName(rs.getString("name"));
                        kpi.setDescription(rs.getString("description"));
                        kpi.setUnit(rs.getString("unit"));
                        kpi.setValue(rs.getFloat("value"));
                        final Timestamp t = rs.getTimestamp("date");
                        kpi.setDate(sdf.format(t));
                        kpi.setIdCity(id_city);
                        kpi.setKpiLevel(level);
                        kpi.setKpiCategory(rs.getString("category"));
                        kpi.setNbs(rs.getString("nbs"));
                        kpi.setIdKPI(rs.getInt("idkpi"));
                        kpi.setTableId(rs.getInt("tableid"));
                        kpi.setIsManual(type);
                        kpi.setMotivation(rs.getString("motivation"));
                        kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                        kpiList.add(kpi);
                    }
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return kpiList;
    }
    
    public static ArrayList<KPIBean> getManualKPI(final String level, final String category, final Integer id_city, final String type) {
        Connection conn = null;
        final ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
        String query;
        try {
            conn = DBConnection.createConnection();
            if (!category.equalsIgnoreCase("all")) {
                query = "SELECT * from kpi WHERE level='"+level+"' AND category='"+category+"' AND city="+id_city+" AND type='"+type+"'";
            }
            else {
            	query = "SELECT * from kpi WHERE level='"+level+"' AND city="+id_city+" AND type='"+type+"'";
            }
                
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            KPIBean kpi = null;
                if (rs != null) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    while (rs.next()) {
                        kpi = new KPIBean();
                        kpi.setName(rs.getString("name"));
                        kpi.setDescription(rs.getString("description"));
                        kpi.setUnit(rs.getString("unit"));
                        kpi.setEntity(rs.getString("orionEntity"));
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        kpi.setDate(sdf.format(timestamp));
                        kpi.setIdCity(id_city);
                        kpi.setKpiLevel(level);
                        kpi.setNbs(rs.getString("nbs"));
                        kpi.setKpiCategory(rs.getString("category"));
                        kpi.setIdKPI(rs.getInt("idkpi"));
                        kpi.setTableId(rs.getInt("tableid"));
                        kpi.setIsManual(type);
                        kpi.setEntity(rs.getString("orionEntity"));
                        String motivation = getInfluxManualMotivation(level, category, id_city, rs.getInt("idkpi"));
                        kpi.setMotivation(motivation);
                        kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                        
                        double value = getInfluxManualKPI(level, category, id_city, rs.getInt("idkpi"));
                        kpi.setValue((float) value);
                        kpiList.add(kpi);
                    }
                }

            
            
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return kpiList;
    }
    
    
    public static ArrayList<KPIBean> getCommonKPI(String level, String category, Integer id_city, String type) {
        Connection conn = null;
        final ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
        String query;
        
        try {
            conn = DBConnection.createConnection();
            if (!category.equalsIgnoreCase("all")) {
                query = "SELECT * from kpi WHERE level='"+level+"' AND category='"+category+"' AND city=0 AND type='"+type+"'";
            }
            else {
            	query = "SELECT * from kpi WHERE level='"+level+"' AND city=0 AND type='"+type+"'";
            }
            System.out.println(query);
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            KPIBean kpi = null;
                if (rs != null) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    while (rs.next()) {
                        kpi = new KPIBean();
                        kpi.setName(rs.getString("name"));
                        kpi.setDescription(rs.getString("description"));
                        kpi.setUnit(rs.getString("unit"));
                        kpi.setEntity(rs.getString("orionEntity"));
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        kpi.setDate(sdf.format(timestamp));
                        kpi.setIdCity(id_city);
                        kpi.setKpiLevel(level);
                        kpi.setNbs(rs.getString("nbs"));
                        kpi.setKpiCategory(rs.getString("category"));
                        kpi.setIdKPI(rs.getInt("idkpi"));
                        kpi.setTableId(rs.getInt("tableid"));
                        kpi.setIsManual(type);
                        if(id_city == 0) {
                        	id_city = 3;
                        }
                        String motivation = getInfluxCommonMotivation(level, category, id_city, rs.getInt("idkpi"));
                        kpi.setMotivation(motivation);
                        kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                        
                        double value = getInfluxCommonKPI(level, category, id_city, rs.getInt("idkpi"));
                        kpi.setValue((float) value);
                        kpiList.add(kpi);
                    }
                }

            
            
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return kpiList;
    }
    
    
    
    public static ArrayList<KPIBean> getDownloadKPI(final Integer id_city, final String type) {
        Connection conn = null;
        final ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
        try {
            conn = DBConnection.createConnection();
            
                final String query = "SELECT a.lTS, b.*\nFROM (SELECT idkpi, MAX(date) as lTS\n\tfrom kpilist \n\twhere idcity=" + id_city + " AND type='" + type + "'\n" + "\tGROUP BY idkpi) a JOIN kpilist b\n" + "      ON a.idkpi = b.idkpi\n" + "WHERE b.idcity=" + id_city + " AND b.type='" + type + "' AND\n" + "      a.lTS = b.date;";
                final Statement st = conn.createStatement();
                final ResultSet rs = st.executeQuery(query);
                KPIBean kpi = null;
                if (rs != null) {
                    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    while (rs.next()) {
                        kpi = new KPIBean();
                        kpi.setName(rs.getString("name"));
                        kpi.setDescription(rs.getString("description"));
                        kpi.setUnit(rs.getString("unit"));
                        kpi.setValue(rs.getFloat("value"));
                      
                        final Timestamp t = rs.getTimestamp("date");
                        kpi.setDate(sdf.format(t));
                        //kpi.setIdCity(id_city);
                        //kpi.setKpiLevel("level");
                        //kpi.setKpiCategory("category");
                        kpi.setNbs(rs.getString("nbs"));
                        kpi.setIdKPI(rs.getInt("idkpi"));
                        kpi.setTableId(rs.getInt("tableid"));
                        kpi.setIsManual(type);
                        kpi.setMotivation(rs.getString("motivation"));
                        //kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                        kpiList.add(kpi);
                    }
                }
           
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return kpiList;
    }

    public static String getHCategory(final String c) {
        switch (c) {
            case "airquality": {
                final String category = "air quality";
                return category;
            }
            case "green": {
                break;
            }
            case "water": {
                final String category = "water management";
                return category;
            }
            case "climate": {
                final String category = "climate change adaptation and mitigation";
                return category;
            }
            case "economic": {
                final String category = "economic opportunities and green jobs";
                return category;
            }
            default:
                break;
        }
        final String category = "green space management";
        return category;
    }

    public static String getMyIcon(final String category) {
        switch (category) {
            case "airquality": {
                final String i = "warning";
                return i;
            }
            case "green": {
            	
                break;
            }
            case "water": {
                final String i = "waves";
                return i;
            }
            case "climate": {
                final String i = "cloud_queue";
                return i;
            }
            case "economic": {
                final String i = "euro_symbol";
                return i;
            }
            case "people": {
            	final String i = "group";
            	return i;
            }
            case "planet": {
            	final String i = "public";
            	return i;
            }
            case "prosperity": {
            	final String i = "attach_money";
            	return i;
            }
            case "governance": {
            	final String i = "gavel";
            	return i; 
            }
            case "propagation": {
            	final String i = "graphic_eq";
            	return i;
            }
            default:
                break;
        }
        final String i = "nature_people";
        return i;
    }

    public static ArrayList<City> getCities() {
        Connection conn = null;
        final ArrayList<City> cities = new ArrayList<City>();
        try {
            conn = DBConnection.createConnection();
            Statement st = conn.createStatement();
            String query = "SELECT * from city";
            System.out.println(query);
            
            ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    final City city = new City();
                    city.setName(rs.getString("cityname"));
                    city.setId(rs.getInt("idcity"));
                    cities.add(city);
                }
            }
            conn.close();  
        }
        
        catch (Exception e) {
            e.printStackTrace();
        }
        return cities;
    }

    public static Integer getMaxTableId() {
        
        Integer max = 0;
        Connection conn = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT MAX(`tableid`) FROM `kpi`";
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    max = rs.getInt("MAX(`tableid`)");
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return max;
    }

    public static String getCategory(final Integer idKPI) {
        
        Connection conn = null;
        String cat = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT category from kpi";
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    cat = rs.getString("category");
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return cat;
    }

    public static String getCityName(final Integer city) {
       
        Connection conn = null;
        String cityname = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT cityname from city where idcity=" + city;
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    cityname = rs.getString("cityname");
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return cityname;
    }

    public static Integer getCityId(final String city) {
        final Logger logger = Logger.getLogger(Queries.class.getName());
        Connection conn = null;
        Integer cityId = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT idcity from city where cityname='" + city + "'";
            logger.debug((Object)query);
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    cityId = rs.getInt("idcity");
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return cityId;
    }

//    public static Integer getCategoryId(final String city, final String categoryType) {
//        Integer categoryId = 0;
//        Connection conn = null;
//        try {
//            conn = DBConnectionCalculated.createConnection();
//            final String query = "SELECT `VALUE_ID` FROM `SBI_DOMAINS` WHERE VALUE_NM='" + city.toUpperCase() + "' AND DOMAIN_NM='" + categoryType + "'";
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            if (rs != null) {
//                while (rs.next()) {
//                    categoryId = rs.getInt("VALUE_ID");
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return categoryId;
//    }

    public static Integer addKPI(final Integer id, final String name, final String desc, final String unit, final Integer city, final String level, final String category, final String isManual, final float value, String nbs) {
        final Logger logger = Logger.getLogger(Queries.class.getName());
        Connection conn = null;
        Integer retrieveID = 0;
        try {
            conn = DBConnection.createConnection();
            final String query = "INSERT INTO kpi(`name`,`type`,`tableid`,`description`,`unit`,`level`,`category`,`city`,`nbs`) VALUES ('" + name + "','" + isManual + "'," + id + ",'" + desc + "','" + unit + "','" + level + "','" + category + "'," + city + ", '"+nbs+"')";
            System.out.println(query);
            final Statement st = conn.createStatement();
            st.executeUpdate(query);
            
            conn.close();
            conn = DBConnection.createConnection();
            final String query2 = "SELECT idkpi from kpi WHERE name='" + name + "'";
            System.out.println(query2);
            final Statement st2 = conn.createStatement();
            final ResultSet rs = st2.executeQuery(query2);
            
           
            if (rs != null) {
                while (rs.next()) {
                    retrieveID = rs.getInt("idkpi");
                }
            }
            
            conn.close();
            
            System.out.println("ID nuovo KPI manuale creato: " + retrieveID + " " +isManual);
//            if (isManual.equalsIgnoreCase("manual")) {
//                final ArrayList<City> cities = getCities();
//                for (final City c : cities) {
//                    editKPIValue(c.getId(), retrieveID, value, "default value");
//                }
//            }
//            else{
//                editKPIValue(city, retrieveID, value, "default value");
//            }
            
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        
		return retrieveID;
    }
    
    public static Integer getIdEngine(String id) {
    	Statement statement = null;
        ResultSet rs = null;
        Connection con;
        Integer idEngine = null;
        String query;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DBConnectionEngine.createConnection();
            
            query = "SELECT id from formula where id_CPM="+id;
           
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            
            if (rs != null) {
                while (rs.next()) {
                    
                	idEngine = rs.getInt("id");
                }
            }
            con.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return idEngine;
    }
    
    public static String getKPIName(String idKpi) {
    	String name = "";
        
        Connection conn = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT name from kpi where idkpi="+idKpi;
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            
            if (rs != null) {
                while (rs.next()) {
                    
                   
                    name = rs.getString("name");
                   
                    
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    
    public static String getOrionEntityById(Integer idEngine) {
//    	String url = "jdbc:mysql://217.172.12.202:8093/cpm";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        String entity = null;
        String query;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DBConnectionEngine.createConnection();
            
            query = "SELECT orionEntity from formula where id="+idEngine;
           
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            
            if (rs != null) {
                while (rs.next()) {
                    
                	entity = rs.getString("orionEntity");
                }
            }
            con.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public static JSONArray getInfluxValues(String database, String id) throws JSONException, ParseException {


    	JSONArray values = new JSONArray();
    	System.out.println("DB: "+database);
    	String measurement="KPI_Values";
    	String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
    	InfluxDB influx = InfluxDBFactory.connect(url);
    	Query q = new Query ("SELECT * from " + measurement,database);
    	QueryResult queryResult = influx.query(q);
    	System.out.println(queryResult);	
    	List<Result> res = queryResult.getResults();
    	for(int i = 0; i<res.get(0).getSeries().get(0).getValues().size(); i++) {
    		JSONObject j =  new JSONObject();
    		Double value = (Double) res.get(0).getSeries().get(0).getValues().get(i).get(3);
    		
    		String time = (String) res.get(0).getSeries().get(0).getValues().get(i).get(0);
    		SimpleDateFormat dt = new SimpleDateFormat("yyyyy-MM-dd'T'HH:mm:ss.SSS'Z'"); 
    		Date date = dt.parse(time); 
    		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    		
    		String motivation = (String) res.get(0).getSeries().get(0).getValues().get(i).get(1);
    		String unit = getUnit(Integer.parseInt(id));
    		j.put("x", dt1.format(date));
    		j.put("y", value);
    		j.put("z", motivation);
    		j.put("a", unit);
    		values.put(j);
    		
    	}


    	influx.close();
    	return values;

    	}
    
    
    public static String getUnit(Integer idKpi) {
    	String unit = null;
    	Connection conn = null;
    	try {
            conn = DBConnection.createConnection();
            final String query = "SELECT unit from kpi where idkpi="+idKpi;
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    unit = rs.getString("unit");
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return unit;
    	
    }



    public static Integer addManualKPI(String motivation, final Integer id, final String name, final String desc, final String unit, final Integer city, final String level, final String category, final String isManual, final float value, String nbs) {
        final Logger logger = Logger.getLogger(Queries.class.getName());
        Connection conn = null;
        Integer retrieveID = 0;
        try {
            conn = DBConnection.createConnection();
            final String query = "INSERT INTO kpi(`name`,`type`,`tableid`,`description`,`unit`,`level`,`category`,`city`,`nbs`) VALUES ('" + name + "','" + isManual + "'," + id + ",'" + desc + "','" + unit + "','" + level + "','" + category + "'," + city + ", '"+nbs+"')";
            System.out.println(query);
            final Statement st = conn.createStatement();
            st.executeUpdate(query);
            
            conn.close();
            conn = DBConnection.createConnection();
            final String query2 = "SELECT idkpi from kpi WHERE name='" + name + "'";
            System.out.println(query2);
            final Statement st2 = conn.createStatement();
            final ResultSet rs = st2.executeQuery(query2);
            
           
            if (rs != null) {
                while (rs.next()) {
                    retrieveID = rs.getInt("idkpi");
                }
            }
            
            
            conn.close();
            
            
            System.out.println("ID nuovo KPI manuale creato: " + retrieveID + " " +isManual);
            String cityName = Queries.getCityName(city);
            String database = cityName.toLowerCase()+"_ManualKPI"+retrieveID;
            InfluxManager.createInfluxDb(database);
            InfluxManager.createInfluxMeasurement(database, name, (double) value, "KPI_Values");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        
		return retrieveID;
    }
    
    public static Integer addComputedKPI(String motivation, final Integer id, final String name, final String desc, final String unit, final Integer city, final String level, final String category, final String isManual, final float value, String nbs) {
        final Logger logger = Logger.getLogger(Queries.class.getName());
        Connection conn = null;
        Integer retrieveID = 0;
        try {
            conn = DBConnection.createConnection();
            final String query = "INSERT INTO kpi(`name`,`type`,`tableid`,`description`,`unit`,`level`,`category`,`city`,`nbs`) VALUES ('" + name + "','" + isManual + "'," + id + ",'" + desc + "','" + unit + "','" + level + "','" + category + "'," + city + ", '"+nbs+"')";
            System.out.println(query);
            final Statement st = conn.createStatement();
            st.executeUpdate(query);
            
            conn.close();
            conn = DBConnection.createConnection();
            final String query2 = "SELECT idkpi from kpi WHERE name='" + name + "'";
            System.out.println(query2);
            final Statement st2 = conn.createStatement();
            final ResultSet rs = st2.executeQuery(query2);
            
           
            if (rs != null) {
                while (rs.next()) {
                    retrieveID = rs.getInt("idkpi");
                }
            }
            
            
            conn.close();
            
            
           
//            String cityName = Queries.getCityName(city);
//            String database = cityName.toLowerCase()+"_ManualKPI"+retrieveID;
//            InfluxManager.createInfluxDb(database);
//            InfluxManager.createInfluxMeasurement(database, name, (double) value, "KPI_Values");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        
		return retrieveID;
    }
    
    
    public static Integer addCommonKPI(String motivation, final Integer id, final String name, final String desc, final String unit, final Integer city, final String level, final String category, final String isManual, final float value) {
        final Logger logger = Logger.getLogger(Queries.class.getName());
        Connection conn = null;
        Integer retrieveID = 0;
        try {
            conn = DBConnection.createConnection();
            final String query = "INSERT INTO kpi(`name`,`type`,`tableid`,`description`,`unit`,`level`,`category`,`city`) VALUES ('" + name + "','" + isManual + "'," + id + ",'" + desc + "','" + unit + "','" + level + "','" + category + "'," + city + ")";
            System.out.println(query);
            final Statement st = conn.createStatement();
            st.executeUpdate(query);
            
            conn.close();
            conn = DBConnection.createConnection();
            final String query2 = "SELECT idkpi from kpi WHERE name='" + name + "'";
            System.out.println(query2);
            final Statement st2 = conn.createStatement();
            final ResultSet rs = st2.executeQuery(query2);
            
           
            if (rs != null) {
                while (rs.next()) {
                    retrieveID = rs.getInt("idkpi");
                }
            }
            
            conn.close();
            
            System.out.println("ID nuovo KPI common creato: " + retrieveID + " " +isManual);
            ArrayList<City> cities = (ArrayList<City>)Queries.getCities();
            for (final City c : cities) {
            	String cityName = c.getName();
            	String database = cityName.toLowerCase()+"_CommonKPI"+retrieveID;
            	InfluxManager.createInfluxDb(database);
            	InfluxManager.createInfluxMeasurementManual("common kpi", database, name, (double) value, "KPI_Values");
            	
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        
		return retrieveID;
    }

    public static Integer addKPIFromEngine(final Integer id, final String name, final String desc, final String unit, final Integer city, final String level, final String category, final String isManual, final float value, String nbs) {
        final Logger logger = Logger.getLogger(Queries.class.getName());
        Connection conn = null;
        Integer retrieveID = 0;
        try {
            conn = DBConnection.createConnection();
            final String query = "INSERT INTO kpi(`name`,`type`,`tableid`,`description`,`unit`,`level`,`category`,`city`,`nbs`) VALUES ('" + name + "','" + isManual + "'," + id + ",'" + desc + "','" + unit + "','" + level + "','" + category + "'," + city + ", '"+nbs+"')";
            System.out.println(query);
            final Statement st = conn.createStatement();
            st.executeUpdate(query);
            
            conn.close();
            conn = DBConnection.createConnection();
            final String query2 = "SELECT idkpi from kpi WHERE name='" + name + "'";
            System.out.println(query2);
            final Statement st2 = conn.createStatement();
            final ResultSet rs = st2.executeQuery(query2);
            
           
            if (rs != null) {
                while (rs.next()) {
                    retrieveID = rs.getInt("idkpi");
                }
            }
            
            conn.close();
            
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        
		return retrieveID;
    }

    
   

//    public static ArrayList<KPIBean> getComputedKPI(final String level, final String category, final Integer id_city) {
//        Connection conn = null;
//        final ArrayList<KPIBean> kpicomputedList = new ArrayList<KPIBean>();
//        try {
//            conn = DBConnection.createConnection();
//            if (category.equalsIgnoreCase("all")) {
//                final String query = "SELECT `idkpi`,`category`,`name`,`tableid`,`description`,`unit`,`idknowage`, `nbs` FROM kpi WHERE level='" + level + "' AND type='automatic' AND city=" + id_city;
//                final Statement st = conn.createStatement();
//                final ResultSet rs = st.executeQuery(query);
//                KPIBean kpi = null;
//                if (rs != null) {
//                    while (rs.next()) {
//                        kpi = new KPIBean();
//                        kpi.setName(rs.getString("name"));
//                        kpi.setDescription(rs.getString("description"));
//                        kpi.setUnit(rs.getString("unit"));
//                        kpi.setKpiLevel(level);
//                        kpi.setKpiCategory(rs.getString("category"));
//                        kpi.setNbs(rs.getString("nbs"));
//                        kpi.setIdKPI(rs.getInt("idkpi"));
//                        kpi.setTableId(rs.getInt("tableid"));
//                        kpi.setIdKnowage(rs.getInt("idknowage"));
//                        kpi.setIsManual("automatic");
//                        kpi.setIdCity(id_city);
//                        kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
//                        kpicomputedList.add(kpi);
//                    }
//                }
//            }
//            else {
//                final String query = "SELECT `idkpi`,`name`,`tableid`,`description`,`unit`,`idknowage`,`nbs`  FROM kpi WHERE level='" + level + "' AND category='" + category + "' AND type='automatic' AND city=" + id_city;
//                final Statement st = conn.createStatement();
//                final ResultSet rs = st.executeQuery(query);
//                KPIBean kpi = null;
//                if (rs != null) {
//                    while (rs.next()) {
//                        kpi = new KPIBean();
//                        kpi.setName(rs.getString("name"));
//                        kpi.setDescription(rs.getString("description"));
//                        kpi.setUnit(rs.getString("unit"));
//                        kpi.setKpiLevel(level);
//                        kpi.setKpiCategory(category);
//                        kpi.setNbs(rs.getString("nbs"));
//                        kpi.setIdKPI(rs.getInt("idkpi"));
//                        kpi.setTableId(rs.getInt("tableid"));
//                        kpi.setIdKnowage(rs.getInt("idknowage"));
//                        kpi.setIsManual("automatic");
//                        kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
//                        kpicomputedList.add(kpi);
//                    }
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//        return kpicomputedList;
//    }

    
    
    public static ArrayList<KPIBean> getDownloadComputedKPI(final Integer id_city) {
        Connection conn = null;
        final ArrayList<KPIBean> kpicomputedList = new ArrayList<KPIBean>();
        try {
            conn = DBConnection.createConnection();
            
                final String query = "SELECT `idkpi`,`category`,`name`,`tableid`,`description`,`unit`,`idknowage`,`nbs`  FROM kpi WHERE type='automatic' AND city=" + id_city;
                final Statement st = conn.createStatement();
                final ResultSet rs = st.executeQuery(query);
                KPIBean kpi = null;
                if (rs != null) {
                    while (rs.next()) {
                        kpi = new KPIBean();
                        kpi.setName(rs.getString("name"));
                        kpi.setDescription(rs.getString("description"));
                        kpi.setUnit(rs.getString("unit"));
                        //kpi.setKpiLevel("level");
                        //kpi.setKpiCategory(rs.getString("category"));
                        kpi.setNbs(rs.getString("nbs"));
                        kpi.setIdKPI(rs.getInt("idkpi"));
                        kpi.setTableId(rs.getInt("tableid"));
                        //kpi.setIdKnowage(rs.getInt("idknowage"));
                        kpi.setIsManual("automatic");
                        //kpi.setIdCity(id_city);
                        //kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                        kpicomputedList.add(kpi);
                    }
                }
           
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return kpicomputedList;
    }

    
    
//    public static ArrayList<Integer> getIds(final ArrayList<KPIBean> kpicomputedList, final Integer id_city) {
//        final ArrayList<Integer> idKnowage = new ArrayList<Integer>();
//        Connection conn = null;
//        try {
//            conn = DBConnectionCalculated.createConnection();
//            final String query = "SELECT ID from SBI_KPI_KPI where CATEGORY_ID=" + id_city;
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            Integer id = 0;
//            if (rs != null) {
//                while (rs.next()) {
//                    id = rs.getInt("ID");
//                    idKnowage.add(id);
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//        return idKnowage;
//    }
//
//    public static ArrayList<KPIBean> getComputedValues(final ArrayList<KPIBean> kpicomputedList, final ArrayList<Integer> idKnowage) {
//        Connection conn = null;
//        try {
//            for (int i = 0; i < idKnowage.size(); ++i) {
//                for (int j = 0; j < kpicomputedList.size(); ++j) {
//                    if (idKnowage.get(i) == kpicomputedList.get(j).getIdKnowage()) {
//                        conn = DBConnectionCalculated.createConnection();
//                        final String query = "SELECT a.d, b.* FROM\n (SELECT COMPUTED_VALUE, MAX(TIME_RUN) as d\n from SBI_KPI_VALUE WHERE KPI_ID=" + kpicomputedList.get(j).getIdKnowage() + "\n " + "GROUP BY COMPUTED_VALUE) a JOIN knowage_ce.SBI_KPI_VALUE b\n " + "ON a.COMPUTED_VALUE = b.COMPUTED_VALUE WHERE b.KPI_ID=" + kpicomputedList.get(j).getIdKnowage() + " AND a.d = b.TIME_RUN\n " + "ORDER BY ID DESC LIMIT 1";
//                        Queries.logger.debug((Object)query);
//                        final Statement st = conn.createStatement();
//                        final ResultSet rs = st.executeQuery(query);
//                        if (rs.next()) {
//                            do {
//                                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                float value = (float)rs.getInt("COMPUTED_VALUE");
//                                Queries.logger.debug((Object)("COMPUTED VALUE: " + value));
//                                final Timestamp t = rs.getTimestamp("TIME_RUN");
//                                if (t == null) {
//                                    final Date dateobj = new Date();
//                                    kpicomputedList.get(j).setDate(sdf.format(dateobj));
//                                    value = 0.0f;
//                                    kpicomputedList.get(j).setValue(value);
//                                }
//                                else {
//                                    kpicomputedList.get(j).setDate(sdf.format(t));
//                                    kpicomputedList.get(j).setValue(value);
//                                }
//                            } while (rs.next());
//                            rs.close();
//                        }
//                        else {
//                            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                            final Date dateobj2 = new Date();
//                            kpicomputedList.get(j).setDate(sdf.format(dateobj2));
//                            final float value = 0.0f;
//                            kpicomputedList.get(j).setValue(value);
//                        }
//                    }
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return kpicomputedList;
//    }
//
//    public static List<Integer> verifyId(final List<Integer> ids) {
//        final List<Integer> x = new ArrayList<Integer>();
//        Connection conn = null;
//       
//        try {
//            for (int j = 0; j < ids.size(); ++j) {
//                conn = DBConnection.createConnection();
//                final String query = "SELECT COUNT(*) as calculated from kpi where idknowage=" + ids.get(j);
//                final Statement st = conn.createStatement();
//                final ResultSet rs = st.executeQuery(query);
//                while (rs.next()) {
//                    if (rs.getInt("calculated") == 0) {
//                        x.add(ids.get(j));
//                    }
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return x;
//    }
//
//    public static List<Integer> getComputedIds(final Integer category_city) {
//        final List<Integer> id = new ArrayList<Integer>();
//        Connection conn = null;
//        Integer i = 0;
//        try {
//            conn = DBConnectionCalculated.createConnection();
//            final String query = "SELECT ID from SBI_KPI_KPI where CATEGORY_ID=" + category_city;
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            if (rs != null) {
//                while (rs.next()) {
//                    i = rs.getInt("ID");
//                    id.add(i);
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return id;
//    }
//
//    public static List<JSONObject> getComputedIndicators(final List<Integer> presentIds) {
//        List<JSONObject> indicators = new ArrayList<JSONObject>();
//        Connection conn = null;
//        try {
//            String ids = " ";
//            for (int i = 0; i < presentIds.size(); ++i) {
//                ids = String.valueOf(ids) + "ID=" + presentIds.get(i);
//                if (i <= presentIds.size() - 2) {
//                    ids = String.valueOf(ids) + " OR ";
//                }
//            }
//            conn = DBConnectionCalculated.createConnection();
//            final String query = "SELECT ID,NAME from SBI_KPI_KPI where (" + ids + ")";
//            Queries.logger.debug((Object)("query:" + query));
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            indicators = getFormattedFilteredResult(rs);
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return indicators;
//    }

    public static List<JSONObject> getFormattedFilteredResult(final ResultSet rs) {
        final List<JSONObject> resList = new ArrayList<JSONObject>();
        try {
            final ResultSetMetaData rsMeta = rs.getMetaData();
            final int columnCnt = rsMeta.getColumnCount();
            final List<String> columnNames = new ArrayList<String>();
            for (int i = 1; i <= columnCnt; ++i) {
                columnNames.add(rsMeta.getColumnName(i).toUpperCase());
            }
            while (rs.next()) {
                final JSONObject obj = new JSONObject();
                for (int j = 1; j <= columnCnt; ++j) {
                    final String key = columnNames.get(j - 1);
                    final String value = rs.getString(j);
                    obj.put(key, (Object)value);
                }
                resList.add(obj);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            try {
                rs.close();
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
            return resList;
        }
        finally {
            try {
                rs.close();
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
        try {
            rs.close();
        }
        catch (SQLException e2) {
            e2.printStackTrace();
        }
        return resList;
    }

//    public static Integer getIdKnowageByidKpi(final Integer id) {
//        Connection conn = null;
//        Integer idK = 0;
//        try {
//            conn = DBConnection.createConnection();
//            final String query = "SELECT idknowage from kpi where idkpi=" + id;
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            if (rs != null) {
//                while (rs.next()) {
//                    idK = rs.getInt("idknowage");
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return idK;
//    }
//
//    public static Integer getSingleIdKnowage(final String tableID) {
//        Connection conn = null;
//        Integer id = 0;
//        try {
//            conn = DBConnection.createConnection();
//            final String query = "SELECT idknowage from kpi where tableid=" + tableID;
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            if (rs != null) {
//                while (rs.next()) {
//                    id = rs.getInt("idknowage");
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return id;
//    }

   
    public static boolean isPresentManualKpi(final Integer kPI_id) {
        boolean isPresent = false;
        Connection conn = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT COUNT(*) as valore from value where idkpi=" + kPI_id;
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                isPresent = (rs.getInt("valore") != 0);
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return isPresent;
    }

    
    public static void updateKPI(final Integer kPI_id, final String name, final String desc, final String unit, final String isManual, final Integer city, String nbs) {
        Connection conn = null;
        String query;
        
            query = "UPDATE kpi SET name='" + name + "', description='" + desc + "', type='" + isManual + "', unit='" + unit + "', city=" + city + ", nbs='" + nbs + "' where idkpi=" + kPI_id;
       
        try {
            Queries.logger.debug((Object)query);
            conn = DBConnection.createConnection();
            final Statement st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
       
    }

//    public static float getManualValue(final Integer id_kpi, final Integer id_city) {
//        Connection conn = null;
//        float value = 0.0f;
//        try {
//            conn = DBConnection.createConnection();
//            final String query = "SELECT value, MAX(date) FROM kpilist WHERE idkpi=" + id_kpi + " AND idcity=" + id_city;
//            final Statement st = conn.createStatement();
//            final ResultSet rs = st.executeQuery(query);
//            if (rs != null) {
//                while (rs.next()) {
//                    value = rs.getFloat("value");
//                }
//            }
//            conn.close();
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            return 0.0f;
//        }
//        return value;
//    }

    public static ArrayList<KPIBean> getKpi4Mashup() {
        final ArrayList<KPIBean> kpis = new ArrayList<KPIBean>();
        
        Connection conn = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT idkpi, name, type, description from kpi";
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            KPIBean kpi = null;
            if (rs != null) {
                while (rs.next()) {
                    kpi = new KPIBean();
                    kpi.setIdKPI(rs.getInt("idkpi"));
                    kpi.setName(rs.getString("name"));
                    kpi.setDescription(rs.getString("description"));
                    kpi.setIsManual(rs.getString("type"));
                    kpis.add(kpi);
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return kpis;
    }

    public static String authenticateUser(final LoginBean user) {
        final String userName = user.getMail();
        final String password = user.getPassword();
        Connection con = null;
        Statement statement = null;
        ResultSet resultSet = null;
        String userNameDB = "";
        String passwordDB = "";
        String roleDB = "";
        String pilot = "";
        String mail = "";
        try {
            con = DBConnection.createConnection();
            statement = con.createStatement();
            resultSet = statement.executeQuery("select * from users");
            while (resultSet.next()) {
                userNameDB = resultSet.getString("username");
                mail = resultSet.getString("mail");
                passwordDB = resultSet.getString("password");
                roleDB = resultSet.getString("role");
                final Integer userId = resultSet.getInt("id");
                pilot = resultSet.getString("pilot");
                if (userName.equals(mail) && password.equals(passwordDB) && roleDB.equals("admin") && pilot.equals("all")) {
                    return "admin";
                }
                if (userName.equals(mail) && password.equals(passwordDB) && roleDB.equals("city_manager")) {
                    return String.valueOf(pilot.toLowerCase()) + "_city_manager";
                }
            }
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        
        return "Invalid user credentials";
    }


	public static Integer checkInfluxData(Integer checkId) {
//		String url = "jdbc:mysql://217.172.12.202:8093/kpiEngine";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        Integer idEngine = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DBConnectionEngine.createConnection();
            //con = DriverManager.getConnection(url, username, password);
            String query = "SELECT id FROM formula WHERE id_CPM="+checkId+ " AND status='PLAYED'";
            System.out.println(query);
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            System.out.println(rs);
            if (rs.next()) {
                do{
                	idEngine = rs.getInt("id");
                }while (rs.next());
                    
            }
            
            else {
            	idEngine=0;
            }
            con.close();
	}
        
        catch (SQLException e) {
            e.printStackTrace();
        }
        
        return idEngine;
}


	public static String getValueEngine(String id) {
//		String url = "jdbc:mysql://217.172.12.202:8093/kpiEngine";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        String name = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DBConnectionEngine.createConnection();
            //con = DriverManager.getConnection(url, username, password);
            String query = "SELECT name FROM formula WHERE id_CPM="+id;
            System.out.println(query);
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            System.out.println(rs);
            if (rs != null) {
                while (rs.next()) {
                	name = rs.getString("name");
                }
                    
            }
            
            
            con.close();
	}
        
        catch (SQLException e) {
            e.printStackTrace();
        }
        
        System.out.println("NAME: " + name);
        return name;
	}


	public static void updateFormula(String kpiName, Integer kPI_id) {
//		String url = "jdbc:mysql://217.172.12.202:8093/kpiEngine";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        String name = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            //con = DriverManager.getConnection(url, username, password);
            con = DBConnectionEngine.createConnection();
            String query = "UPDATE formula SET id_CPM=" + kPI_id + " where name='" + kpiName+"'";
            
            System.out.println(query);
            statement = con.createStatement();
            statement.executeUpdate(query);
           // System.out.println(rs);
//            if (rs != null) {
//                while (rs.next()) {
//                	name = rs.getString("name");
//                }
//                    
//            }
            
            
            con.close();
	}
        
        catch (SQLException e) {
            e.printStackTrace();
        }
        
       
	}


	public static KPIBean getKPIFromKpiTable(Integer checkId) {
		KPIBean kpi = new KPIBean();
        
        Connection conn = null;
        try {
            conn = DBConnection.createConnection();
            final String query = "SELECT * from kpi where idkpi="+checkId;
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            
            if (rs != null) {
                while (rs.next()) {
                    
                   
                    kpi.setName(rs.getString("name"));
                    kpi.setDescription(rs.getString("description"));
                    kpi.setTableId(rs.getInt("tableid"));
                    kpi.setUnit(rs.getString("unit"));
                    kpi.setNbs(rs.getString("nbs"));
                    kpi.setKpiCategory(rs.getString("category"));
                    kpi.setKpiLevel(rs.getString("level"));
                    kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                    
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return kpi;
	}


	public static ArrayList<KPIBean> getComputedKPI(String level, String category, Integer id_city) {
		
        ArrayList<KPIBean> kpis = new ArrayList<KPIBean>();
        Connection conn = null;
        try {
            conn = DBConnection.createConnection();
            String query;
            if(category.equalsIgnoreCase("all")){
            	query = "SELECT * from kpi where type='computed' AND level='"+level+"' AND city="+id_city;
            }
            else {
            	query = "SELECT * from kpi where type='computed' AND level='"+level+"' AND category='"+category+"' AND city="+id_city;
            }
            
            final Statement st = conn.createStatement();
            final ResultSet rs = st.executeQuery(query);
            KPIBean kpi = null;
            if (rs != null) {
                while (rs.next()) {
                    
                	kpi = new KPIBean();
                	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    kpi.setName(rs.getString("name"));
                    kpi.setDescription(rs.getString("description"));
                    kpi.setTableId(rs.getInt("tableid"));
                    kpi.setUnit(rs.getString("unit"));
                    kpi.setNbs(rs.getString("nbs"));
                    kpi.setKpiCategory(rs.getString("category"));
                    kpi.setKpiLevel(rs.getString("level"));
                    kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
                    kpi.setDate(sdf.format(timestamp));
                    kpi.setIdKPI(rs.getInt("idkpi"));
                   
                    kpi.setEntity(rs.getString("orionEntity"));
                    kpis.add(kpi);
                    
                }
            }
            conn.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return kpis;
	}


	public static void deleteId(Integer kPI_id) {
//		String url = "jdbc:mysql://217.172.12.202:8093/kpiEngine";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            //con = DriverManager.getConnection(url, username, password);
            con = DBConnectionEngine.createConnection();
            String query = "DELETE FROM formula WHERE id_CPM="+kPI_id;
            
            System.out.println(query);
            statement = con.createStatement();
            statement.execute(query);
		
        }
        
        catch (Exception e) {
            e.printStackTrace();
        }
	}


	public static void updateFormulaEntity(String entity, Integer id) {
//		String url = "jdbc:mysql://217.172.12.202:8093/kpiEngine";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        String name = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            //con = DriverManager.getConnection(url, username, password);
            con = DBConnectionEngine.createConnection();
            String query = "UPDATE formula SET orionEntity='" + entity + "' where id=" + id;
            
            System.out.println(query);
            statement = con.createStatement();
            statement.executeUpdate(query);
            
            
            con.close();
	}
        
        catch (SQLException e) {
            e.printStackTrace();
        }
        
		
	}


	public static void updateEntityKpi(Integer idKpi, String entity) {
//		String url = "jdbc:mysql://217.172.12.202:8093/cpm";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        String name = null;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DBConnectionEngine.createConnection();
            
            String query = "UPDATE kpi SET orionEntity='" + idKpi.toString() + "' where idkpi=" + idKpi;
            
            System.out.println(query);
            statement = con.createStatement();

            
            
            con.close();
	}
        
        catch (SQLException e) {
            e.printStackTrace();
        }
        
		
		
	}


	public static String getOrionEntity(String auto) {
//		String url = "jdbc:mysql://217.172.12.202:8093/cpm";
//        String username = "root";
//        String password = "mysql";
        Statement statement = null;
        ResultSet rs = null;
        Connection con;
        String entity = null;
        String query;
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DBConnectionEngine.createConnection();
            
            query = "SELECT orionEntity from formula where name='"+auto+"'";
           
            statement = con.createStatement();
            rs = statement.executeQuery(query);
            
            if (rs != null) {
                while (rs.next()) {
                    
                	entity = rs.getString("orionEntity");
                }
            }
            con.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
	}
	
	public static List<String> getInfluxIoTDatabaseNames(String city) {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		String db = "_internal";
		System.out.println("DB: "+db);
		InfluxDB influx = InfluxDBFactory.connect(url);
		
		Query q = new Query ("SHOW DATABASES",db);
		QueryResult queryResult = influx.query(q);			
		List<Result> res = queryResult.getResults();
		List<String> iotDatabases = new ArrayList<String>();
		List<List<Object>> databases = res.get(0).getSeries().get(0).getValues();
		for (List<Object> database : databases) {
			String dtb = (String) database.get(0);
			if(filterIoT(dtb, city.toLowerCase())) {
				iotDatabases.add(dtb);
			};
		}
		influx.close();
		return iotDatabases;
	}
	
	public static List<String> getInfluxIoTDatabaseMeasures(String db) {
		String url = PropertyManager.getProperty(manualKPIProperties.INFLUX_DB);
		System.out.println("DB: "+db);
		InfluxDB influx = InfluxDBFactory.connect(url);
		
		Query q = new Query ("SHOW MEASUREMENTS",db);
		QueryResult queryResult = influx.query(q);			
		List<Result> res = queryResult.getResults();
		List<String> iotMeasures = new ArrayList<String>();
		List<List<Object>> measures = res.get(0).getSeries().get(0).getValues();
		for (List<Object> measure : measures) {
			String msr = (String) measure.get(0);
			iotMeasures.add(msr);
		}
		influx.close();
		return iotMeasures;
	}
	
	protected static Boolean filterIoT(String database, String city) {
		Boolean isIoT = false;
		String[] values = database.split("_");
		String[] categories = new String[]{"environment","mobility","parking","waste","illumination","tourism","water","kpi","issues","taxes","controlroom"};
		List<String> categoryList = Arrays.asList(categories);
		if(city.equalsIgnoreCase(values[0]) && categoryList.contains(values[1])) {
			isIoT = true;
		}
		return isIoT;
	}
}
