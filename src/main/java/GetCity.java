package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetCity" })
public class GetCity extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String idcity = request.getParameter("id");
        Integer id = 0;
        String city;
        if (idcity.matches(".*\\d.*")) {
            id = Integer.parseInt(idcity);
            city = Queries.getCityName(id);
        }
        else {
            city = idcity;
        }
        System.out.println("CITY " + city);
        response.getWriter().write(city);
    }
}
