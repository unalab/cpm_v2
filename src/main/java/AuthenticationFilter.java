package main.java;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import java.io.IOException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.FilterChain;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.Filter;

public class AuthenticationFilter implements Filter
{
    private ServletContext context;
    Logger logger;

    public AuthenticationFilter() {
        this.logger = Logger.getLogger(AuthenticationFilter.class.getName());
    }

    public void destroy() {
    }

    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest)request;
        final HttpServletResponse res = (HttpServletResponse)response;
        final HttpSession session = req.getSession(true);
        if (request.getParameter("lang") != null) {
            session.setAttribute("lang", (Object)request.getParameter("lang"));
        }
        else if (request.getParameter("lang") == null && session.getAttribute("lang") == null) {
            session.setAttribute("lang", (Object)"en");
        }
        final String path = req.getRequestURI().substring(req.getContextPath().length()).toString();
        String role = "null";
        role = (String)session.getAttribute("user");
        if (role == null) {
            role = "null";
        }
        if (role.equalsIgnoreCase("admin")) {
            if (RolePermission.adminPermission(path)) {
                chain.doFilter(request, response);
            }
            else {
                res.sendRedirect(String.valueOf(req.getContextPath()) + "/index.jsp");
            }
        }
        else if (role.equalsIgnoreCase("null")) {
            if (RolePermission.guestPermission(path)) {
                chain.doFilter(request, response);
            }
            else {
                this.context.log(String.valueOf(path) + " Accesso non autorizzato");
                res.sendRedirect(String.valueOf(req.getContextPath()) + "/index.jsp");
            }
        }
        else if (RolePermission.citymanagerPermission(path)) {
            chain.doFilter(request, response);
        }
        else {
            res.sendRedirect(String.valueOf(req.getContextPath()) + "/index.jsp");
        }
    }

    public void init(final FilterConfig fConfig) throws ServletException {
        (this.context = fConfig.getServletContext()).log("AuthenticationFilter id initialized");
    }
}
