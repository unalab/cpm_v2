package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/ModifyKPI" })
public class ModifyKPI extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public ModifyKPI() {
        this.logger = Logger.getLogger(ModifyKPI.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String name = request.getParameter("name");
        String isManual = request.getParameter("setComp");
        final String desc = request.getParameter("desc");
        final String unit = request.getParameter("unit");
        final String idCityP = request.getParameter("city");
        final String level = request.getParameter("level");
        final String category = request.getParameter("category");
        final String KPI_idP = request.getParameter("KPI_id");
        final String id_knowage = request.getParameter("id_knowage");
        String nbs = request.getParameter("nbs");
        Integer idCity;
        if (idCityP.matches(".*\\d.*")) {
            idCity = Integer.parseInt(idCityP);
        }
        else {
            idCity = Queries.getCityId(idCityP);
        }
        final Integer KPI_id = Integer.parseInt(KPI_idP);
        Integer id_knowageInt;
        
        if (isManual != null) {
            isManual = "automatic";
            Queries.updateKPI(KPI_id, name, desc, unit, isManual, idCity, nbs);
        }
        else {
            isManual = "manual";
            //final boolean presente = Queries.isPresentManualKpi(KPI_id);
//            if (!presente) {
//                Queries.addKPI(KPI_id, name, desc, unit, idCity, level, category, isManual, 0.0f, nbs);
//            }
//            else {
                Queries.updateKPI(KPI_id, name, desc, unit, isManual, idCity, nbs);
            //}
        }
        final String nextJSP = "/cpm_v2/view.jsp?city=" + idCity + "&level=" + level + "&category=" + category;
        response.sendRedirect(nextJSP);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
