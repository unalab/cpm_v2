package main.java;

import java.io.IOException;
import javax.servlet.ServletException;

import java.util.HashSet;
import main.bean.City;


import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/AddKPIfromEngine" })
public class AddKPIfromEngine extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public AddKPIfromEngine() {
        this.logger = Logger.getLogger(AddKPIfromEngine.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        
        float value = 0.0f;
        String idP = request.getParameter("id");
        String name = request.getParameter("name");
        name = name.trim();
        String desc = request.getParameter("desc");
        desc = desc.trim();
        String unit = request.getParameter("unit");
        unit = unit.trim();
        String cityP = request.getParameter("city");
        String level = request.getParameter("level");
        String nbs = request.getParameter("nbs");
        //Integer city = Integer.parseInt(cityP);
        Integer id = Integer.parseInt(idP);
        String valueS = "0";
        String kpiName="";
        
        kpiName = request.getParameter("kpiName");
        	
        Integer city = Queries.getCityId(cityP);
        ArrayList<City> cities = new ArrayList<City>();
        HashSet<String> notificationAttrs = new HashSet<String>();
        notificationAttrs.add("value");
        Integer idKPI = 0;
   
        String category = request.getParameter("cat2");
        idKPI = Queries.addKPIFromEngine(id, name, desc, unit, city, level, category, "computed", value, nbs);
        String idKpi = idKPI.toString();
        System.out.println("IDKPI: " + idKpi);

        response.getWriter().write(idKpi);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
