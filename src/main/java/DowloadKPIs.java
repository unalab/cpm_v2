package main.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import java.util.Collection;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

import org.json.simple.JSONArray;  
import org.json.simple.JSONObject;

import com.google.gson.Gson;

import main.tools.Ckan;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/download" })
public class DowloadKPIs extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public DowloadKPIs() {
        this.logger = Logger.getLogger(DowloadKPIs.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        ArrayList<KPIBean> kpiList = null;
        ArrayList<KPIBean> kpicomputedList = null;
        ArrayList<KPIBean> intermediate = null;
        final ArrayList<KPIBean> results = new ArrayList<KPIBean>();
        ArrayList<Integer> ids = null;
        ArrayList<KPIBean> cityKpi = null;
        //String level = request.getParameter("one");
        JSONArray kpis = new JSONArray();
        final String city = request.getParameter("city");
        final Integer id_city = Integer.parseInt(city);
        final String cityS = Queries.getCityName(id_city);
        //final Integer category_city = Queries.getCategoryId(cityS.toUpperCase(), "KPI_KPI_CATEGORY");
        String lang;
        Ckan ckan = new Ckan();
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        //String category;
//        if (level == null || level == "") {
//            level = request.getParameter("level");
//            category = request.getParameter("category");
//        }
//        else if (level.equalsIgnoreCase("task")) {
//            category = request.getParameter("two");
//        }
//        else {
//            category = request.getParameter("third");
//        }
        kpiList = (ArrayList<KPIBean>)Queries.getDownloadKPI(id_city, "manual");
        intermediate = (ArrayList<KPIBean>)Queries.getDownloadComputedKPI(id_city);
        //ids = (ArrayList<Integer>)Queries.getIds((ArrayList)intermediate, category_city);
        //kpicomputedList = (ArrayList<KPIBean>)Queries.getComputedValues((ArrayList)intermediate, (ArrayList)ids);
        if (kpiList != null) {
            results.addAll(kpiList);
        }
//      if (kpicomputedList != null) {
//            results.addAll(kpicomputedList);
//        }
        cityKpi = (ArrayList<KPIBean>)Queries.getDownloadKPI(id_city, "citymanual");
        if (cityKpi != null) {
            results.addAll(cityKpi);
        }
        
         
        
         String jsonString = new Gson().toJson(results);
         
         jsonString = jsonString.replace("[{", "{\"map\": [{");
         jsonString = jsonString.replace("}]", "}]}");
         
//         System.out.println(jsonString);
//         try {
//				ckan.ckanDatasetResourceMngt(jsonString, cityS);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
         jsonString = jsonString.replace("{\"map\":", "");
		 jsonString = jsonString.replace("}}", "}"); 
		 //jsonString = jsonString.replace("{\"myArrayList\":", ""); 
		 jsonString = jsonString.replace("]}", "]");
		 jsonString = jsonString.replace("idKPI", "entityID");
		 jsonString = jsonString.replace("unit", "measureUnit");
		 jsonString = jsonString.replace("date", "lastUpdate");
		 jsonString = jsonString.replace("tableId", "id");
		 jsonString = jsonString.replace("isManual", "type");
		 jsonString = jsonString.replace("motivation", "note");
		 jsonString = jsonString.replace("nbs", "NBSTitle");
		 
		 
		
			
		 System.out.println(jsonString);
		 
		 response.setCharacterEncoding("UTF-8");
		 response.getWriter().write(jsonString);
       
   }
        


    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
