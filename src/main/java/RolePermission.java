package main.java;

import java.util.ArrayList;
import java.util.List;

public class RolePermission
{
    public static List<String> basePermission() {
        final List<String> basePermission = new ArrayList<String>();
        basePermission.add("/");
        basePermission.add("/css/cardScroll.css");
        basePermission.add("/css/common.css");
        basePermission.add("/css/custom.css");
        basePermission.add("/css/materialize.css");
        basePermission.add("/css/bootstrap.css");
        basePermission.add("/css/bootstrap.min.css");
        basePermission.add("/css/mdb.css");
        basePermission.add("/css/mdb.min.css");
        basePermission.add("/css/main.css");
        basePermission.add("/css/buttons.css");
        basePermission.add("/css/styles.css");
        basePermission.add("/css/materialize.min.css");
        basePermission.add("/css/table-sorter.css");
        basePermission.add("/css/theme.default.css");
        basePermission.add("/css/gauge.css");
        basePermission.add("/css/codemirror.css");
        basePermission.add("/css/customEngine.css");
        basePermission.add("/css/formulize.css");
        basePermission.add("/css/ui.css");
        basePermission.add("/css/style.css");
        basePermission.add("/img/Eindhoven.jpg");
        basePermission.add("/img/Genova.jpg");
        basePermission.add("/img/logo_alpha_270x.png");
        basePermission.add("/img/Tampere.jpg");
        basePermission.add("/img/icons/icon-uptake.png");
        basePermission.add("/img/angle-arrow-down.svg");
        basePermission.add("/img/navigate-up-arrow.svg");
        basePermission.add("/img/umbrella.png");
        basePermission.add("/img/siPer.jpg");
        basePermission.add("/img/1.svg");
        basePermission.add("img/angle-arrow-down.svg");
        basePermission.add("img/navigate-up-arrow.svg");
        basePermission.add("/img/measure.png");
        basePermission.add("/img/formula.png");
        basePermission.add("/img/2.png");
        basePermission.add("/img/3.png");
        basePermission.add("/img/digital-enabler-white_symbol.png");
        basePermission.add("/img/digital-enabler-white.png");
        basePermission.add("/js/materialize.js");
        basePermission.add("/js/common.js");
        basePermission.add("/js/custom.js");
        basePermission.add("/js/expert.js");
        basePermission.add("/js/materialize.min.js");
        basePermission.add("/js/materialize-pagination.js");
        basePermission.add("/js/paginathing.js");
        basePermission.add("/js/pagination.js");
        basePermission.add("/js/table-sorter.js");
        basePermission.add("/js/variable.js");
        basePermission.add("/js/bootstrap/bootstrap.js");
        basePermission.add("/js/bootstrap/bootstrap.min.js");
        basePermission.add("/js/bootstrap/jquery-3.2.1.min.js");
        basePermission.add("/js/bootstrap/main.js");
        basePermission.add("/js/bootstrap/mdb.js");
        basePermission.add("/js/bootstrap/mdb.min.js");
        basePermission.add("/js/bootstrap/popper.min.js");
        basePermission.add("/js/bootstrap/isotope.js");
        basePermission.add("/js/bootstrap/item.js");
        basePermission.add("/js/bootstrap/layout-mode.js");
        basePermission.add("/js/jspdf.js");
        basePermission.add("/js/sortable.js");
        basePermission.add("/js/jquery.tablesorter.js");
        basePermission.add("/js/jquery.tablesorter.widgets.js");
        basePermission.add("/js/customgauge.js");
        basePermission.add("/js/dist/html2pdf.bundle.min.js");
        basePermission.add("/js/dist/");
        basePermission.add("/login.jsp");
        basePermission.add("/dashboard.jsp");
        basePermission.add("/index.jsp");
        basePermission.add("/commonKPI.jsp");
        basePermission.add("/guest.jsp");
        basePermission.add("/GetCalculatedIndicators");
        basePermission.add("/test");
        basePermission.add("/GetCommonKPIs");
        basePermission.add("/GetIdCityKnowage");
        basePermission.add("/getNewStatements");
        basePermission.add("/GetIdKnowage");
        basePermission.add("/GetChartData");
        basePermission.add("/GetGuestKPIList");
        basePermission.add("/GetGuestKPIList2");
        basePermission.add("/GetKPIList");
        basePermission.add("/getCpmKPIs");
        basePermission.add("/GetValue");
        basePermission.add("/GetKPIName");
        basePermission.add("/GetKPIUnit");
        basePermission.add("/Log4JInitServlet");
        basePermission.add("/LoginServlet");
        basePermission.add("/GetCity");
        basePermission.add("/download");
        basePermission.add("/getInitialKPI()");
        basePermission.add("/js/admin.js");
        basePermission.add("/js/guest.js");
        basePermission.add("/js/view.js");
        basePermission.add("/js/css.js");
        basePermission.add("/js/customEngine.js");
        basePermission.add("/js/Chart.min.js");
        basePermission.add("/js/cypher.js");
        basePermission.add("/js/dbConfiguration.js");
        basePermission.add("/js/formula.js");
        basePermission.add("/js/formulize.umd.js");
        basePermission.add("/js/htmlmixed.js");
        basePermission.add("/js/schedule.js");
        basePermission.add("/js/calculations.js");
        basePermission.add("/js/codemirror.js");
        
        return basePermission;
    }

    public static boolean adminPermission(final String path) {
        final List<String> admin = basePermission();
        admin.add("/admin.jsp");
        admin.add("/AddAdminKPI");
        admin.add("/EditKPI");
        admin.add("/GetAdminKPIList");
        admin.add("/ModifyAdminKPI");
        admin.add("/LogoutServlet");
        
        return admin.contains(path);
    }

    public static boolean citymanagerPermission(final String path) {
        final List<String> citymanager = basePermission();
        citymanager.add("/view.jsp");
        citymanager.add("/AddKPI");
        citymanager.add("/EditKPI");
        citymanager.add("/GetKPIList");
        citymanager.add("/ModifyCityKPI");
        citymanager.add("/AddKPIfromEngine");
        citymanager.add("/LogoutServlet");
        citymanager.add("/AddComputedKPI");
        citymanager.add("/GetValueEngine");
        citymanager.add("/LogoutServlet");
        citymanager.add("/check");
        citymanager.add("/executeStat");
        citymanager.add("/getAllMeasures");
        citymanager.add("/getFormulas");      
        citymanager.add("/getNewStatements");
        citymanager.add("/getKPIs");
        citymanager.add("/getStatements");
        citymanager.add("/influxConnect");
        citymanager.add("/jobs");
        citymanager.add("/sqlConnect");
        citymanager.add("/saveFormula");
        citymanager.add("/saveJob");
        citymanager.add("/save");
        citymanager.add("/showDB");
        citymanager.add("/showMeas");
        citymanager.add("/interrupt");
        citymanager.add("/unalabConnect");
        citymanager.add("/restConnect");
        citymanager.add("/getJob");
        citymanager.add("/overview.jsp");
        citymanager.add("/measure.jsp");
        citymanager.add("/formula.jsp");
        citymanager.add("/newformulapage.jsp");
        citymanager.add("/schedule.jsp");
        return citymanager.contains(path);
    }

    public static boolean guestPermission(final String path) {
        final List<String> guest = basePermission();
        return guest.contains(path);
    }
}
