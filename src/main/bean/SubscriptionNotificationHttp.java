package main.bean;

public class SubscriptionNotificationHttp
{
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public SubscriptionNotificationHttp(final String url) {
        this.url = url;
    }

    public SubscriptionNotificationHttp() {
        this.url = "http://localhost:8080/cpm_v2";
    }
}
