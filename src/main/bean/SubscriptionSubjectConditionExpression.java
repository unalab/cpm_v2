package main.bean;

public class SubscriptionSubjectConditionExpression
{
    String q;
    String mq;
    String geometry;
    String coords;
    String georel;

    public String getQ() {
        return this.q;
    }

    public void setQ(final String q) {
        this.q = q;
        this.mq = null;
        this.geometry = null;
        this.coords = null;
        this.georel = null;
    }

    public String getMq() {
        return this.mq;
    }

    public void setMq(final String mq) {
        this.q = null;
        this.mq = mq;
        this.geometry = null;
        this.coords = null;
        this.georel = null;
    }

    public String getGeometry() {
        return this.geometry;
    }

    public void setGeometry(final String geometry) {
        this.q = null;
        this.mq = null;
        this.geometry = geometry;
        this.coords = null;
        this.georel = null;
    }

    public String getCoords() {
        return this.coords;
    }

    public void setCoords(final String coords) {
        this.q = null;
        this.mq = null;
        this.geometry = null;
        this.coords = coords;
        this.georel = null;
    }

    public String getGeorel() {
        return this.georel;
    }

    public void setGeorel(final String georel) {
        this.q = null;
        this.mq = null;
        this.geometry = null;
        this.coords = null;
        this.georel = georel;
    }

    public SubscriptionSubjectConditionExpression(final String q) {
        this.q = null;
        this.mq = null;
        this.geometry = null;
        this.coords = null;
        this.georel = null;
        this.q = q;
    }

    public SubscriptionSubjectConditionExpression() {
        this.q = null;
        this.mq = null;
        this.geometry = null;
        this.coords = null;
        this.georel = null;
        this.q = null;
        this.mq = null;
        this.geometry = null;
        this.coords = null;
        this.georel = null;
    }
}
