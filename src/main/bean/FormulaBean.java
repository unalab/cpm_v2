package main.bean;

public class FormulaBean {
	
	private int id;
	private String name;
	private String formula;
	private String status;
	private String city;
	private int id_cpm;
	private String orionEntity;
	
	public FormulaBean() {
		super();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getId_cpm() {
		return id_cpm;
	}
	public void setId_cpm(int id_cpm) {
		this.id_cpm = id_cpm;
	}
	public String getOrionEntity() {
		return orionEntity;
	}
	public void setOrionEntity(String orionEntity) {
		this.orionEntity = orionEntity;
	}

}
