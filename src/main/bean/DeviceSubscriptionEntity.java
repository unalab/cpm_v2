package main.bean;

public class DeviceSubscriptionEntity extends CBEntity
{
    private EntityAttribute<String> deviceId;
    private EntityAttribute<String> subscriptionId;
    private EntityAttribute<String> mashupId;
    private EntityAttribute<String> status;
    private EntityAttribute<String> value;
    private EntityAttribute<String> lastUpdate;
    private EntityAttribute<String> unitMeasure;
    private EntityAttribute<String> name;
    

    public EntityAttribute<String> getUnitMeasure() {
        return this.unitMeasure;
    }

    public void setUnitMeasure(final EntityAttribute<String> unitMeasure) {
        this.unitMeasure = unitMeasure;
    }
    
    public EntityAttribute<String> getName() {
        return this.name;
    }

    public void setName(final EntityAttribute<String> name) {
        this.name = name;
    }

    public EntityAttribute<String> getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(final EntityAttribute<String> lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public EntityAttribute<String> getValue() {
        return this.value;
    }

    public void setValue(final EntityAttribute<String> value) {
        this.value = value;
    }

    public EntityAttribute<String> getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(final EntityAttribute<String> deviceId) {
        this.deviceId = deviceId;
    }

    public EntityAttribute<String> getSubscriptionId() {
        return this.subscriptionId;
    }

    public void setSubscriptionId(final EntityAttribute<String> subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public EntityAttribute<String> getStatus() {
        return this.status;
    }

    public void setStatus(final EntityAttribute<String> status) {
        this.status = status;
    }

    public EntityAttribute<String> getMashupId() {
        return this.mashupId;
    }

    public void setMashupId(final EntityAttribute<String> mashupId) {
        this.mashupId = mashupId;
    }

    public DeviceSubscriptionEntity() {
        super("", "");
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
        new EntityAttribute((Object)new String());
    }

    public DeviceSubscriptionEntity(final String id, final String type, final EntityAttribute<String> deviceId, final EntityAttribute<String> subscriptionId, final EntityAttribute<String> mashupId, final EntityAttribute<String> value) {
        super(id, type);
        this.deviceId = deviceId;
        this.subscriptionId = subscriptionId;
        this.mashupId = mashupId;
        this.value = value;
    }
}
