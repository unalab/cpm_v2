package main.bean;

public class SubcriptionSubjectEntity
{
    String idPattern;
    String id;
    String type;

    public String getIdPattern() {
        return this.idPattern;
    }

    public void setIdPattern(final String idPattern) {
        this.idPattern = idPattern;
        this.id = null;
    }

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
        this.idPattern = null;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public SubcriptionSubjectEntity() {
        this.idPattern = "";
        this.id = "";
        this.type = "";
        this.idPattern = "";
        this.id = "";
        this.type = "";
    }
}
